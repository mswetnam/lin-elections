$(document).ready(function(){			
	reloadSearch = function(isLoading, isDirty, delay, filterval, stationid, theSource, theData, theProcessor, theException) {
		//alert('During');
		if(!isLoading){
			isLoading = true;
			fetchData(theSource, theData, theProcessor);
			setTimeout(function(){
				isLoading=false;
				if(isDirty){
					isDirty = false;
					reloadSearch(isLoading, isDirty, filterval, stationid, theSource, theData, theProcessor, theException);
				}
			}, delay);
		}
	};
	fetchData = function(theSource, theData, theProcessor, theException)
	{
		//alert(theData);
		$.ajax({
			url: theSource,
			type:'POST',
			dataType: 'json',
			data: theData,
			error: function(jqXHR, textStatus, errorThrown){
				if (typeof theException=="function"){ theException(errorThrown); }
			},
			success: function(data, textStatus, jqXHR){
				//alert(data);
				if (typeof theProcessor=="function"){ theProcessor(data); }
			}
		});
	}
	addElement = function(el_type, el_attr, el_html) {
		var theelement = document.createElement(el_type);
		for(i=0; i<el_attr.length; i++) {
			var theattr = el_attr[i].split("|");
			if(theattr[0]==="class") {
				$(theelement).addClass(theattr[1]);
			} else {
				$(theelement).attr(theattr[0], theattr[1]);
			}
		}
		$(theelement).html(el_html);
		return theelement;
	}
	editRecord = function(therow, theform, formaction, thefunction) {
		var idsplit = therow.id.split("-");
		var idsubmit = idsplit[1];
		theform.attr('action', formaction + '/' + thefunction + '/' + idsubmit);
		theform.submit();
	}
	deleteRecord = function(therow, theform, formaction, thefunction) {
		var idsplit = therow.id.split("-");
		var idsubmit = idsplit[1];
		if(confirm('You are about to delete record ' + idsubmit + ' from the database. This action cannot be undone. Are you sure you want to continue? ')) {
			theform.attr('action', formaction + '/' + thefunction + '/' + idsubmit);
			theform.submit();
		}
	}
	toggleRecord = function(therow, theform, formaction, thefunction) {
		var idsplit = therow.id.split("-");
		var idsubmit = idsplit[1];
		theform.attr('action', formaction + '/' + thefunction + '/' + idsubmit);
		theform.submit();
	}
	selectAll = function(thebox, thelot) {
		var gcheck = $(thebox).is(':checked')? true : false;
		$(thelot).each(function(){
			(gcheck===true)? $(this).prop("checked", true) : $(this).prop("checked", false);
		});
	}
	makePaging = function(pagecount, totalrecs, step, pagenum, increment, infotext, pagenav, pagingfield, filterval, stationid, theProcessor) {
		var prev = "";
		var next = "";
		var page_list = "";
		var page_link = "";
		var total_records = totalrecs;
		var curpage = getCookie('the_page');
		var step = getCookie('the_step');
		var range_start = (parseInt(curpage)-1)*parseInt(step)+1;
		var range_end = ((parseInt(curpage)-1)*parseInt(step))+parseInt(step);
		var pagelimit = 10;
		range_end = (range_end < total_records.val())? range_end : total_records.val();
		pagingfield.val(curpage);
		infotext.empty();
		pagenav.empty();
		// Create wrapper
		var paging = addElement("div", new Array("id|admin_cp_paging","class|admin_cp_paging"));
		// Create range
		var range = addElement("span", new Array("id|page_range","class|page_range"), "Items " + range_start + " to " + range_end + " of " + total_records.val());
		// Create 'Previous' and 'Next' links
		if(increment===true) {
			prev = addElement("span", new Array("id|page_number-p","class|page_number"), "<&nbsp;&nbsp;");
			$(prev).bind('click', function(){
				curpage = (parseInt(curpage)==1)? 1 : parseInt(curpage)-1;
				setCookie('the_page',curpage,1);
				fetchData(theSource, theData, theProcessor);
			});
			next = addElement("span", new Array("id|page_number-n","class|page_number"), "&nbsp;&nbsp;>");
			$(next).bind('click', function(){
				curpage = (parseInt(curpage)==parseInt(pages))? parseInt(pages) : parseInt(curpage)+1;
				setCookie('the_page',curpage,1);
				fetchData(theSource, theData, theProcessor);
			});
		}
		// Append Previous span to wrapper
		$(paging).append(prev);
		//Create page number links and append to wrapper
		var numtest = true;
		if(pagenum===true) {
			var pages = Math.ceil(parseInt(pagecount)/parseInt(step));
			numtest = (parseInt(pages) <= 1)? false : true;
			for(i=1; i<=pages; i++) {
				if(i > parseInt(curpage)-parseInt(pagelimit) && i< parseInt(curpage)+parseInt(pagelimit)){
					var selected = (i==parseInt(curpage))? ' page_selected' : '';
					var page_first = (i==1)? ' page_first' : '';
					var page_last = (i==parseInt(pages))? ' page_last' : '';
					nav_text = (i!=parseInt(pages))? '&nbsp;' + i + '&nbsp;' : i;
					page_link = document.createElement('span');
					$(page_link).attr('id', 'page_number-' + i);
					$(page_link).attr('class', 'page_number' + selected + page_first + page_last);
					$(page_link).html(nav_text);
					$(page_link).bind('click', function(){
						var pageval = $(this).attr("id").split("-")[1];
						setCookie('the_page',pageval,1);
						var stationid = getCookie('the_organization');
						var dayformat = getCookie('the_dayformat');
						var timeformat = getCookie('the_timeformat');
						theData = "filter_val=" + escape(filterval) + "&station_id=" + escape(stationid) + "&curpage=" + escape(pageval) + "&step=" + escape(step) + "&dayformat=" + escape(dayformat) + "&timeformat=" + escape(timeformat);
						fetchData(theSource, theData, theProcessor);
					});
					$(paging).append(page_link);
				}
			}
		} else {
			numtest = false;
		}
		// Append Next span to wrapper
		$(paging).append(next);
		// Place range
		infotext.append(range);
		// Place paging
		if(numtest===true){
			pagenav.append(paging);
		}
		var cleardiv = addElement("div", new Array("id|clear_paging","style|clear:both;"));
		pagenav.append(cleardiv);
	}
	setCookie = function(c_name,value,exdays)
	{
		var exdate=new Date();
		exdate.setDate(exdate.getDate() + exdays);
		var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
		document.cookie=c_name + "=" + c_value;
	}
	getCookie = function(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
			x=x.replace(/^\s+|\s+$/g,"");
			if (x==c_name)
			{
				return unescape(y);
			}
		}
	}
	killCookie = function(c_name)
	{
		document.cookie=c_name + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT";
	}
});
