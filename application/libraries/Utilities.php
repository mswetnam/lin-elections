<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//--------------------------------------
// High school sports library
// Author: Matthew Swetnam
// On behalf of: WISH-TV
//--------------------------------------

class Utilities {
	// Works well for a single key
	function aasort(&$array, $key)
	{
		$sorter=array();
		$ret=array();
		reset($array);
		foreach ($array as $ii => $va)
		{
			$sorter[$ii]=$va[$key];
		}
		asort($sorter);
		foreach ($sorter as $ii => $va)
		{
			$ret[$ii]=$array[$ii];
		}
		$array=$ret;
	}
	
	function getPaging($current, $pagecount=NULL, $step=100, $pagenum=NULL, $increment=NULL) {
		$prev = "";
		$next = "";
		$page_list = "";
		if($increment) {
			$prev = "<span id=\"page_number-p\" class=\"page_number\">Previous</span>\n";
			$next = "<span id=\"page_number-n\" class=\"page_number\">Next</span>\n";
		}
		if($pagenum) {
			$pages = ceil($pagecount/$step);
			for($i=1; $i<=$pages; $i++) {
				$page_list .= "<span id=\"page_number-".$i."\" class=\"page_number".(($i==$current)? ' page_selected' : '').(($i==1)? ' page_first' : '').(($i==$pages)? ' page_last' : '')."\">&nbsp;".$i."&nbsp;</span>|";
			}
			$page_list = substr_replace ($page_list, "", strrpos($page_list, "|"), 1);
		}
		$return = 
			"<div id=\"admin_cp_paging\" class=\"admin_cp_paging\">".$prev."&nbsp;".$page_list."&nbsp;".$next."</div>\n".
			"<input type=\"hidden\" id=\"admin_paging\" name=\"admin_paging\" value=\"".$current."\">\n";
		return $return;
	}
	
	function minSecToAllSec($val){
		$val = explode(":", $val);
		$secs = ($val[0]*60) + $val[1];
		return $secs;
	}
	
	function allSecToMinSec($val){
		$min = floor(floatval($val/60));
		$sec = str_pad(($val%60), 2, "0", STR_PAD_LEFT);
		$time = $min.":".$sec;
		return $time;
	}
	
	function multiColSort() {
		$args = func_get_args();
		$data = array_shift($args);
		foreach ($args as $n => $field) {
			if (is_string($field)) {
				$tmp = array();
				foreach ($data as $key => $row)
					$tmp[$key] = $row[$field];
					$args[$n] = $tmp;
			}
		}
		$args[] = &$data;
		call_user_func_array('array_multisort', $args);
		return array_pop($args);
	}

	function arrayToXml($array, $rootElement = null, $xml = null) {
		$_xml = $xml;
		if ($_xml === null) {
			$_xml = new SimpleXMLElement($rootElement !== null ? $rootElement : '<root/>');
		}
		foreach ($array as $k => $v) {
			if (is_array($v)) {
				$this->arrayToXml($v, $k, $_xml->addChild($k));
			} else {
				$_xml->addChild($k, $v);
			}
		}
		return $_xml->asXML();
	}
	
	function array_to_xml($thearray, $thexml) {
		foreach($thearray as $key => $value) {
			if(is_array($value)) {
				if(!is_numeric($key)){
					$subnode = $thexml->addChild("$key");
					$this->array_to_xml($value, $subnode);
				} else {
					$this->array_to_xml($value, $thexml);
				}
			} else {
				$thexml->addChild($key,$value);
			}
		}
		return $thexml;
	}
	
	public function XmlToJson($url) {
		$fileContents= file_get_contents($url);
		$fileContents = str_replace(array("\n", "\r", "\t"), '', $fileContents);
		$fileContents = trim(str_replace('"', "'", $fileContents));
		$simpleXml = simplexml_load_string($fileContents);
		$json = json_encode($simpleXml);
		return $json;
	}
	
	/**
	* Check that a file exists at a given url
	*/
	public function remoteFileExists($url) {
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_NOBODY, true);
		curl_exec($ch);
		$retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		if ($retcode === 200) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	* Downloads the XML from a given URL
	*/
	public function downloadFileContents($url, $agent=NULL)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FAILONERROR, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSLVERSION, 3);
		curl_setopt($ch, CURLOPT_TIMEOUT, 0);
		if(!is_null($agent)){
			curl_setopt($ch, CURLOPT_USERAGENT, $agent);
		}
		if($return = curl_exec($ch)){
			curl_close($ch);
			return $return;
		} else {
			$theerror = curl_error($ch);
			curl_close($ch);
			return $theerror;
		}
	}
}
/* End of file Xmlfeed.php */