<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
//--------------------------------------
// Election data library
// Author: Matthew Swetnam
// On behalf of: WISH-TV
//--------------------------------------

class Electiondatalib
{	
	
	/**
	* Checks to see if a remote file exists
	*/
	function remoteFileExists($url) {
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_NOBODY, true);
		curl_exec($ch);
		$retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		if ($retcode === 200) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	* Downloads the XML from a given URL
	*/
	public function downloadXml($url)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FAILONERROR, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 15);
		$return = curl_exec($ch);
		curl_close($ch);
		return $return;
	}
	
	/**
	* Parses a given XML string
	*/
	public function getParsedFeed($xml)
	{
		$xml = simplexml_load_string($xml);
		return $xml;
	}
	
	/**
	* Counts the nodes for the node name specified in a previously parsed feed
	*/
	public function countNodes($xml, $node)
	{
		return count($xml->$node);
	}
	
	/**
	* Returns the array of attributes associated with a specified node in a previously parsed feed
	*/
	public function getAttributeArray($xml)
	{
		$att_array = array();
		foreach($xml->$node->attributes() as $key=>$value)
		{
			$att_array[$key] = strval($value);
		}
		return $att_array;
	}
	
	/**
	* The following functions are for returning data elements from a leader feed
	*/
	
	function getLeaderRace($xml, $i=0) {
		$therace = $xml->race[$i]->race_info;
		return $therace;
	}
	
	function getLeaderRaceID($xml, $i=0) {
		$therace = $this->getLeaderRace($xml, $i);
		return $therace->race_id;
	}
	
	function getLeaderRaceName1($xml, $i=0) {
		$therace = $this->getLeaderRace($xml, $i);
		return $therace->race_name1;
	}
	
	function getLeaderRaceName2($xml, $i=0) {
		$therace = $this->getLeaderRace($xml, $i);
		return $therace->race_name2;
	}
	
	function getLeaderRaceName3($xml, $i=0) {
		$therace = $this->getLeaderRace($xml, $i);
		return $therace->race_name3;
	}
	
	function getLeaderRacePrecinctsReporting($xml, $i=0) {
		$therace = $this->getLeaderRace($xml, $i);
		return $therace->precincts_reporting;
	}
	
	function getLeaderRacePercentPrecinctsReporting($xml, $i=0) {
		$therace = $this->getLeaderRace($xml, $i);
		return $therace->pct_precincts_reporting;
	}
	
	function getLeaderRacePrecinctsTotal($xml, $i=0) {
		$therace = $this->getLeaderRace($xml, $i);
		return $therace->precincts_total;
	}
	
	function getLeaderRaceLastUpdated($xml, $i=0) {
		$therace = $this->getLeaderRace($xml, $i);
		return $therace->last_updated;
	}
	
	function getLeaderRaceTotalVote($xml, $i=0) {
		$therace = $this->getLeaderRace($xml, $i);
		return $therace->total_vote;
	}
	
	function getLeaderRaceJurisdiction($xml, $i=0) {
		$therace = $this->getLeaderRace($xml, $i);
		return $therace->jurisdiction_id;
	}
	
	function getLeaderRaceOther1($xml, $i=0) {
		$therace = $this->getLeaderRace($xml, $i);
		return $therace->other1;
	}
	
	function getLeaderRaceOther2($xml, $i=0) {
		$therace = $this->getLeaderRace($xml, $i);
		return $therace->other2;
	}
	
	function getLeaderRaceOther3($xml, $i=0) {
		$therace = $this->getLeaderRace($xml, $i);
		return $therace->other3;
	}
	
	function getLeaderRaceProposition($xml, $i=0) {
		$therace = $this->getLeaderRace($xml, $i);
		return $therace->proposition;
	}
	
	function getLeaderRaceYesNo1($xml, $i=0) {
		$therace = $this->getLeaderRace($xml, $i);
		return $therace->yesno1;
	}
	
	function getLeaderRaceYesNo2($xml, $i=0) {
		$therace = $this->getLeaderRace($xml, $i);
		return $therace->yesno2;
	}
	
	function getLeaderRaceYesNo3($xml, $i=0) {
		$therace = $this->getLeaderRace($xml, $i);
		return $therace->yesno3;
	}
	
	function getLeaderRaceYesNo4($xml, $i=0) {
		$therace = $this->getLeaderRace($xml, $i);
		return $therace->yesno4;
	}
	
	/**
	* The following functions are for returning data elements from a newsticker feed
	*/
	
	function getNewsTickerRace($xml, $i=0) {
		$therace = $xml->RACE[$i]->RACE_INFO;
		return $therace;
	}
	
	function getNewsTickerRaceID($xml, $i=0) {
		$therace = $this->getNewsTickerRace($xml, $i);
		return $therace->RACE_ID;
	}
	
	function getNewsTickerRaceJurisdiction($xml, $i=0) {
		$therace = $this->getNewsTickerRace($xml, $i);
		return $therace->JURISDICTION_ID;
	}
	
	function getNewsTickerRaceName1($xml, $i=0) {
		$therace = $this->getNewsTickerRace($xml, $i);
		return $therace->RACE_NAME1;
	}
	
	function getNewsTickerRaceName2($xml, $i=0) {
		$therace = $this->getNewsTickerRace($xml, $i);
		return $therace->RACE_NAME2;
	}
	
	function getNewsTickerRaceName3($xml, $i=0) {
		$therace = $this->getNewsTickerRace($xml, $i);
		return $therace->RACE_NAME3;
	}
	
	function getNewsTickerRaceName4($xml, $i=0) {
		$therace = $this->getNewsTickerRace($xml, $i);
		return $therace->RACE_NAME4;
	}
	
	function getNewsTickerRaceNumberOfSeats($xml, $i=0) {
		$therace = $this->getNewsTickerRace($xml, $i);
		return $therace->NUM_SEATS;
	}
	
	function getNewsTickerRacePrompt1($xml, $i=0) {
		$therace = $this->getNewsTickerRace($xml, $i);
		return $therace->PROMPT_LINES1;
	}
	
	function getNewsTickerRacePrompt2($xml, $i=0) {
		$therace = $this->getNewsTickerRace($xml, $i);
		return $therace->PROMPT_LINES2;
	}
	
	function getNewsTickerRacePrecinctsReporting($xml, $i=0) {
		$therace = $this->getNewsTickerRace($xml, $i);
		return $therace->PRECINCTS_REPORTING;
	}
	
	function getNewsTickerRacePercentPrecinctsReporting($xml, $i=0) {
		$therace = $this->getNewsTickerRace($xml, $i);
		return $therace->PCT_PRECINCTS_REPORTING;
	}
	
	function getNewsTickerRacePrecinctsTotal($xml, $i=0) {
		$therace = $this->getNewsTickerRace($xml, $i);
		return $therace->PRECINCTS_TOTAL;
	}
	
	function getNewsTickerRaceUpdated($xml, $i=0) {
		$therace = $this->getNewsTickerRace($xml, $i);
		return $therace->UPDATED;
	}
	
	function getNewsTickerRaceAllVotes($xml, $i=0) {
		$therace = $this->getNewsTickerRace($xml, $i);
		return $therace->ALLCAND_VOTES;
	}
	
	/**
	* The following functions are all related to parsing the AP state-by-state presidential feed
	*/
	function getApState($xml, $i=0) {
		return $xml->State[$i];
	}
	
	function getApStateCode($xml, $i=0) {
		$state =  $this->getApState($xml, $i);
		$attributes =  $state->attributes();
		return $attributes['PostalCode'];
	}
	
	function getApStateTotal($xml, $i=0) {
		$state =  $this->getApState($xml, $i);
		$attributes =  $state->attributes();
		return $attributes['ElectTotal'];
	}
	
	function getApStatePrecincts($xml, $i=0) {
		$state =  $this->getApState($xml, $i);
		$attributes =  $state->attributes();
		return $attributes['PrecinctsPct'];
	}
	
	function getApCandidate($xml, $i=0, $j=0) {
		$state =  $this->getApState($xml, $i);
		return $state->Cand[$j];
	}
	
	function getApCandidateName($xml, $i=0, $j=0) {
		$candidate = $this->getApCandidate($xml, $i, $j);
		$attributes =  $candidate->attributes();
		return $attributes['name'];
	}
	
	function getApCandidateID($xml, $i=0, $j=0) {
		$candidate = $this->getApCandidate($xml, $i, $j);
		$attributes =  $candidate->attributes();
		return $attributes['CandID'];
	}
	
	function getApCandidateParty($xml, $i=0, $j=0) {
		$candidate = $this->getApCandidate($xml, $i, $j);
		$attributes =  $candidate->attributes();
		return $attributes['party'];
	}
	
	function getApCandidatePopular($xml, $i=0, $j=0) {
		$candidate = $this->getApCandidate($xml, $i, $j);
		$attributes =  $candidate->attributes();
		return $attributes['PopVote'];
	}
	
	function getApCandidatePopPercent($xml, $i=0, $j=0) {
		$candidate = $this->getApCandidate($xml, $i, $j);
		$attributes =  $candidate->attributes();
		return $attributes['PopPct'];
	}
	
	function getApCandidateElectWon($xml, $i=0, $j=0) {
		$candidate = $this->getApCandidate($xml, $i, $j);
		$attributes =  $candidate->attributes();
		return $attributes['ElectWon'];
	}
	
	function getApCandidateWinner($xml, $i=0, $j=0) {
		$candidate = $this->getApCandidate($xml, $i, $j);
		$attributes =  $candidate->attributes();
		$winner = (($attributes['ElectWon'])? true : false);
		return $winner;
	}
	
	/**
	* The following functions are for returning data from internally generated feeds
	*/
	
	function getE2012Race($xml, $i=0) {
		$therace = $xml->race[$i]->race_info;
		return $therace;
	}
	
	function getE2012RaceID($xml, $i=0) {
		$therace = $this->getE2012Race($xml, $i);
		return $therace->race_id;
	}
	
	function getE2012RaceName1($xml, $i=0) {
		$therace = $this->getE2012Race($xml, $i);
		return $therace->race_name1;
	}
	
	function getE2012RaceName2($xml, $i=0) {
		$therace = $this->getE2012Race($xml, $i);
		return $therace->race_name2;
	}
	
	function getE2012RaceName3($xml, $i=0) {
		$therace = $this->getE2012Race($xml, $i);
		return $therace->race_name3;
	}
	
	function getE2012Precincts($xml, $i=0) {
		$therace = $this->getE2012Race($xml, $i);
		return $therace->precincts_reporting;
	}
	
	function getE2012PrecinctsPct($xml, $i=0) {
		$therace = $this->getE2012Race($xml, $i);
		return $therace->pct_precincts_reporting;
	}
	
	function getE2012PrecinctsTotal($xml, $i=0) {
		$therace = $this->getE2012Race($xml, $i);
		return $therace->pct_precincts_reporting;
	}
	
	function getE2012LastUpdated($xml, $i=0) {
		$therace = $this->getE2012Race($xml, $i);
		return $therace->last_updated;
	}
	
	function getE2012Timestamp($xml, $i=0) {
		$therace = $this->getE2012Race($xml, $i);
		return $therace->timestamp;
	}
	
	function getE2012TotalVote($xml, $i=0) {
		$therace = $this->getE2012Race($xml, $i);
		return $therace->total_vote;
	}
	
	function getE2012Other1($xml, $i=0) {
		$therace = $this->getE2012Race($xml, $i);
		return $therace->other1;
	}
	
	function getE2012Other2($xml, $i=0) {
		$therace = $this->getE2012Race($xml, $i);
		return $therace->other2;
	}
	
	function getE2012Other3($xml, $i=0) {
		$therace = $this->getE2012Race($xml, $i);
		return $therace->other3;
	}
	
	function getE2012Jurisdiction($xml, $i=0) {
		$therace = $this->getE2012Race($xml, $i);
		return $therace->jurisdiction_id;
	}
	
	function getE2012Candidate($xml, $i=0, $j=0) {
		$thecandidate = $xml->race[$i]->candidate_info;
		return $thecandidate->candidate[$j];
	}
	
	function getE2012CandidateId($xml, $i=0, $j=0) {
		$thecandidate = $this->getE2012Candidate($xml, $i, $j);
		return $thecandidate->candidate_id;
	}
	
	function getE2012CandidateFirst($xml, $i=0, $j=0) {
		$thecandidate = $this->getE2012Candidate($xml, $i, $j);
		return $thecandidate->first_name;
	}
	
	function getE2012CandidateMI($xml, $i=0, $j=0) {
		$thecandidate = $this->getE2012Candidate($xml, $i, $j);
		return $thecandidate->initial;
	}
	
	function getE2012CandidateLast($xml, $i=0, $j=0) {
		$thecandidate = $this->getE2012Candidate($xml, $i, $j);
		return $thecandidate->last_name;
	}
	
	function getE2012CandidateIncumbent($xml, $i=0, $j=0) {
		$thecandidate = $this->getE2012Candidate($xml, $i, $j);
		return $thecandidate->incumbent;
	}
	
	function getE2012CandidateParty($xml, $i=0, $j=0) {
		$thecandidate = $this->getE2012Candidate($xml, $i, $j);
		return $thecandidate->party_abbr;
	}
	
	function getE2012CandidatePopVote($xml, $i=0, $j=0) {
		$thecandidate = $this->getE2012Candidate($xml, $i, $j);
		return $thecandidate->cand_vote;
	}
	
	function getE2012CandidateVotePct($xml, $i=0, $j=0) {
		$thecandidate = $this->getE2012Candidate($xml, $i, $j);
		return $thecandidate->vote_percent;
	}
	
	function getE2012CandidateWinner($xml, $i=0, $j=0) {
		$thecandidate = $this->getE2012Candidate($xml, $i, $j);
		return $thecandidate->winner;
	}
	
	function getE2012CandidateProjectedWinner($xml, $i=0, $j=0) {
		$thecandidate = $this->getE2012Candidate($xml, $i, $j);
		return $thecandidate->projected_winner;
	}
	
	function getE2012CandidateOther1($xml, $i=0, $j=0) {
		$thecandidate = $this->getE2012Candidate($xml, $i, $j);
		return $thecandidate->cother1;
	}
	
	function getE2012CandidateOther2($xml, $i=0, $j=0) {
		$thecandidate = $this->getE2012Candidate($xml, $i, $j);
		return $thecandidate->cother2;
	}
	
	function getE2012CandidateOther3($xml, $i=0, $j=0) {
		$thecandidate = $this->getE2012Candidate($xml, $i, $j);
		return $thecandidate->cother3;
	}
}
/* End of file Electiondatalib.php */