	<div id="admin_header" class="admin_header">
		<div id="admin_title" class="admin_title">System Management</div>
		<div id="admin_header_nav" class="admin_header_nav">
			<div id="admin_selection_home" class="admin_selection"><a href="<?=$base_url?>index.php/coreAdmin" target="_self">Home</a></div>
			<div id="admin_selection_users" class="admin_selection"><a href="<?=$base_url?>index.php/coreAdmin/manageUsers" target="_self">Users</a></div>
			<div id="admin_selection_geographic" class="admin_selection"><a href="<?=$base_url?>index.php/coreAdmin/manageGeographic" target="_self">Geographic</a></div>
			<div id="admin_selection_organizations" class="admin_selection"><a href="<?=$base_url?>index.php/coreAdmin/manageOrganizations" target="_self">Organizations</a></div>
			<div id="admin_selection_groups" class="admin_selection"><a href="<?=$base_url?>index.php/coreAdmin/manageGroups" target="_self">Groups</a></div>
			<div id="admin_selection_modules" class="admin_selection"><a href="<?=$base_url?>index.php/coreAdmin/manageModules" target="_self">Modules</a></div>
			<div id="admin_selection_permissions" class="admin_selection"><a href="<?=$base_url?>index.php/coreAdmin/managePermissions" target="_self">Permissions</a></div>
			<div id="admin_selection_configs" class="admin_selection"><a href="<?=$base_url?>index.php/coreAdmin/manageConfigurations" target="_self">Configs</a></div>
		</div>
		<div id="admin_userbar" class="admin_userbar">
			<?php if(array_key_exists("wish_user", $wish_user)) { ?>
				Logged In As: <?=$wish_user['wish_username']?> || <a href="<?=$base_url?>index.php/coreAdmin/manageUsers/userlogout" target="_self">Logout</a>
			<?php } else { ?>
			<form id="userbar_login_form" action="<?=$base_url?>index.php/coreAdmin/manageUsers/checklogin" enctype="multipart/form-data" method="POST">
				<label for="user_username">Log In </label><input type="text" id="user_username" class="form_text 50_wide" name="user_username" value="" />
				<input type="password" id="user_password" class="form_text 50_wide" name="user_password" value="" />
				<input type="hidden" id="user_redirect" name="user_redirect" value="" />
				<input type="submit" id="user_login_submit" class="form_button 100_wide" name="user_login_submit" value="GO"><br />
				<div id="user_login_footnote1" class"user_login_footnote">Not registered? <a href="<?=$base_url?>index.php/coreAdmin/manageUsers/adduser" target="_self">Create an account</a>.</div>
				<div id="user_login_footnote2" class"user_login_footnote">Don't remember your login? <a href="<?=$base_url?>index.php/coreAdmin/manageUsers/retrievelogin" target="_self">Retrieve it</a>.</div>
			</form>
			<?php } ?>
		</div>
	</div>
	<div id="admin_clear" class="admin_clear"></div>
	<div id="admin_body_wrapper" class="admin_body_wrapper">