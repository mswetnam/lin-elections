<?php
if(!array_key_exists("wish_user", $wish_user)) {
	$logged_in = false;
	$op = "login";
} else {
	$logged_in = true;
	if(array_key_exists("wish_permissions", $wish_user)){
		$wish_permissions = $wish_user['wish_permissions'];
	} else {
		$wish_permissions = array();
	}
}
$active_icon = "<img src='".$base_url."ci/_common/images/admin-check.png' class='row_toggle' border=0 />";
$inactive_icon = "<img src='".$base_url."ci/_common/images/admin-check-false.png' class='row_toggle' border=0 />";
?>
<div id="admin_load" class="admin_load"><img src="<?=$base_url?>ci/_common/images/spinner.gif" id="admin_load_wait" class="admin_load_wait" border="0" /></div>
<div id="admin_wrapper" class="admin_wrapper">
	<div id="admin_header" class="admin_header">
		<div id="admin_title" class="admin_title">Manage Elections 2012</div>
		<div id="admin_header_nav" class="admin_header_nav">
			<div id="admin_selection_home" class="admin_selection"><a href="<?=$base_url?>index.php/election2012/manageElection" target="_self">Home</a></div>
			<div id="admin_selection_party" class="admin_selection"><a href="<?=$base_url?>index.php/election2012/manageElection/party" target="_self">Party</a></div>
			<div id="admin_selection_racestatus" class="admin_selection"><a href="<?=$base_url?>index.php/election2012/manageElection/racestatus" target="_self">Status</a></div>
			<div id="admin_selection_race" class="admin_selection"><a href="<?=$base_url?>index.php/election2012/manageElection/race" target="_self">Race</a></div>
			<div id="admin_selection_candidate" class="admin_selection"><a href="<?=$base_url?>index.php/election2012/manageElection/candidate" target="_self">Candidate</a></div>
			<div id="admin_selection_preferences" class="admin_selection"><a href="<?=$base_url?>index.php/election2012/manageElection/preference" target="_self">Preferences</a></div>
		</div>
		<div id="admin_userbar" class="admin_userbar">
			<?php if(array_key_exists("wish_user", $wish_user)) { ?>
			<form id="userbar_login_form" action="<?=$base_url?>index.php/coreAdmin/manageUsers/userlogout" enctype="multipart/form-data" method="POST">
				Logged In As: <?=$wish_user['wish_username']?> || <?php 
				if(in_array("admin_access", $wish_permissions)) { ?>
				<input type="image" src="<?=$base_url?>ci/_common/images/admin-tools-small.png" id="user_admin_access" class="form_button" name="user_admin_access" value=""> || 
				<?php } ?>
				<input type="submit" id="user_login_submit" class="form_button 100_wide" name="user_login_submit" value="Logout"><br />
				<input type="hidden" id="user_redirect" name="user_redirect" value="" />
			</form>
			<?php } else { ?>
			<form id="userbar_login_form" action="<?=$base_url?>index.php/coreAdmin/manageUsers/checklogin" enctype="multipart/form-data" method="POST">
				<label for="user_username">Log In </label><input type="text" id="user_username" class="form_text 50_wide" name="user_username" value="" />
				<input type="password" id="user_password" class="form_text 50_wide" name="user_password" value="" />
				<input type="hidden" id="user_redirect" name="user_redirect" value="" />
				<input type="submit" id="user_login_submit" class="form_button 100_wide" name="user_login_submit" value="GO"><br />
				<div id="user_login_footnote" class"user_login_footnote">Not registered? <a href="<?=$base_url?>index.php/coreAdmin/manageUsers/adduser" target="_self">Create an account</a>.</div>
				<div id="user_login_footnote" class"user_login_footnote">Don't remember your login? <a href="<?=$base_url?>index.php/coreAdmin/manageUsers/retrievelogin" target="_self">Retrieve it</a>.</div>
			</form>
			<?php } ?>
		</div>
	</div>
	<div id="admin_clear" class="admin_clear"></div>
	<div id="admin_body_wrapper" class="admin_body_wrapper">
<?php
switch($op) {
	// Party functions
	case 'party':
	{ ?>
		<form id="admin_action_form" action="<?=$base_url?>index.php/election2012/manageElection" enctype="multipart/form-data" method="POST">
			<div id="admin_cp_wrapper" class="admin_cp_wrapper_50">
				<span id="cp_back" class="cp_back"><--Back</span>
				<select id="admin_cp_station" class="form_select" name="admin_cp_station"><?php
				$stations = (($wish_user)? ((array_key_exists('wish_stations', $wish_user))? $wish_user['wish_stations'] : '') : '');
				if($stations) {
					foreach($stations as $key=>$value) {
						?><option value="<?=$key?>"<?=(($key==$station_id)? ' selected=\"selected\"' : '')?>><?=$value?></option><?php
					}
				}
				?></select>
				<select id="op_select" class="op_select" name="op_select">
					<option value="addparty">Add</option>
					<option value="removeparty">Delete</option>
				</select>
				<input type="submit" id="admin_action_form_go" class="admin_action_form_btn" name="admin_action_form_go" value="GO">
				<input type="hidden" id="admin_table_rowselect_hidden" name="admin_table_rowselect_hidden" value="">
				<input type="hidden" id="admin_redirect" name="admin_redirect" value="" />
				<input type="hidden" id="admin_paging" name="admin_paging" value="" />
				<input type="hidden" id="admin_total_records" name="admin_total_records" value="" />
			</div>
			<div id="admin_list_outter_wrapper" class="admin_list_outter_wrapper_50">
				<div id="admin_list_inner_wrapper" class="admin_list_inner_wrapper container">
					
				</div>
			</div>
		</form>
		<script language="JavaScript">
			var _theform = "";
			var _formaction = "";
			$(document).ready(function(){
				$("#stdwww").addClass('gone');
				_theform = $('#admin_action_form');
				_formaction = "<?=$base_url?>index.php/election2012/manageElection";
				$('#admin_redirect').val(document.URL);
				$('#admin_selection_party').addClass('selected');
				$('#admin_action_form_go').focus();
				$('#admin_action_form_go').bind('click', function(e){
					e.preventDefault();
					var selectaction = $('#op_select').find(':selected').val();
					var selectedboxes = $('input[id^="admin_list_container_select-"]:checked').map(function() {
						return this.id.split("-")[1];
					}).get().join(',');
					$('#admin_table_rowselect_hidden').val(selectedboxes);
					$('#admin_action_form').attr('action', _formaction + '/' + selectaction);
					switch(selectaction)
					{
						case 'removeparty':
						{
							if(confirm('You are about to delete ' + $('input[id^="admin_list_container_select-"]:checked').length + ' records from the database. This action cannot be undone. Are you sure you want to continue?')) {
								$('#admin_action_form').submit();
							}
							break;
						}
						default:
						{
							$('#admin_action_form').attr('action', _formaction + '/addparty');
							$('#admin_action_form').submit();
						}
					}
				});
				$('#admin_cp_station').bind("change", function() {
					var newstation = $('#admin_cp_station').find(':selected').val();
					$('#admin_action_form').attr('action', formaction + '/setstation/' + newstation);
					$('#admin_action_form').submit();
				});
				getPartyData('','','','',<?=$station_id?>,'','',processPartyData,processPartyError);
				function getPartyData(id,name,shortn,icon,org,dformat,tformat,success,failure) {
					var theSource = "<?=$base_url?>index.php/election2012/fetchPartyData";
					var theData = "id=" + escape(id) + 
						"&name=" + escape(name) + 
						"&short=" + escape(shortn) + 
						"&icon=" + escape(icon) + 
						"&org=" + escape(org) + 
						"&dformat=" + escape(dformat) + 
						"&tformat=" + escape(tformat);
					fetchData(theSource, theData, success, failure);
				}
				function processPartyData(data){
					//alert(JSON.stringify(data));
					$('#admin_list_admin_container_wrapper').detach();
					$('#admin_modal_container').detach();
					var new_list = addElement("table", new Array("id|admin_list_container","class|admin_list_container 50percent display"));
					var new_list_head = addElement("thead", new Array("id|admin_list_admin_container_head","class|admin_list_admin_container_head"));
					var new_list_body = addElement("tbody", new Array("id|admin_list_admin_container_body","class|admin_list_admin_container_body"));
					var new_list_foot = addElement("tfoot", new Array("id|admin_list_admin_container_foot","class|admin_list_admin_container_foot"));
					var head_row = addElement("tr", new Array("id|admin_list_admin_container_head_row","class|admin_list_admin_container_head_row"));
					var headselect = addElement("input", new Array("type|checkbox","id|admin_list_container_head_select","class|admin_list_container_head_cb","name|admin_list_container_head_select"));
					var headid = addElement("th", new Array("id|admin_list_container_head_item-id","class|admin_list_container_head_item centered"));
					$(headid).append(headselect);
					var head_item_icon = addElement("th", new Array("id|admin_table_head_cell-id","class|admin_table_head_cell centered"));
					var head_item_name = addElement("th", new Array("id|admin_table_head_cell-name","class|admin_table_head_cell"),"Name");
					var head_item_short = addElement("th", new Array("id|admin_table_head_cell-nameonair","class|admin_table_head_cell"),"Short Name");
					var head_item_util1 = addElement("th", new Array("id|admin_table_head_cell-utility1","class|admin_table_head_cell centered"));
					var head_item_util2 = addElement("th", new Array("id|admin_table_head_cell-utility2","class|admin_table_head_cell centered"));
					$(head_row).append(headid, head_item_icon, head_item_name, head_item_short, head_item_util1, head_item_util2);
					$(new_list_head).append(head_row);
					$(new_list).append(new_list_head);
					var i = 1;
					$.each(data, function(index) {
						j=0;
						var list_row = addElement("tr", new Array("id|admin_list_admin_container_row","class|admin_list_admin_container_row gradeA " + (i%2==0)? 'even' : 'odd'));
						var list_item_select = addElement("input", new Array("type|checkbox","id|admin_list_container_select-" + data[index].party_id,"class|admin_table_row","name|admin_table_select[]"));
						var list_item_id = addElement("td", new Array("id|admin_list_container_item-" + data[index].party_id + "-" + i,"class|admin_list_container_item_cb centered","name|admin_list_container_item-" + data[index].party_id + "-" + i));
						$(list_item_id).append(list_item_select);
						var list_image_icon = addElement("img", new Array("src|" + data[index].party_icon, "id|admin_list_admin_container_image-amount","class|admin_list_admin_container_item"));
						var list_item_icon = addElement("td", new Array("id|admin_list_admin_container_item-party","class|admin_list_admin_container_item"));
						$(list_item_icon).append(list_image_icon);
						var list_item_name = addElement("td", new Array("id|admin_list_admin_container_item-name","class|admin_list_admin_container_item"),data[index].party_name);
						var list_item_short = addElement("td", new Array("id|admin_list_admin_container_item-short","class|admin_list_admin_container_item"),data[index].party_name_short);
						var list_item_edit = addElement("input", new Array("type|image","src|<?=$base_url?>ci/_common/images/admin-edit.png","id|admin_table_row_edit-" + data[index].party_id,"class|admin_table_cell centered","name|admin_table_row_edit"));
						$(list_item_edit).bind('click', function(){ editRecord(this, _theform, _formaction, 'editparty'); });
						var list_item_util1 = addElement("td", new Array("id|admin_table_cell-" + data[index].party_id,"class|admin_table_cell centered"));
						$(list_item_util1).append(list_item_edit);
						var list_item_delete = addElement("input", new Array("type|image","src|<?=$base_url?>ci/_common/images/admin-delete.png","id|admin_table_row_delete-" + data[index].party_id,"class|admin_table_cell centered","name|admin_table_row_delete"));
						$(list_item_delete).bind('click', function(){ deleteRecord(this, _theform, _formaction, 'removeparty'); });
						var list_item_util2 = addElement("td", new Array("id|admin_table_cell-" + data[index].party_id,"class|admin_table_cell centered"));
						$(list_item_util2).append(list_item_delete);
						$(list_row).append(list_item_id,list_item_icon,list_item_name,list_item_short,list_item_util1,list_item_util2);
						$(new_list_body).append(list_row);
						i++;
					});
					$(new_list).append(new_list_body);
					$(new_list).append(new_list_foot);
					$('#admin_list_inner_wrapper').append(new_list);
					$('#admin_list_container_head_row').clone().attr('id', 'admin_list_container_head_row-foot').appendTo('#admin_list_admin_container_foot');
					$('#admin_list_container').dataTable({
						"aaSorting": [[ 2, "asc" ]],
						"iDisplayLength": 10,
						"bProcessing": true,
						"sPaginationType": "full_numbers",
						"bStateSave": true,
						"bJQueryUI": true
					}).hide().fadeIn("slow");
				}
				function processPartyError(data){
					alert(data);
				}
			});
		</script>
		<?php
		break;
	}
	case 'addparty':
	case 'editparty':
	{
		// PARTY ADD / EDIT
		$party_name			= "";
		$party_name_short	= "";
		$party_icon			= "";
		if($party_id) { 
			foreach($parties as $party) {
				$party_id			= $party['party_id'];
				$party_name			= $party['party_name'];
				$party_name_short	= $party['party_name_short'];
				$party_icon			= $party['party_icon'];
			} 
		}
		$row=0;
		?>
		<form id="admin_action_form" action="<?=$base_url?>index.php/election2012/manageElection/saveparty" enctype="multipart/form-data" method="POST">
			<div id="admin_cp_wrapper" class="admin_cp_wrapper_50">
				<span id="cp_back" class="cp_back"><--Back</span>
				<input type="submit" id="admin_btn_edit" class="form_button" name="admin_btn_edit" value="Save" tabindex="3"/>
				<input type="hidden" id="admin_redirect" name="admin_redirect" value="" />
			</div>
			<table id="admin_table" class="admin_table_50" cellpadding="0" cellspacing="0">
				<tr id="admin_table_head_row" class="admin_table_head_row"><th>Field</th><th>Value</th></tr>
				<tr id="admin_table_row-" class="admin_table_row <?php echo (($row%2)? ' even' : ' odd'); $row++; ?>"><td class="admin_table_cell label">Party Name</td><td class="admin_table_cell field">
					<input type="text" id="party_name" class="form_text" name="party_name" value="<?=(($party_name)? $party_name : '')?>" tabindex="1"/>
					</td></tr>
				<tr id="admin_table_row-" class="admin_table_row <?php echo (($row%2)? ' even' : ' odd'); $row++; ?>"><td class="admin_table_cell label">Party Name (Short)</td><td class="admin_table_cell field">
					<input type="text" id="party_name_short" class="form_text" name="party_name_short" value="<?=(($party_name_short)? $party_name_short : '')?>" tabindex="2"/>
					</td></tr>
				<tr id="admin_table_row-" class="admin_table_row <?php echo (($row%2)? ' even' : ' odd'); $row++; ?>"><td class="admin_table_cell label">Party Icon</td><td class="admin_table_cell field">
					<input type="text" id="party_icon" class="form_text" name="party_icon" value="<?=(($party_icon)? $party_icon : '')?>" tabindex="2"/>
					</td></tr>
			</table>
			<input type="hidden" id="party_id" name="party_id" value="<?=(($party_id)? $party_id : '')?>"/>
		</form>
		<script language="JavaScript">
			$(document).ready(function(){
				var formaction = "<?=$base_url?>index.php/election2012/manageElection";
				$('#admin_redirect').val(document.URL);
				$('#admin_selection_party').addClass('selected');
				$('#admin_cp_station').bind("change", function() {
					var newstation = $('#admin_cp_station').find(':selected').val();
					$('#admin_action_form').attr('action', formaction + '/setstation/' + newstation);
					$('#admin_action_form').submit();
				});
			});
		</script>
		<?php
		break;
	}
	// Race status functions
	case 'racestatus':
	{ ?>
		<form id="admin_action_form" action="<?=$base_url?>index.php/election2012/manageElection" enctype="multipart/form-data" method="POST">
			<div id="admin_cp_wrapper" class="admin_cp_wrapper_50">
				<span id="cp_back" class="cp_back"><--Back</span>
				<select id="admin_cp_station" class="form_select" name="admin_cp_station"><?php
				$stations = (($wish_user)? ((array_key_exists('wish_stations', $wish_user))? $wish_user['wish_stations'] : '') : '');
				if($stations) {
					foreach($stations as $key=>$value) {
						?><option value="<?=$key?>"<?=(($key==$station_id)? ' selected=\"selected\"' : '')?>><?=$value?></option><?php
					}
				}
				?></select>
				<select id="op_select" class="op_select" name="op_select">
					<option value="addracestatus">Add</option>
					<option value="togglecomplete">Toggle Completed</option>
					<option value="toggleflag">Toggle Flagged</option>
					<option value="removeracestatus">Delete</option>
				</select>
				<input type="submit" id="admin_action_form_go" class="admin_action_form_btn" name="admin_action_form_go" value="GO">
				<input type="hidden" id="admin_table_rowselect_hidden" name="admin_table_rowselect_hidden" value="">
				<input type="hidden" id="admin_redirect" name="admin_redirect" value="" />
				<input type="hidden" id="admin_paging" name="admin_paging" value="" />
				<input type="hidden" id="admin_total_records" name="admin_total_records" value="" />
			</div>
			<div id="admin_list_outter_wrapper" class="admin_list_outter_wrapper_50">
				<div id="admin_list_inner_wrapper" class="admin_list_inner_wrapper container">
					
				</div>
			</div>
		</form>
		<script language="JavaScript">
			var _theform = "";
			var _formaction = "";
			$(document).ready(function(){
				$("#stdwww").addClass('gone');
				_theform = $('#admin_action_form');
				_formaction = "<?=$base_url?>index.php/election2012/manageElection";
				$('#admin_redirect').val(document.URL);
				$('#admin_selection_racestatus').addClass('selected');
				$('#admin_action_form_go').focus();
				$('#admin_action_form_go').bind('click', function(e){
					e.preventDefault();
					var selectaction = $('#op_select').find(':selected').val();
					var selectedboxes = $('input[id^="admin_list_container_select-"]:checked').map(function() {
						return this.id.split("-")[1];
					}).get().join(',');
					$('#admin_table_rowselect_hidden').val(selectedboxes);
					$('#admin_action_form').attr('action', _formaction + '/' + selectaction);
					switch(selectaction)
					{
						case 'togglecomplete': { $('#admin_action_form').submit(); break; }
						case 'toggleflag': { $('#admin_action_form').submit(); break; }
						case 'removeracestatus':
						{
							if(confirm('You are about to delete ' + $('input[id^="admin_list_container_select-"]:checked').length + ' records from the database. This action cannot be undone. Are you sure you want to continue?')) {
								$('#admin_action_form').submit();
							}
							break;
						}
						default:
						{
							$('#admin_action_form').attr('action', _formaction + '/addracestatus');
							$('#admin_action_form').submit();
						}
					}
				});
				$('#admin_cp_station').bind("change", function() {
					var newstation = $('#admin_cp_station').find(':selected').val();
					$('#admin_action_form').attr('action', formaction + '/setstation/' + newstation);
					$('#admin_action_form').submit();
				});
				getRaceStatusData('','','','',<?=$station_id?>,'','',processRaceStatusData,processRaceStatusError);
				function getRaceStatusData(id,name,complete,flag,org,dformat,tformat,success,failure) {
					var theSource = "<?=$base_url?>index.php/election2012/fetchRaceStatusData";
					var theData = "id=" + escape(id) + 
						"&name=" + escape(name) + 
						"&complete=" + escape(complete) + 
						"&flag=" + escape(flag) + 
						"&org=" + escape(org) + 
						"&dformat=" + escape(dformat) + 
						"&tformat=" + escape(tformat);
					fetchData(theSource, theData, success, failure);
				}
				function processRaceStatusData(data){
					//alert(JSON.stringify(data));
					$('#admin_list_admin_container_wrapper').detach();
					$('#admin_modal_container').detach();
					var new_list = addElement("table", new Array("id|admin_list_container","class|admin_list_container 50percent display"));
					var new_list_head = addElement("thead", new Array("id|admin_list_admin_container_head","class|admin_list_admin_container_head"));
					var new_list_body = addElement("tbody", new Array("id|admin_list_admin_container_body","class|admin_list_admin_container_body"));
					var new_list_foot = addElement("tfoot", new Array("id|admin_list_admin_container_foot","class|admin_list_admin_container_foot"));
					var head_row = addElement("tr", new Array("id|admin_list_admin_container_head_row","class|admin_list_admin_container_head_row"));
					var headselect = addElement("input", new Array("type|checkbox","id|admin_list_container_head_select","class|admin_list_container_head_cb","name|admin_list_container_head_select"));
					var headid = addElement("th", new Array("id|admin_list_container_head_item-id","class|admin_list_container_head_item centered"));
					$(headid).append(headselect);
					var head_item_name = addElement("th", new Array("id|admin_table_head_cell-name","class|admin_table_head_cell"),"Name");
					var head_item_complete = addElement("th", new Array("id|admin_table_head_cell-complete","class|admin_table_head_cell centered"),"Complete?");
					var head_item_flag = addElement("th", new Array("id|admin_table_head_cell-flag","class|admin_table_head_cell centered"),"Flagged?");
					var head_item_util1 = addElement("th", new Array("id|admin_table_head_cell-utility1","class|admin_table_head_cell centered"));
					var head_item_util2 = addElement("th", new Array("id|admin_table_head_cell-utility2","class|admin_table_head_cell centered"));
					$(head_row).append(headid, head_item_name, head_item_complete, head_item_flag, head_item_util1, head_item_util2);
					$(new_list_head).append(head_row);
					$(new_list).append(new_list_head);
					var i = 1;
					$.each(data, function(index) {
						j=0;
						var list_row = addElement("tr", new Array("id|admin_list_admin_container_row","class|admin_list_admin_container_row gradeA " + (i%2==0)? 'even' : 'odd'));
						var list_item_select = addElement("input", new Array("type|checkbox","id|admin_list_container_select-" + data[index].race_status_id,"class|admin_table_row","name|admin_table_select[]"));
						var list_item_id = addElement("td", new Array("id|admin_list_container_item-" + data[index].race_status_id + "-" + i,"class|admin_list_container_item_cb centered","name|admin_list_container_item-" + data[index].race_status_id + "-" + i));
						$(list_item_id).append(list_item_select);
						var list_item_name = addElement("td", new Array("id|admin_list_admin_container_item-name","class|admin_list_admin_container_item"),data[index].race_status_name);
						
						var list_image_complete = addElement("img", new Array("src|<?=$base_url?>ci/_common/images/" + ((data[index].race_status_complete==1)? 'admin-check.png' : 'admin-check-false.png'), "id|admin_list_admin_container_image-amount","class|admin_list_admin_container_item"));
						$(list_image_complete).bind('click', function(){ toggleRecord(this, _theform, _formaction, 'togglecomplete'); });
						var list_item_complete = addElement("td", new Array("id|admin_list_admin_container_item-complete","class|admin_list_admin_container_item centered"));
						$(list_item_complete).append(list_image_complete);
						
						var list_image_flag = addElement("img", new Array("src|<?=$base_url?>ci/_common/images/" + ((data[index].race_status_flag==1)? 'admin-check.png' : 'admin-check-false.png'), "id|admin_list_admin_container_image-amount","class|admin_list_admin_container_item"));
						$(list_image_flag).bind('click', function(){ toggleRecord(this, _theform, _formaction, 'toggleflag'); });
						var list_item_flag = addElement("td", new Array("id|admin_list_admin_container_item-flag","class|admin_list_admin_container_item centered"));
						$(list_item_flag).append(list_image_flag);
						
						var list_item_edit = addElement("input", new Array("type|image","src|<?=$base_url?>ci/_common/images/admin-edit.png","id|admin_table_row_edit-" + data[index].race_status_id,"class|admin_table_cell centered","name|admin_table_row_edit"));
						$(list_item_edit).bind('click', function(){ editRecord(this, _theform, _formaction, 'editracestatus'); });
						var list_item_util1 = addElement("td", new Array("id|admin_table_cell-" + data[index].race_status_id,"class|admin_table_cell centered"));
						$(list_item_util1).append(list_item_edit);
						var list_item_delete = addElement("input", new Array("type|image","src|<?=$base_url?>ci/_common/images/admin-delete.png","id|admin_table_row_delete-" + data[index].race_status_id,"class|admin_table_cell centered","name|admin_table_row_delete"));
						$(list_item_delete).bind('click', function(){ deleteRecord(this, _theform, _formaction, 'removeracestatus'); });
						var list_item_util2 = addElement("td", new Array("id|admin_table_cell-" + data[index].race_status_id,"class|admin_table_cell centered"));
						$(list_item_util2).append(list_item_delete);
						$(list_row).append(list_item_id,list_item_name,list_item_complete,list_item_flag,list_item_util1,list_item_util2);
						$(new_list_body).append(list_row);
						i++;
					});
					$(new_list).append(new_list_body);
					$(new_list).append(new_list_foot);
					$('#admin_list_inner_wrapper').append(new_list);
					$('#admin_list_container_head_row').clone().attr('id', 'admin_list_container_head_row-foot').appendTo('#admin_list_admin_container_foot');
					$('#admin_list_container').dataTable({
						"aaSorting": [[ 1, "asc" ]],
						"iDisplayLength": 10,
						"bProcessing": true,
						"sPaginationType": "full_numbers",
						"bStateSave": true,
						"bJQueryUI": true
					}).hide().fadeIn("slow");
				}
				function processRaceStatusError(data){
					alert(data);
				}
			});
		</script>
		<?php
		break;
	}
	case 'addracestatus':
	case 'editracestatus':
	{
		// RACE STATUS ADD / EDIT
		$race_status_name		= "";
		$race_status_complete	= "";
		$race_status_flag		= "";
		if($race_status_id) { 
			foreach($statuses as $status) {
				$race_status_id			= $status['race_status_id'];
				$race_status_name		= $status['race_status_name'];
				$race_status_complete	= (($status['race_status_complete'])? 1 : 0);
				$race_status_flag		= (($status['race_status_flag'])? 1 : 0);
			} 
		}
		$row=0;
		?>
		<form id="admin_action_form" action="<?=$base_url?>index.php/election2012/manageElection/saveracestatus" enctype="multipart/form-data" method="POST">
			<div id="admin_cp_wrapper" class="admin_cp_wrapper_50">
				<span id="cp_back" class="cp_back"><--Back</span>
				<input type="submit" id="admin_btn_edit" class="form_button" name="admin_btn_edit" value="Save" tabindex="3"/>
				<input type="hidden" id="admin_redirect" name="admin_redirect" value="" />
			</div>
			<table id="admin_table" class="admin_table_50" cellpadding="0" cellspacing="0">
				<tr id="admin_table_head_row" class="admin_table_head_row"><th>Field</th><th>Value</th></tr>
				<tr id="admin_table_row-" class="admin_table_row <?php echo (($row%2)? ' even' : ' odd'); $row++; ?>"><td class="admin_table_cell label">Status Name</td><td class="admin_table_cell field">
					<input type="text" id="race_status_name" class="form_text" name="race_status_name" value="<?=(($race_status_name)? $race_status_name : '')?>" tabindex="2"/>
					</td></tr>
				<tr id="admin_table_row-" class="admin_table_row <?php echo (($row%2)? ' even' : ' odd'); $row++; ?>"><td class="admin_table_cell label">Complete?</td><td class="admin_table_cell field">
					<input type="checkbox" id="race_status_complete" class="form_text" name="race_status_complete" <?=(($race_status_complete==1)? ' checked=checked' : '')?>" tabindex="2"/>
					</td></tr>
				<tr id="admin_table_row-" class="admin_table_row <?php echo (($row%2)? ' even' : ' odd'); $row++; ?>"><td class="admin_table_cell label">Flagged</td><td class="admin_table_cell field">
					<input type="checkbox" id="race_status_flag" class="form_text" name="race_status_flag" <?=(($race_status_flag==1)? ' checked=checked' : '')?>" tabindex="2"/>
					</td></tr>
			</table>
			<input type="hidden" id="race_status_id" name="race_status_id" value="<?=(($race_status_id)? $race_status_id : '')?>"/>
		</form>
		<script language="JavaScript">
			$(document).ready(function(){
				var formaction = "<?=$base_url?>index.php/election2012/manageElection";
				$('#admin_redirect').val(document.URL);
				$('#admin_selection_racestatus').addClass('selected');
				$('#admin_cp_station').bind("change", function() {
					var newstation = $('#admin_cp_station').find(':selected').val();
					$('#admin_action_form').attr('action', formaction + '/setstation/' + newstation);
					$('#admin_action_form').submit();
				});
			});
		</script>
		<?php
		break;
	}
	// Race functions
	case 'race':
	{ ?>
		<form id="admin_action_form" action="<?=$base_url?>index.php/election2012/manageElection" enctype="multipart/form-data" method="POST">
			<div id="admin_cp_wrapper" class="admin_cp_wrapper_75">
				<span id="cp_back" class="cp_back"><--Back</span>
				<select id="admin_cp_station" class="form_select" name="admin_cp_station"><?php
				$stations = (($wish_user)? ((array_key_exists('wish_stations', $wish_user))? $wish_user['wish_stations'] : '') : '');
				if($stations) {
					foreach($stations as $key=>$value) {
						?><option value="<?=$key?>"<?=(($key==$station_id)? ' selected=\"selected\"' : '')?>><?=$value?></option><?php
					}
				}
				?></select>
				<select id="op_select" class="op_select" name="op_select">
					<option value="addrace">Add</option>
					<option value="removerace">Delete</option>
				</select>
				<input type="submit" id="admin_action_form_go" class="admin_action_form_btn" name="admin_action_form_go" value="GO">
				<input type="hidden" id="admin_table_rowselect_hidden" name="admin_table_rowselect_hidden" value="">
				<input type="hidden" id="admin_redirect" name="admin_redirect" value="" />
				<input type="hidden" id="admin_paging" name="admin_paging" value="" />
				<input type="hidden" id="admin_total_records" name="admin_total_records" value="" />
			</div>
			<div id="admin_list_outter_wrapper" class="admin_list_outter_wrapper_75">
				<div id="admin_list_inner_wrapper" class="admin_list_inner_wrapper container">
					
				</div>
			</div>
		</form>
		<script language="JavaScript">
			var _theform = "";
			var _formaction = "";
			$(document).ready(function(){
				$("#stdwww").addClass('gone');
				_theform = $('#admin_action_form');
				_formaction = "<?=$base_url?>index.php/election2012/manageElection";
				$('#admin_redirect').val(document.URL);
				$('#admin_selection_race').addClass('selected');
				$('#admin_action_form_go').focus();
				$('#admin_action_form_go').bind('click', function(e){
					e.preventDefault();
					var selectaction = $('#op_select').find(':selected').val();
					var selectedboxes = $('input[id^="admin_list_container_select-"]:checked').map(function() {
						return this.id.split("-")[1];
					}).get().join(',');
					$('#admin_table_rowselect_hidden').val(selectedboxes);
					$('#admin_action_form').attr('action', _formaction + '/' + selectaction);
					switch(selectaction)
					{
						case 'removerace':
						{
							if(confirm('You are about to delete ' + $('input[id^="admin_list_container_select-"]:checked').length + ' records from the database. This action cannot be undone. Are you sure you want to continue?')) {
								$('#admin_action_form').submit();
							}
							break;
						}
						default:
						{
							$('#admin_action_form').attr('action', _formaction + '/addrace');
							$('#admin_action_form').submit();
						}
					}
				});
				$('#admin_cp_station').bind("change", function() {
					var newstation = $('#admin_cp_station').find(':selected').val();
					$('#admin_action_form').attr('action', formaction + '/setstation/' + newstation);
					$('#admin_action_form').submit();
				});
				getRaceData('','','','','','','','','','','','','','',<?=$station_id?>,'','',processRaceData,processRaceError);
				function getRaceData(rid,rsid,rrid,rrfip,title,shortn,name,rbegin,rend,report,percent,total,tpercent,source,org,dformat,tformat,success,failure) {
					var theSource = "<?=$base_url?>index.php/election2012/fetchRaceData";
					var theData = "rid=" + escape(rid) + 
						"&rsid=" + escape(rsid) + 
						"&rrid=" + escape(rrid) + 
						"&rrfip=" + escape(rrfip) + 
						"&title=" + escape(title) + 
						"&short=" + escape(shortn) + 
						"&name=" + escape(name) + 
						"&rbegin=" + escape(rbegin) + 
						"&rend=" + escape(rend) + 
						"&report=" + escape(report) + 
						"&percent=" + escape(percent) + 
						"&total=" + escape(total) + 
						"&tpercent=" + escape(tpercent) + 
						"&source=" + escape(source) + 
						"&org=" + escape(org) + 
						"&dformat=" + escape(dformat) + 
						"&tformat=" + escape(tformat);
					fetchData(theSource, theData, success, failure);
				}
				function processRaceData(data){
					//alert(JSON.stringify(data));
					$('#admin_list_admin_container_wrapper').detach();
					$('#admin_modal_container').detach();
					var new_list = addElement("table", new Array("id|admin_list_container","class|admin_list_container 75percent display"));
					var new_list_head = addElement("thead", new Array("id|admin_list_admin_container_head","class|admin_list_admin_container_head"));
					var new_list_body = addElement("tbody", new Array("id|admin_list_admin_container_body","class|admin_list_admin_container_body"));
					var new_list_foot = addElement("tfoot", new Array("id|admin_list_admin_container_foot","class|admin_list_admin_container_foot"));
					var head_row = addElement("tr", new Array("id|admin_list_admin_container_head_row","class|admin_list_admin_container_head_row"));
					var headselect = addElement("input", new Array("type|checkbox","id|admin_list_container_head_select","class|admin_list_container_head_cb","name|admin_list_container_head_select"));
					var headid = addElement("th", new Array("id|admin_list_container_head_item-id","class|admin_list_container_head_item centered"));
					$(headid).append(headselect);
					var head_item_id = addElement("th", new Array("id|admin_table_head_cell-id","class|admin_table_head_cell centered"),"ID");
					var head_item_title = addElement("th", new Array("id|admin_table_head_cell-title","class|admin_table_head_cell centered"),"Title");
					var head_item_short = addElement("th", new Array("id|admin_table_head_cell-short","class|admin_table_head_cell"),"Short Title");
					var head_item_status = addElement("th", new Array("id|admin_table_head_cell-status","class|admin_table_head_cell"),"Status");
					var head_item_util1 = addElement("th", new Array("id|admin_table_head_cell-utility1","class|admin_table_head_cell centered"));
					var head_item_util2 = addElement("th", new Array("id|admin_table_head_cell-utility2","class|admin_table_head_cell centered"));
					$(head_row).append(headid, head_item_id, head_item_title, head_item_short, head_item_status, head_item_util1, head_item_util2);
					$(new_list_head).append(head_row);
					$(new_list).append(new_list_head);
					var i = 1;
					$.each(data, function(index) {
						j=0;
						var list_row = addElement("tr", new Array("id|admin_list_admin_container_row","class|admin_list_admin_container_row gradeA " + (i%2==0)? 'even' : 'odd'));
						var list_item_select = addElement("input", new Array("type|checkbox","id|admin_list_container_select-" + data[index].race_id,"class|admin_table_row","name|admin_table_select[]"));
						var list_item_id = addElement("td", new Array("id|admin_list_container_item-" + data[index].race_id + "-" + i,"class|admin_list_container_item_cb centered","name|admin_list_container_item-" + data[index].race_id + "-" + i));
						$(list_item_id).append(list_item_select);
						var list_item_showid = addElement("td", new Array("id|admin_list_admin_container_item-id","class|admin_list_admin_container_item centered"),data[index].race_id);
						var list_item_title = addElement("td", new Array("id|admin_list_admin_container_item-title","class|admin_list_admin_container_item"),data[index].race_title);
						var list_item_short = addElement("td", new Array("id|admin_list_admin_container_item-short","class|admin_list_admin_container_item"),data[index].race_title_short);
						var list_item_status = addElement("td", new Array("id|admin_list_admin_container_item-status","class|admin_list_admin_container_item centered"),data[index].race_status_id);
						
						var list_item_edit = addElement("input", new Array("type|image","src|<?=$base_url?>ci/_common/images/admin-edit.png","id|admin_table_row_edit-" + data[index].race_id,"class|admin_table_cell centered","name|admin_table_row_edit"));
						$(list_item_edit).bind('click', function(){ editRecord(this, _theform, _formaction, 'editrace'); });
						var list_item_util1 = addElement("td", new Array("id|admin_table_cell-" + data[index].racte_id,"class|admin_table_cell centered"));
						$(list_item_util1).append(list_item_edit);
						var list_item_delete = addElement("input", new Array("type|image","src|<?=$base_url?>ci/_common/images/admin-delete.png","id|admin_table_row_delete-" + data[index].race_id,"class|admin_table_cell centered","name|admin_table_row_delete"));
						$(list_item_delete).bind('click', function(){ deleteRecord(this, _theform, _formaction, 'removerace'); });
						var list_item_util2 = addElement("td", new Array("id|admin_table_cell-" + data[index].race_id,"class|admin_table_cell centered"));
						$(list_item_util2).append(list_item_delete);
						$(list_row).append(list_item_id,list_item_showid,list_item_title,list_item_short,list_item_status,list_item_util1,list_item_util2);
						$(new_list_body).append(list_row);
						i++;
					});
					$(new_list).append(new_list_body);
					$(new_list).append(new_list_foot);
					$('#admin_list_inner_wrapper').append(new_list);
					$('#admin_list_container_head_row').clone().attr('id', 'admin_list_container_head_row-foot').appendTo('#admin_list_admin_container_foot');
					$('#admin_list_container').dataTable({
						"aaSorting": [[ 2, "asc" ]],
						"iDisplayLength": 25,
						"bProcessing": true,
						"sPaginationType": "full_numbers",
						"bStateSave": true,
						"bJQueryUI": true
					}).hide().fadeIn("slow");
				}
				function processRaceError(data){
					alert(data);
				}
			});
		</script>
		<?php
		break;
	}
	case 'addrace':
	case 'editrace':
	{
		// RACE STATUS ADD / EDIT
		$race_status_id		= "";
		//$county_fip			= "";
		$race_title			= "";
		$race_title_short	= "";
		$race_name			= "";
		if($race_id) { 
			foreach($races as $race) {
				$race_id			= $race['race_id'];
				$race_status_id		= $race['race_status_id'];
				//$county_fip			= $status['county_fip'];
				$race_title			= $race['race_title'];
				$race_title_short	= $race['race_title_short'];
				$race_name			= $race['race_name'];
			} 
		}
		$row=0;
		?>
		<form id="admin_action_form" action="<?=$base_url?>index.php/election2012/manageElection/saverace" enctype="multipart/form-data" method="POST">
			<div id="admin_cp_wrapper" class="admin_cp_wrapper_50">
				<span id="cp_back" class="cp_back"><--Back</span>
				<input type="submit" id="admin_btn_edit" class="form_button" name="admin_btn_edit" value="Save" tabindex="3"/>
				<input type="hidden" id="admin_redirect" name="admin_redirect" value="" />
			</div>
			<table id="admin_table" class="admin_table_50" cellpadding="0" cellspacing="0">
				<tr id="admin_table_head_row" class="admin_table_head_row"><th>Field</th><th>Value</th></tr>
				<tr id="admin_table_row-" class="admin_table_row <?php echo (($row%2)? ' even' : ' odd'); $row++; ?>"><td class="admin_table_cell label">Status</td><td class="admin_table_cell field">
					<select id="race_status_id" class="select" name="race_status_id" tabindex="6">
						<option value="0">Select Status</option>
						<?php
						foreach($status_options as $status_option) { ?>
							<option value="<?=$status_option['race_status_id']?>"<?=(($race_status_id==$status_option['race_status_id'])? ' selected=\"selected\"' : '')?>><?=$status_option['race_status_name']?></option>
						<?php } ?>
					</select>
					</td></tr>
				<tr id="admin_table_row-" class="admin_table_row <?php echo (($row%2)? ' even' : ' odd'); $row++; ?>"><td class="admin_table_cell label">Title</td><td class="admin_table_cell field">
					<input type="text" id="race_title" class="form_text" name="race_title" value="<?=(($race_title)? $race_title : '')?>" tabindex="2"/>
					</td></tr>
				<tr id="admin_table_row-" class="admin_table_row <?php echo (($row%2)? ' even' : ' odd'); $row++; ?>"><td class="admin_table_cell label">Short Title</td><td class="admin_table_cell field">
					<input type="text" id="race_title_short" class="form_text" name="race_title_short" value="<?=(($race_title_short)? $race_title_short : '')?>" tabindex="2"/>
					</td></tr>
				<tr id="admin_table_row-" class="admin_table_row <?php echo (($row%2)? ' even' : ' odd'); $row++; ?>"><td class="admin_table_cell label">Name</td><td class="admin_table_cell field">
					<input type="text" id="race_name" class="form_text" name="race_name" value="<?=(($race_name)? $race_name : '')?>" tabindex="2"/>
					</td></tr>
			</table>
			<input type="hidden" id="race_id" name="race_id" value="<?=(($race_id)? $race_id : '')?>"/>
		</form>
		<script language="JavaScript">
			$(document).ready(function(){
				var formaction = "<?=$base_url?>index.php/election2012/manageElection";
				$('#admin_redirect').val(document.URL);
				$('#admin_selection_race').addClass('selected');
				$('#admin_cp_station').bind("change", function() {
					var newstation = $('#admin_cp_station').find(':selected').val();
					$('#admin_action_form').attr('action', formaction + '/setstation/' + newstation);
					$('#admin_action_form').submit();
				});
			});
		</script>
		<?php
		break;
	}
	// Result functions
	case 'assignracefips':
	{
		$race_status_id		= "";
		$race_title			= "";
		$race_title_short	= "";
		$race_name			= "";
		if($race_id) { 
			foreach($races as $race) {
				$race_id			= $race['race_id'];
				$race_status_id		= $race['race_status_id'];
				$race_title			= $race['race_title'];
				$race_title_short	= $race['race_title_short'];
				$race_name			= $race['race_name'];
			} 
		}
		$row=0;
		?>
		<form id="admin_action_form" action="<?=$base_url?>index.php/election2012/manageElection/saveracefips" enctype="multipart/form-data" method="POST">
			<div id="admin_cp_wrapper" class="admin_cp_wrapper_50">
				<span id="cp_back" class="cp_back"><--Back</span>
				<input type="submit" id="admin_btn_edit" class="form_button" name="admin_btn_edit" value="Save" tabindex="3"/>
				<input type="hidden" id="admin_redirect" name="admin_redirect" value="" />
			</div>
			<table id="admin_table" class="admin_table_50" cellpadding="0" cellspacing="0">
				<tr id="admin_table_head_row" class="admin_table_head_row"><th>Race</th><th>FIPS</th></tr>
				<tr id="admin_table_row-" class="admin_table_row <?php echo (($row%2)? ' even' : ' odd'); $row++; ?>">
					<td class="admin_table_cell label"><?=$race_title?></td>
					<td class="admin_table_cell field">
					<select id="county_fip" class="select" name="county_fip[]" multiple="multiple" size="10" tabindex="6">
						<?php
						foreach($county_options as $county_option) { ?>
							<option value="<?=$county_option['county_fip']?>"<?=(($county_option['county_selected']==1)? ' selected=selected' : '')?>>[<?=$county_option['county_fip']?>] | <?=$county_option['state_code']?> | <?=$county_option['county_name']?></option>
						<?php } ?>
					</select>
					</td></tr>
			</table>
			<input type="hidden" id="race_id" name="race_id" value="<?=(($race_id)? $race_id : '')?>"/>
		</form>
		<script language="JavaScript">
			$(document).ready(function(){
				var formaction = "<?=$base_url?>index.php/election2012/manageElection";
				$('#admin_redirect').val(document.URL);
				$('#admin_selection_race').addClass('selected');
				$('#admin_cp_station').bind("change", function() {
					var newstation = $('#admin_cp_station').find(':selected').val();
					$('#admin_action_form').attr('action', formaction + '/setstation/' + newstation);
					$('#admin_action_form').submit();
				});
			});
		</script>
		<?php
		break;
	}
	// Candidate functions
	case 'candidate':
	{ ?>
		<form id="admin_action_form" action="<?=$base_url?>index.php/election2012/manageElection" enctype="multipart/form-data" method="POST">
			<div id="admin_cp_wrapper" class="admin_cp_wrapper_75">
				<span id="cp_back" class="cp_back"><--Back</span>
				<select id="admin_cp_station" class="form_select" name="admin_cp_station"><?php
				$stations = (($wish_user)? ((array_key_exists('wish_stations', $wish_user))? $wish_user['wish_stations'] : '') : '');
				if($stations) {
					foreach($stations as $key=>$value) {
						?><option value="<?=$key?>"<?=(($key==$station_id)? ' selected=\"selected\"' : '')?>><?=$value?></option><?php
					}
				}
				?></select>
				<select id="op_select" class="op_select" name="op_select">
					<option value="addcandidate">Add</option>
					<option value="mergecandidates">Merge</option>
					<option value="removecandidate">Delete</option>
				</select>
				<input type="submit" id="admin_action_form_go" class="admin_action_form_btn" name="admin_action_form_go" value="GO">
				<input type="hidden" id="admin_table_rowselect_hidden" name="admin_table_rowselect_hidden" value="">
				<input type="hidden" id="admin_redirect" name="admin_redirect" value="" />
				<input type="hidden" id="admin_paging" name="admin_paging" value="" />
				<input type="hidden" id="admin_total_records" name="admin_total_records" value="" />
			</div>
			<div id="admin_list_outter_wrapper" class="admin_list_outter_wrapper_75">
				<div id="admin_list_inner_wrapper" class="admin_list_inner_wrapper container">
					
				</div>
			</div>
		</form>
		<script language="JavaScript">
			var _theform = "";
			var _formaction = "";
			$(document).ready(function(){
				$("#stdwww").addClass('gone');
				_theform = $('#admin_action_form');
				_formaction = "<?=$base_url?>index.php/election2012/manageElection";
				$('#admin_redirect').val(document.URL);
				$('#admin_selection_candidate').addClass('selected');
				$('#admin_action_form_go').focus();
				$('#admin_action_form_go').bind('click', function(e){
					e.preventDefault();
					var selectaction = $('#op_select').find(':selected').val();
					var selectedboxes = $('input[id^="admin_list_container_select-"]:checked').map(function() {
						return this.id.split("-")[1];
					}).get().join(',');
					$('#admin_table_rowselect_hidden').val(selectedboxes);
					$('#admin_action_form').attr('action', _formaction + '/' + selectaction);
					switch(selectaction)
					{
						case 'mergecandidates': { $('#admin_action_form').submit(); break; }
						case 'removecandidate':
						{
							if(confirm('You are about to delete ' + $('input[id^="admin_list_container_select-"]:checked').length + ' records from the database. This action cannot be undone. Are you sure you want to continue?')) {
								$('#admin_action_form').submit();
							}
							break;
						}
						default:
						{
							$('#admin_action_form').attr('action', _formaction + '/addrace');
							$('#admin_action_form').submit();
						}
					}
				});
				$('#admin_cp_station').bind("change", function() {
					var newstation = $('#admin_cp_station').find(':selected').val();
					$('#admin_action_form').attr('action', formaction + '/setstation/' + newstation);
					$('#admin_action_form').submit();
				});
				getCandidateData('','','','','','','','','','',<?=$station_id?>,'','',processCandidateData,processCandidateError);
				function getCandidateData(id,rid,pid,pname,first,mi,last,incumbent,winner,rname,org,dformat,tformat,success,failure) {
					var theSource = "<?=$base_url?>index.php/election2012/fetchCandidateData";
					var theData = "id=" + escape(id) + 
						"&rid=" + escape(rid) + 
						"&pid=" + escape(pid) + 
						"&pname=" + escape(pname) + 
						"&first=" + escape(first) + 
						"&mi=" + escape(mi) + 
						"&last=" + escape(last) + 
						"&incumbent=" + escape(incumbent) + 
						"&winner=" + escape(winner) + 
						"&rname=" + escape(rname) + 
						"&org=" + escape(org) + 
						"&dformat=" + escape(dformat) + 
						"&tformat=" + escape(tformat);
					fetchData(theSource, theData, success, failure);
				}
				function processCandidateData(data){
					//alert(JSON.stringify(data));
					$('#admin_list_admin_container_wrapper').detach();
					$('#admin_modal_container').detach();
					var new_list = addElement("table", new Array("id|admin_list_container","class|admin_list_container 75percent display"));
					var new_list_head = addElement("thead", new Array("id|admin_list_admin_container_head","class|admin_list_admin_container_head"));
					var new_list_body = addElement("tbody", new Array("id|admin_list_admin_container_body","class|admin_list_admin_container_body"));
					var new_list_foot = addElement("tfoot", new Array("id|admin_list_admin_container_foot","class|admin_list_admin_container_foot"));
					var head_row = addElement("tr", new Array("id|admin_list_admin_container_head_row","class|admin_list_admin_container_head_row"));
					var headselect = addElement("input", new Array("type|checkbox","id|admin_list_container_head_select","class|admin_list_container_head_cb","name|admin_list_container_head_select"));
					var headid = addElement("th", new Array("id|admin_list_container_head_item-id","class|admin_list_container_head_item centered"));
					$(headid).append(headselect);
					var head_item_id = addElement("th", new Array("id|admin_table_head_cell-id","class|admin_table_head_cell centered"),"ID");
					var head_item_party = addElement("th", new Array("id|admin_table_head_cell-party","class|admin_table_head_cell"),"Party");
					var head_item_first = addElement("th", new Array("id|admin_table_head_cell-first","class|admin_table_head_cell"),"First");
					var head_item_last = addElement("th", new Array("id|admin_table_head_cell-last","class|admin_table_head_cell"),"Last");
					var head_item_race = addElement("th", new Array("id|admin_table_head_cell-race","class|admin_table_head_cell"),"Running For");
					var head_item_util1 = addElement("th", new Array("id|admin_table_head_cell-utility1","class|admin_table_head_cell centered"));
					var head_item_util2 = addElement("th", new Array("id|admin_table_head_cell-utility2","class|admin_table_head_cell centered"));
					$(head_row).append(headid, head_item_id, head_item_party, head_item_first, head_item_last, head_item_race, head_item_util1, head_item_util2);
					$(new_list_head).append(head_row);
					$(new_list).append(new_list_head);
					var i = 1;
					$.each(data, function(index) {
						j=0;
						var list_row = addElement("tr", new Array("id|admin_list_admin_container_row","class|admin_list_admin_container_row gradeA " + (i%2==0)? 'even' : 'odd'));
						var list_item_select = addElement("input", new Array("type|checkbox","id|admin_list_container_select-" + data[index].candidate_id,"class|admin_table_row","name|admin_table_select[]"));
						var list_item_id = addElement("td", new Array("id|admin_list_container_item-" + data[index].candidate_id + "-" + i,"class|admin_list_container_item_cb centered","name|admin_list_container_item-" + data[index].candidate_id + "-" + i));
						$(list_item_id).append(list_item_select);
						var list_item_showid = addElement("td", new Array("id|admin_list_admin_container_item-id","class|admin_list_admin_container_item centered"),data[index].candidate_id);
						var list_item_party = addElement("td", new Array("id|admin_list_admin_container_item-party","class|admin_list_admin_container_item"),data[index].party_name_short);
						var list_item_first = addElement("td", new Array("id|admin_list_admin_container_item-first","class|admin_list_admin_container_item"),data[index].candidate_first);
						var list_item_last = addElement("td", new Array("id|admin_list_admin_container_item-last","class|admin_list_admin_container_item"),data[index].candidate_last);
						var list_item_race = addElement("td", new Array("id|admin_list_admin_container_item-race","class|admin_list_admin_container_item"),data[index].race_title_short);
						
						var list_item_edit = addElement("input", new Array("type|image","src|<?=$base_url?>ci/_common/images/admin-edit.png","id|admin_table_row_edit-" + data[index].candidate_id,"class|admin_table_cell centered","name|admin_table_row_edit"));
						$(list_item_edit).bind('click', function(){ editRecord(this, _theform, _formaction, 'editcandidate'); });
						var list_item_util1 = addElement("td", new Array("id|admin_table_cell-" + data[index].candidate_id,"class|admin_table_cell centered"));
						$(list_item_util1).append(list_item_edit);
						var list_item_delete = addElement("input", new Array("type|image","src|<?=$base_url?>ci/_common/images/admin-delete.png","id|admin_table_row_delete-" + data[index].candidate_id,"class|admin_table_cell centered","name|admin_table_row_delete"));
						$(list_item_delete).bind('click', function(){ deleteRecord(this, _theform, _formaction, 'removecandidate'); });
						var list_item_util2 = addElement("td", new Array("id|admin_table_cell-" + data[index].candidate_id,"class|admin_table_cell centered"));
						$(list_item_util2).append(list_item_delete);
						$(list_row).append(list_item_id,list_item_showid,list_item_party,list_item_first,list_item_last,list_item_race,list_item_util1,list_item_util2);
						$(new_list_body).append(list_row);
						i++;
					});
					$(new_list).append(new_list_body);
					$(new_list).append(new_list_foot);
					$('#admin_list_inner_wrapper').append(new_list);
					$('#admin_list_container_head_row').clone().attr('id', 'admin_list_container_head_row-foot').appendTo('#admin_list_admin_container_foot');
					$('#admin_list_container').dataTable({
						"aaSorting": [[ 4, "asc" ],[ 3, "asc" ]],
						"iDisplayLength": 100,
						"bProcessing": true,
						"sPaginationType": "full_numbers",
						"bStateSave": true,
						"bJQueryUI": true
					}).hide().fadeIn("slow");
				}
				function processCandidateError(data){
					alert(data);
				}
			});
		</script>
		<?php
		break;
	}
	case 'addcandidate':
	case 'editcandidate':
	{
		// CANDIDATE ADD / EDIT
		$party_id				= "";
		$race_id				= "";
		$candidate_first		= "";
		$candidate_mi			= "";
		$candidate_last			= "";
		$candidate_incumbent	= "";
		$candidate_winner		= "";
		if($candidate_id) { 
			foreach($candidates as $candidate) {
				$candidate_id			= $candidate['candidate_id'];
				$party_id				= $candidate['party_id'];
				$race_id				= $candidate['race_id'];
				$candidate_first		= $candidate['candidate_first'];
				$candidate_mi			= $candidate['candidate_mi'];
				$candidate_last			= $candidate['candidate_last'];
				$candidate_incumbent	= (($candidate['candidate_incumbent'])? 1 : 0);
				$candidate_winner		= (($candidate['candidate_winner'])? 1 : 0);
			} 
		}
		$row=0;
		?>
		<form id="admin_action_form" action="<?=$base_url?>index.php/election2012/manageElection/savecandidate" enctype="multipart/form-data" method="POST">
			<div id="admin_cp_wrapper" class="admin_cp_wrapper_50">
				<span id="cp_back" class="cp_back"><--Back</span>
				<input type="submit" id="admin_btn_edit" class="form_button" name="admin_btn_edit" value="Save" tabindex="3"/>
				<input type="hidden" id="admin_redirect" name="admin_redirect" value="" />
			</div>
			<table id="admin_table" class="admin_table_50" cellpadding="0" cellspacing="0">
				<tr id="admin_table_head_row" class="admin_table_head_row"><th>Field</th><th>Value</th></tr>
				<tr id="admin_table_row-" class="admin_table_row <?php echo (($row%2)? ' even' : ' odd'); $row++; ?>"><td class="admin_table_cell label">Party</td><td class="admin_table_cell field">
					<select id="party_id" class="select" name="party_id" tabindex="6">
						<option value="0">Select Party</option>
						<?php
						foreach($party_options as $party_option) { ?>
							<option value="<?=$party_option['party_id']?>"<?=(($party_id==$party_option['party_id'])? ' selected=\"selected\"' : '')?>><?=$party_option['party_name_short']?></option>
						<?php } ?>
					</select>
					</td></tr>
				<tr id="admin_table_row-" class="admin_table_row <?php echo (($row%2)? ' even' : ' odd'); $row++; ?>"><td class="admin_table_cell label">Race</td><td class="admin_table_cell field">
					<select id="race_id" class="select" name="race_id" tabindex="6">
						<option value="0">Select Race</option>
						<?php
						foreach($race_options as $race_option) { ?>
							<option value="<?=$race_option['race_id']?>"<?=(($race_id==$race_option['race_id'])? ' selected=\"selected\"' : '')?>><?=$race_option['race_title_short']?></option>
						<?php } ?>
					</select>
					</td></tr>
				<tr id="admin_table_row-" class="admin_table_row <?php echo (($row%2)? ' even' : ' odd'); $row++; ?>"><td class="admin_table_cell label">First Name</td><td class="admin_table_cell field">
					<input type="text" id="candidate_first" class="form_text" name="candidate_first" value="<?=(($candidate_first)? $candidate_first : '')?>" tabindex="1"/>
					</td></tr>
				<tr id="admin_table_row-" class="admin_table_row <?php echo (($row%2)? ' even' : ' odd'); $row++; ?>"><td class="admin_table_cell label">Middle Initial</td><td class="admin_table_cell field">
					<input type="text" id="candidate_mi" class="form_text" name="candidate_mi" value="<?=(($candidate_mi)? $candidate_mi : '')?>" tabindex="2"/>
					</td></tr>
				<tr id="admin_table_row-" class="admin_table_row <?php echo (($row%2)? ' even' : ' odd'); $row++; ?>"><td class="admin_table_cell label">Last Name</td><td class="admin_table_cell field">
					<input type="text" id="candidate_last" class="form_text" name="candidate_last" value="<?=(($candidate_last)? $candidate_last : '')?>" tabindex="2"/>
					</td></tr>
				<tr id="admin_table_row-" class="admin_table_row <?php echo (($row%2)? ' even' : ' odd'); $row++; ?>"><td class="admin_table_cell label">Incumbent?</td><td class="admin_table_cell field">
					<input type="checkbox" id="candidate_incumbent" class="form_text" name="candidate_incumbent" <?=(($candidate_incumbent==1)? ' checked=checked' : '')?>" tabindex="2"/>
					</td></tr>
				<tr id="admin_table_row-" class="admin_table_row <?php echo (($row%2)? ' even' : ' odd'); $row++; ?>"><td class="admin_table_cell label">Winner?</td><td class="admin_table_cell field">
					<input type="checkbox" id="candidate_winner" class="form_text" name="candidate_winner" <?=(($candidate_winner==1)? ' checked=checked' : '')?>" tabindex="2"/>
					</td></tr>
			</table>
			<input type="hidden" id="candidate_id" name="candidate_id" value="<?=(($candidate_id)? $candidate_id : '')?>"/>
		</form>
		<script language="JavaScript">
			$(document).ready(function(){
				var formaction = "<?=$base_url?>index.php/election2012/manageElection";
				$('#admin_redirect').val(document.URL);
				$('#admin_selection_candidate').addClass('selected');
				$('#admin_cp_station').bind("change", function() {
					var newstation = $('#admin_cp_station').find(':selected').val();
					$('#admin_action_form').attr('action', formaction + '/setstation/' + newstation);
					$('#admin_action_form').submit();
				});
			});
		</script>
		<?php
		break;
	}
	case 'mergecandidates':
	{
		$row=0;
		?>
		<form id="admin_action_form" action="<?=$base_url?>index.php/election2012/manageElection/savecandidatemerge" enctype="multipart/form-data" method="POST">
			<div id="admin_cp_wrapper" class="admin_cp_wrapper_50">
				<span id="cp_back" class="cp_back"><--Back</span>
				<input type="submit" id="admin_btn_edit" class="form_button" name="admin_btn_edit" value="Save" tabindex="3"/>
				<input type="hidden" id="admin_redirect" name="admin_redirect" value="" />
			</div>
			<table id="admin_table" class="admin_table_50" cellpadding="0" cellspacing="0">
				<tr id="admin_table_head_row" class="admin_table_head_row"><th>Candidate Sources</th><th>Map Candidates To</th></tr>
				<tr id="admin_table_row-" class="admin_table_row <?php echo (($row%2)? ' even' : ' odd'); $row++; ?>">
					<td class="admin_table_cell centered select">
					<select id="source_id" class="select" name="source_id[]" multiple="multiple" size="8" tabindex="6">
						<?php
						foreach($candidates as $candidate) { ?>
							<option value="<?=$candidate['candidate_id']?>|<?=$candidate['source_id']?>|<?=$candidate['source_value']?>"><?=$candidate['candidate_first']?> <?=$candidate['candidate_last']?> [<?=$candidate['source_id']?>|<?=$candidate['source_value']?>]</option>
						<?php } ?>
					</select>
					</td>
					<td class="admin_table_cell centered select">
					<select id="candidate_id" class="select" name="candidate_id" tabindex="6">
						<option value="0">Select Candidate To Map To</option>
						<?php
						foreach($candidates as $candidate) { ?>
							<option value="<?=$candidate['candidate_id']?>"><?=$candidate['candidate_first']?> <?=$candidate['candidate_last']?> [<?=$candidate['source_id']?>|<?=$candidate['source_value']?>]</option>
						<?php } ?>
					</select>
					</td></tr>
			</table>
		</form>
		<script language="JavaScript">
			$(document).ready(function(){
				var formaction = "<?=$base_url?>index.php/election2012/manageElection";
				$('#admin_redirect').val(document.URL);
				$('#admin_selection_candidate').addClass('selected');
				$('#admin_cp_station').bind("change", function() {
					var newstation = $('#admin_cp_station').find(':selected').val();
					$('#admin_action_form').attr('action', formaction + '/setstation/' + newstation);
					$('#admin_action_form').submit();
				});
			});
		</script>
		<?php
		break;
	}
	case 'preference':
	{
		$row=0;
		$i=0;?>
		<form id="admin_action_form" action="<?=$base_url?>index.php/election2012/manageElection/savepreference" enctype="multipart/form-data" method="POST">
			<div id="admin_cp_wrapper" class="admin_cp_wrapper_50">
				<span id="cp_back" class="cp_back"><--Back</span>
				<select id="admin_cp_station" class="form_select" name="admin_cp_station"><?php
				$stations = (($wish_user)? ((array_key_exists('wish_stations', $wish_user))? $wish_user['wish_stations'] : '') : '');
				if($stations) {
					foreach($stations as $key=>$value) {
						?><option value="<?=$key?>"<?=(($key==$station_id)? ' selected=\"selected\"' : '')?>><?=$value?></option><?php
					}
				}
				?></select>
				<input type="submit" id="admin_btn_edit" class="form_button" name="admin_btn_edit" value="Save" tabindex="<?=count($preferences)+1?>>
				<input type="hidden" id="admin_table_rowselect_hidden" name="admin_table_rowselect_hidden" value="">
				<input type="hidden" id="admin_redirect" name="admin_redirect" value="" />
			</div>
			<table id="admin_table" class="admin_table_50" cellpadding="0" cellspacing="0">
				<tr id="admin_table_head_row" class="admin_table_head_row">
					<th id="admin_table_head_cell-visiting" class="admin_table_head_cell">Field</th>
					<th id="admin_table_head_cell-visiting" class="admin_table_head_cell">Value</th>
				</tr>
				<?php
				foreach($preferences as $preference) {
				$field_value = "";?>
					<tr id="admin_table_row-<?=key($preference)?>" class="admin_table_row <?php $rowbg = (($row%2)? ' even' : ' odd'); $row++; echo $rowbg; ?>"><?php
					//if($preference['module_id'] == $module_id) {
						switch($preference['preference_type']) {
							case 'text':
							{?>
								<td id="admin_table_cell-<?=$row?>-<?=$i++?>" class="admin_table_cell">
									<label for="<?=$preference['preference_name']?>" class="righted"><?=$preference['preference_title']?></label>
								</td>
								<td id="admin_table_cell-<?=$row?>-<?=$i++?>" class="admin_table_cell">
									<input type="text" id="<?=$preference['preference_name']?>" class="form_text" name="<?=$preference['preference_name']?>" value="<?=((array_key_exists("preference_field_value", $preference))? $preference['preference_field_value'] : '')?>" tabindex="<?=$row?>"/>
								</td><?php
								break;
							}
							case 'select':
							{?>
								<td id="admin_table_cell-<?=$row?>-<?=$i++?>" class="admin_table_cell">
									<label for="<?=$preference['preference_name']?>" class="righted"><?=$preference['preference_title']?></label>
								</td>
								<td id="admin_table_cell-<?=$row?>-<?=$i++?>" class="admin_table_cell">
									<select id="<?=$preference['preference_name']?>" class="form_text" name="<?=$preference['preference_name']?>" tabindex="<?=$row?>"/>
									</select>
								</td><?php
								break;
							}
							case 'date':
							{?>
								<td id="admin_table_cell-<?=$row?>-<?=$i++?>" class="admin_table_cell">
									<label for="<?=$preference['preference_name']?>" class="righted"><?=$preference['preference_title']?></label>
								</td>
								<td id="admin_table_cell-<?=$row?>-<?=$i++?>" class="admin_table_cell">
									<input type="text" id="<?=$preference['preference_name']?>" class="form_text" name="<?=$preference['preference_name']?>" value="<?=((array_key_exists("preference_field_value", $preference))? $preference['preference_field_value'] : '')?>" tabindex="<?=$row?>"/>
								</td><?php
								break;
							}
							case 'hidden':
							{?>
								<input type="hidden" id="<?=$preference['preference_name']?>" name="<?=$preference['preference_name']?>" value="<?=(($field_value)? addslashes($field_value) : '')?>"/><?php
								break;
							}
							default:
							{?>
								<td id="admin_table_cell-<?=$row?>-<?=$i++?>" class="admin_table_cell">
									<label for="<?=$preference['preference_name']?>" class="righted"><?=$preference['preference_title']?></label>
								</td>
								<td id="admin_table_cell-<?=$row?>-<?=$i++?>" class="admin_table_cell">
									<input type="text" id="<?=$preference['preference_name']?>" class="form_text" name="<?=$preference['preference_name']?>" value="<?=((array_key_exists("preference_field_value", $preference))? $preference['preference_field_value'] : '')?>" tabindex="<?=$row?>"/>
								</td><?php
								break;
							}
						}
					//}?>
					</tr><?php
				}?>
					<tr id="admin_table_foot_row" class="admin_table_foot_row">
						<th id="admin_table_foot_cell" class="admin_table_foot_cell" colspan="2">&nbsp;</th>
					</tr>
			</table>
		</form>
		<script language="JavaScript">
			$(document).ready(function(){
				var formaction = "<?=$base_url?>index.php/sportsHS/manageHS";
				$('#admin_redirect').val(document.URL);
				$('#admin_selection_preferences').addClass('selected');
				$('#admin_cp_station').bind("change", function() {
					var newstation = $('#admin_cp_station').find(':selected').val();
					$('#admin_action_form').attr('action', formaction + '/setstation/' + newstation);
					$('#admin_action_form').submit();
				});
			});
		</script>
		<?php
		break;
	}
	default:
	{
	?>
		<fieldset id="admin_cp_fieldset" class="admin_cp_fieldset">
			<legend id="admin_cp_legend" class="admin_cp_legend">Refresh Feeds</legend>
			<form id="form_refresh_election_data" class="form_refresh_election_data">
				<div style="float:left; text-align: left;">
					<select id="admin_cp_station" class="form_select" name="admin_cp_station[]" multiple="multiple" size="8"><?php
					if($the_stations) {
						foreach($the_stations as $key=>$value) {
							?><option value="<?=$key?>"<?=(($key==$station_id)? ' selected=\"selected\"' : '')?>><?=$value?></option><?php
						}
					}
					?></select><br />
					<input type="checkbox" id="admin_cp_station_loop" class="admin_cp_station_loop" name="admin_cp_station_loop" />
					<label for"admin_cp_station_loop">Loop Every </label>
					<select id="admin_cp_station_loop_time" class="form_select" name="admin_cp_station_loop_time">
						<option value="60000">1</option>
						<option value="120000">2</option>
						<option value="180000">3</option>
						<option value="240000">4</option>
						<option value="300000">5</option>
						<option value="360000">6</option>
						<option value="420000">7</option>
						<option value="480000">8</option>
						<option value="540000">9</option>
						<option value="600000">10</option>
					</select>
					<label for="admin_cp_station_loop_time">Minutes</label><br />
					<input type="button" id="admin_refresh_election_data" class="form_button" value="Refresh Selected" />
				</div>
			</form>
			<div style="float:left; padding:5px;">
				<div id="refresh_data_result" class="refresh_data_result" style="width:200px"></div>
			</div>
			<div id="clear"></div>
		</fieldset>
		<fieldset id="admin_cp_fieldset" class="admin_cp_fieldset">
			<legend id="admin_cp_legend" class="admin_cp_legend">Output Data</legend>
			<form id="form_output_election_data" class="form_output_election_data">
				<div style="float:left; text-align: left;">
					<select id="admin_cp_station_output" class="form_select" name="admin_cp_station_output[]" multiple="multiple" size="7"><?php
					if($the_stations) {
						foreach($the_stations as $key=>$value) {
							?><option value="<?=$key?>"<?=(($key==$station_id)? ' selected=\"selected\"' : '')?>><?=$value?></option><?php
						}
					}
					?></select><br />
					<select id="admin_cp_format" class="form_select" name="admin_cp_format">
						<option value="xml">XML</option>
						<option value="json">JSON</option>
					</select><br />
					<input type="checkbox" id="admin_cp_station_output_loop" class="form_checkbox" name="admin_cp_station_output_loop" />
					<label for"admin_cp_station_output_loop">Loop Every </label>
					<select id="admin_cp_output_loop_time" class="form_select" name="admin_cp_output_loop_time">
						<option value="60000">1</option>
						<option value="120000">2</option>
						<option value="180000">3</option>
						<option value="240000">4</option>
						<option value="300000">5</option>
					</select>
					<label for="admin_cp_output_loop_time">Minutes</label><br />
					<input type="button" id="admin_output_election_data" class="form_button" value="Update Files" />
				</div>
			</form>
			<div style="float:left; padding:5px;">
				<div id="output_data_result" class="output_data_result" style="width:200px"></div>
			</div>
			<div id="clear"></div>
		</fieldset>
		<fieldset id="admin_cp_fieldset" class="admin_cp_fieldset">
			<legend id="admin_cp_legend" class="admin_cp_legend">Transfer AP</legend>
			<form id="form_transfer_ap_data" class="form_transfer_ap_data">
				<div style="float:left; text-align: left;">
					<select id="admin_cp_ap_files" class="form_select" name="admin_cp_ap_files[]" multiple="multiple" size="3">
						<option value="pres_summary">pres_summary.xml</option>
						<option value="pres_summary_all">pres_summary_all.xml</option>
						<option value="statebystate_pres">statebystate_pres.xml</option>
					</select><br />
					<input type="checkbox" id="admin_cp_transfer_ap_loop" class="form_checkbox" name="admin_cp_transfer_ap_loop" />
					<label for"admin_cp_transfer_ap_loop">Loop Every </label>
					<select id="admin_cp_transfer_ap_loop_time" class="form_select" name="admin_cp_transfer_ap_loop_time">
						<option value="60000">1</option>
						<option value="120000">2</option>
						<option value="180000">3</option>
						<option value="240000">4</option>
						<option value="300000">5</option>
					</select>
					<label for="admin_cp_transfer_ap_loop_time">Minutes</label><br />
					<input type="button" id="admin_transfer_ap_data" class="form_button" name="admin_transfer_ap_data" value="Transfer AP Data" />
				</div>
			</form>
			<div style="float:left; padding:5px;">
				<div id="transfer_data_result" class="transfer_data_result"></div>
			</div>
			<div id="clear"></div>
		</fieldset>
		<fieldset id="admin_cp_fieldset" class="admin_cp_fieldset">
			<legend id="admin_cp_legend" class="admin_cp_legend">Clear Data</legend>
			<div id="truncate_data_div" class="truncate_data_div centered">
				<select id="admin_cp_tables" class="form_select" name="admin_cp_tables[]" multiple="multiple" size="8">
					<option value="Candidates">Candidates</option>
					<option value="CandidateResults">Candidate Results</option>
					<option value="CandidateDataSources">Candidate Source Map</option>
					<option value="Races">Races</option>
					<option value="RaceCandidates">Race/Candidate Join</option>
					<option value="RaceResults">Race Results</option>
					<option value="RaceDataSources">Race Source Map</option>
				</select>
				<input type="image" src="<?=$base_url?>ci/_common/images/panic_btn_small.png" id="truncate_data_btn" class="truncate_data_btn" name="truncate_data_btn" />
			</div>
			<div id="truncate_data_result" class="truncate_data_result"></div>
		</fieldset>
		<script language="JavaScript">
			var stationtimer = "";
			var outputtimer = "";
			var transfertimer = "";
			$(document).ready(function(){
				$('#admin_selection_home').addClass('selected');
				var formaction = "<?=$base_url?>index.php/election2012/manageElection";
				$('#admin_refresh_election_data').click(function(e){
					e.preventDefault();
					startRefresh();
				});
				$('#admin_output_election_data').click(function(e){
					e.preventDefault();
					startOutput();
				});
				$('#admin_transfer_ap_data').click(function(e){
					e.preventDefault();
					startTransfer();
				});
				$('#truncate_data_btn').click(function(e){
					e.preventDefault();
					var selecttables = $('select#admin_cp_tables').val();
					if(confirm('You are about to clear all candidate and race data from the database. This action cannot be undone. Are you sure you want to continue? ')) {
						theSource = "<?=$base_url?>index.php/election2012/manageElection/cleardata";
						theData = "organization=&tables=" + selecttables;
						fetchData(theSource, theData, processClear, processClearError);
					}
				});
				function startRefresh() {
					window.clearInterval(stationtimer);
					var selectstation = $('select#admin_cp_station').val();
					if($('input#admin_cp_station_loop').is(':checked')){
					 	var stationtime = $('#admin_cp_station_loop_time').find(':selected').val();
						stationtimer = setInterval(function(){startRefresh()},stationtime);
					}
					$(selectstation).each(function(){
						var theorg		= this;
						var theorgtxt	= $("select#admin_cp_station option[value='" + theorg + "']").text();
						var stdwww		= addElement("div", new Array("id|stdwww_field-" + theorg,"class|stdwww_field"));
						var msgtext		= addElement("span", new Array("id|refresh_item_text-" + theorg,"class|refresh_item"), "Refreshing data for " + theorgtxt);
						var item		= addElement("div", new Array("id|refresh_item-" + theorg,"class|refresh_item"));
						$(item).append(msgtext,stdwww);
						$("#refresh_data_result").append($(item).hide().fadeIn("slow"));
						theSource = "<?=$base_url?>index.php/election2012/ingestRaceData";
						theData = "org=<?=$station_id?>";
						fetchData(theSource, theData, processRefresh, processRefreshError);
					});
				}
				function processRefresh(data) {
					$("#refresh_item-" + data.org).detach();
					$.each(data.message, function(index){
						var item = addElement("div", new Array("id|refresh_item-" + index,"class|refresh_item"), data.message[index]);
						if(data.error===true){
							$("#refresh_data_result").append($(item).hide().fadeIn("slow"));
						} else {
							$("#refresh_data_result").append($(item).hide().fadeIn("slow").delay(30000).fadeOut("slow"));
						}
					});
				}
				function processRefreshError(data) {
					alert(data);
				}
				function startOutput() {
					window.clearInterval(outputtimer);
					var selectstation = $('select#admin_cp_station_output').val();
					var selectformat = $('select#admin_cp_format').val();
					if($('input#admin_cp_station_output_loop').is(':checked')){
					 	var outlooptime = $('#admin_cp_output_loop_time').find(':selected').val();
						outputtimer = setInterval(function(){startOutput()},outlooptime);
					}
					$(selectstation).each(function(){
						var theorg		= this;
						var theorgtxt	= $("select#admin_cp_station_output option[value='" + theorg + "']").text();
						var stdwww		= addElement("div", new Array("id|stdwww_field-" + theorg,"class|stdwww_field"));
						var msgtext		= addElement("span", new Array("id|refresh_item_text-" + theorg,"class|refresh_item"), "Exporting data for " + theorgtxt);
						var item		= addElement("div", new Array("id|refresh_item-" + theorg,"class|refresh_item"));
						$(item).append(msgtext,stdwww);
						$("#output_data_result").append($(item).hide().fadeIn("slow"));
						theSource = "<?=$base_url?>index.php/election2012/exportSpecifiedData";
						theData = "org=" + this +
							"&format=" + selectformat +
							"&state=" +
							"&type=";
						fetchData(theSource, theData, processOutput, processOutputError);
					});
				}
				function processOutput(data) {
					$("#refresh_item-" + data.org).detach();
					$.each(data.message, function(index){
						var item = addElement("div", new Array("id|refresh_item-" + index,"class|refresh_item"), data.message[index]);
						if(data.error===true){
							$("#output_data_result").append($(item).hide().fadeIn("slow"));
						} else {
							$("#output_data_result").append($(item).hide().fadeIn("slow").delay(30000).fadeOut("slow"));
						}
					});
					/*var item = addElement("div", new Array("id|refresh_item","class|refresh_item"), data);
					$("#output_data_result").append($(item).hide().fadeIn("slow").delay(30000).fadeOut("slow"));*/
				}
				function processOutputError(data) {
					alert(data);
				}
				function startTransfer() {
					window.clearInterval(transfertimer);
					var selectfile = $('select#admin_cp_ap_files').val();
					if($('input#admin_cp_transfer_ap_loop').is(':checked')){
					 	var transaptime = $('#admin_cp_transfer_ap_loop_time').find(':selected').val();
						transfertimer = setInterval(function(){startTransfer()},transaptime);
					}
					for(i=0;i<selectfile.length; i++){
						theSource = "<?=$base_url?>index.php/election2012/transferAPData/" + selectfile[i];
						theData = "";
						fetchData(theSource, theData, processTransfer, processTransferError);
					}
				}
				function processTransfer(data) {
					var item = addElement("div", new Array("id|refresh_item","class|refresh_item"), data);
					$("#transfer_data_result").append($(item).hide().fadeIn("slow").delay(30000).fadeOut("slow"));
				}
				function processTransferError(data) {
					alert(data);
				}
				function processClear(data) {
					$("#truncate_table_result_inner").detach();
					var row = 0;
					var responsediv = addElement("div", new Array("id|truncate_table_result_inner","class|truncate_table_result_inner"));
					$.each(data, function(index) {
						//alert(JSON.stringify(data[index].response))
						var item = addElement("div", new Array("id|truncate_table_item","class|truncate_table_item"), data[index].response);
						$(responsediv).append(item);
					});
					$("#truncate_data_result").append(responsediv).hide().fadeIn("slow").delay(30000).fadeOut("slow")
				}
				function processClearError(data) {
					alert(data);
				}
			});
		</script>
	<?php
	}
}
?>
	</div>
	<div id="admin_footer" class="admin_footer"></div>
</div>
<script language="JavaScript">
	$.expr[':'].icontains = function(a, i, m) {
		return jQuery(a).text().toUpperCase()
		.indexOf(m[3].toUpperCase()) >= 0;
	};
	$(document).ready(function(){
		var formaction = "<?=$base_url?>index.php/election2012/manageElection";
		$("#admin_load").hide();
		$('#user_redirect').val(document.URL);
		$('#cp_back').bind('click', function(){
			history.back();
		});
		$("#user_admin_access").bind("click", function(e){
			e.preventDefault();
			window.open('<?=$base_url?>index.php/coreAdmin', 'Admin');
		});
		$('#admin_table_head_select').bind('click', function(){
			var gcheck = $(this).is(':checked')? true : false;
			$('input[id^="admin_table_rowselect-"]').each(function(){
				(gcheck===true)? $(this).prop("checked", true) : $(this).prop("checked", false);
			});
		});
		$('tr[id^="admin_table_row-"]').bind("mouseover", function(){
			$(this).addClass("roll");
		});
		$('tr[id^="admin_table_row-"]').bind("mouseout", function(){
			$(this).removeClass("roll");
		});
	});
</script>
<pre id="variable_test">
	
</pre>