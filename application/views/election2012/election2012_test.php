<?php
$race_info = 
	"<?xml version='1.0' encoding='ISO-8859-1'?>\n".
	"<data>\n";
foreach($races as $race){
	$race_info .=
		"	<race>\n".
		"		<race_info>\n".
		"			<race_id>".$race['race_id']."</race_id>\n".
		"			<race_name1>".htmlspecialchars($race['race_title_short'])."</race_name1>\n".
		"			<race_name2>".htmlspecialchars($race['race_title'])."</race_name2>\n".
		"			<race_name3></race_name3>\n".
		"			<precincts_reporting>".number_format($race['race_result_precincts_reporting'])."</precincts_reporting>\n".
		"			<pct_precincts_reporting>".number_format($race['race_result_precincts_percent'])."</pct_precincts_reporting>\n".
		"			<precincts_total>".number_format($race['race_result_total'])."</precincts_total>\n".
		"			<last_updated>".str_replace("PM", "p.m.", str_replace("AM", "a.m.", date("M d, Y g:i A", $race['race_updated'])))."</last_updated>\n". //Nov 03, 2010  11:30 AM
		"			<total_vote/>\n".
		"			<other1/>\n".
		"			<other2/>\n".
		"			<other3/>\n".
		"			<jurisdiction_id>".$race['county_fip']."</jurisdiction_id>\n".
		"		</race_info>\n".
		"		<candidate_info>\n";
	$candidates = ((array_key_exists("race_candidates", $race))? $race['race_candidates'] : array());
	foreach($candidates as $candidate){
		$race_info .=
			"			<candidate>\n".
			"				<candidate_id>".$candidate['candidate_id']."</candidate_id>\n".
			"				<first_name>".htmlspecialchars($candidate['candidate_first'])."</first_name>\n".
			"				<initial>".htmlspecialchars($candidate['candidate_mi'])."</initial>\n".
			"				<last_name>".htmlspecialchars($candidate['candidate_last'])."</last_name>\n".
			"				<incumbent>".$candidate['candidate_incumbent']."</incumbent>\n".
			"				<party_abbr>".$candidate['candidate_party_code']."</party_abbr>\n".
			"				<cand_vote>".number_format($candidate['candidate_result_value'])."</cand_vote>\n".
			"				<vote_percent></vote_percent>\n".
			"				<winner>".$candidate['candidate_winner']."</winner>\n".
			"				<projected_winner></projected_winner>\n".
			"				<cother1></cother1>\n".
			"				<cother2></cother2>\n".
			"				<cother3></cother3>\n".
			"			</candidate>\n";
	}
	$race_info .=
		"		</candidate_info>\n".
		"	</race>\n";
}
$race_info .= "</data>\n";
echo $race_info;
/*echo "<pre>";
print_r($races);
echo "</pre>";*/
?>