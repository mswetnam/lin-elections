<?php
switch($op)
{
	case 'national':
	{ ?>
		<div id="map_page_wrapper_outter" class="map_page_wrapper_outter_full">
			<div id="map_page_wrapper_inner" class="map_page_wrapper_inner">
				<div id="map_frame_full" class="map_frame_full"><?=$national_svg?></div>
				<div id="race_info_full" class="race_info_full info_default_full">
					<input type="hidden" id="selected_div" name="selected_div" val="" />
					<input type="hidden" id="selected_path" name="selected_path" val="" />
				</div>
			</div>
		</div>
		<script language="JavaScript">
			var timer = "";
			$(document).ready(function() {
				document.documentElement.style.overflowX = 'hidden';
				document.documentElement.style.overflowY = 'hidden';
				document.body.scroll = "no";	// ie only
				initializeNationalData();
				
				function initializeNationalData(){
					window.clearInterval(timer);
					theSource = "<?=$base_url?>index.php/election2012/fetchResultsForDisplay/";
					theData = "organization=<?=$theorganization?>&scope=<?=$thescope?>&code=<?=$thecode?>&race=<?=$therace?>&size=<?=$thesize?>";
					fetchData(theSource, theData, setNationalColors);
				}
				function setNationalColors(data){
					$('div[id^="race_wrapper-"]').each(function(){
						$(this).detach();
					});
					$(data).each(function(index){
						var i=0;
						var cannum = 0;
						var party = "";
						var leader = new Array();
						var state_code						= data[index].state_code;
						var state_name						= data[index].state_name;
						var electoral_votes					= data[index].electoral_votes;
						var pct_precincts_reporting			= data[index].pct_precincts_reporting;
						var race_candidates					= data[index].candidates;
						var percent_reporting				= (data[index].pct_precincts_reporting>59)? 'St' : 'Wk';
						var racewrapper 					= addElement("div", new Array("id|race_wrapper-" + data[index].state_code,"class|race_wrapper gone"));
						var raceinfowrapper 				= addElement("div", new Array("id|race_info_wrapper-" + data[index].state_code,"class|race_info_wrapper"));
						var state_code_div					= addElement("div", new Array("id|race_item-" + i,"class|race_item item_county"), data[index].state_name.toUpperCase()); i++;
						var state_title_div					= addElement("div", new Array("id|race_item-" + i,"class|race_item item_title"), 'PRESIDENT'); i++;
						var electoral_votes_div				= addElement("div", new Array("id|race_item-" + i,"class|race_item item_electoral gone"), data[index].electoral_votes); i++;
						var pct_precincts_reporting_div		= addElement("div", new Array("id|race_item-" + i,"class|race_item item_race_percent"), data[index].pct_precincts_reporting + '% Reporting'); i++;
						$(raceinfowrapper).append(state_code_div, state_title_div, electoral_votes_div);
						$("#race_info_full").append(racewrapper);
						var thepath = $('path[id='+state_code+']');
						thepath.attr('style', 'cursor:pointer;');
						thepath.bind('click', function(){ toggleState(state_code, racewrapper, thepath); });
						$(racewrapper).bind('click', function(){ toggleState(state_code, racewrapper); });
						$.each(race_candidates, function(){
							var j=0;
							var candidate_name					= this.candidate_name;
							var candidate_id					= this.candidate_id;
							var candidate_party					= this.candidate_party;
							var candidate_popular_vote			= this.candidate_popular_vote;
							var candidate_popular_percent		= this.candidate_popular_percent;
							var candidate_won					= this.candidate_won;
							if(candidate_party=='Dem' || candidate_party=='GOP'){
								var candidatewrapper 				= addElement("div", new Array("id|candidate_wrapper-" + this.candidate_id,"class|candidate_wrapper"));
								var candidate_id_div				= addElement("div", new Array("id|candidate_item-" + this.candidate_id + '-' + j,"class|candidate_item item_candidate_id gone"), this.candidate_id); j++;
								var candidate_name_div				= addElement("div", new Array("id|candidate_item-" + this.candidate_id + '-' + j,"class|candidate_item item_name"), this.candidate_name.toUpperCase()); j++;
								var candidate_party_div				= addElement("div", new Array("id|candidate_item-" + this.candidate_id + '-' + j,"class|candidate_item item_party gone"), this.candidate_party.toUpperCase()); j++;
								var candidate_popular_vote_div		= addElement("div", new Array("id|candidate_item-" + this.candidate_id + '-' + j,"class|candidate_item item_candidate_pop_vote gone"), this.candidate_popular_vote); j++;
								var candidate_popular_percent_div	= addElement("div", new Array("id|candidate_item-" + this.candidate_id + '-' + j,"class|candidate_item item_candidate_percent"), this.candidate_popular_percent + '%'); j++;
								var candidate_won_div				= addElement("div", new Array("id|candidate_item-" + this.candidate_id + '-' + j,"class|candidate_item item_candidate_id gone"), this.candidate_won); j++;
								$(candidatewrapper).append(candidate_id_div, candidate_name_div, candidate_party_div, candidate_popular_vote_div, candidate_popular_percent_div, candidate_won_div);
								$(racewrapper).append(candidatewrapper);
								if(cannum==0){
									party = candidate_party;
								}
							}
							cannum++;
						});
						var candidate_show = 0;
						$(racewrapper).children().each(function(){
							if(candidate_show>=2){
								$(this).addClass('gone');
							}
							candidate_show++;
						});
						$(racewrapper).append(pct_precincts_reporting_div);
						$(racewrapper).prepend(raceinfowrapper);
						party = (party=='')? 'N' : party;
						var color = {cDemSt:"#0036d0",cDemWk:"#718CD9",cGOPSt:"#c01720",cGOPWk:"#D17D81",cGrnSt:"#336600",cGrnWk:"#99B380",cLibSt:"#580158",cLibWk:"#AC80AC",cCSt:"#FFCC33",cCWk:"#FFCC33",cNSt:"#e2e2e2",cNWk:"#e2e2e2"};
						$('path[id$='+state_code+']').each(function(){
							this.setAttribute('fill', color['c'+party+percent_reporting]);
						});
					});
					var thestate = $("#selected_path").val();
					var thedivid = $("#selected_div").val();
					var theselecteddiv = $('div[id='+thedivid+']');
					toggleState(thestate, theselecteddiv);
					timer = setInterval(function(){initializeNationalData()},60000);
				}
				function toggleState(state, thediv, thepath) {
					$("#selected_path").val(state);
					$("#selected_div").val($(thediv).attr('id'));
					$('div[id^="race_wrapper"]').each(function(){
						$(this).addClass('gone');
					});
					if($(thediv).hasClass('gone')){
						$(thediv).removeClass('gone');
						$("#race_info_full").removeClass('info_default');
						$("#race_info_full").addClass('info_2can');
					} else {
						$(thediv).addClass('gone');
						$("#race_info_full").addClass('info_default');
						$("#race_info_full").removeClass('info_default');
					}
					if($("#selected_path").val()=='' && $("#selected_div").val()==''){
						$("#race_info").addClass('info_default');
					}
				}
			});
		</script>
		<?php
		break;
	}
	case 'pnational':
	{ ?>
		<div id="ext_map_page_wrapper_outter" class="ext_map_page_wrapper_poutter">
			<div id="ext_map_page_wrapper_inner" class="ext_map_page_wrapper_inner">
				<div id="ext_map_frame_full" class="ext_map_frame_full">
					<!--[if !IE]>-->
				        <?=$national_svg?> <!--<![endif]-->
				      <!--[if lt IE 9]>
				        <object src="<?=$national_svg_loc?>" classid="image/svg+xml" id="mySVGObject"></object>
				        <![endif]-->
				      <!--[if gte IE 9]>
				        <object data="<?=$national_svg_loc?>" type="image/svg+xml" id="mySVGObject"></object>
				       <![endif]-->
				</div>
				<div id="ext_race_info_full" class="ext_race_info_full ext_info_default">
					<input type="hidden" id="ext_selected_div" name="ext_selected_div" val="" />
					<input type="hidden" id="ext_selected_path" name="ext_selected_path" val="" />
				</div>
				<div id="ext_data_source_attribution" class="ext_data_source_attribution">Source: Associated Press</div>
				<div id="slider_wrapper" class="slider_wrapper">
					<div id="candidate_left" class="candidate_left">OBAMA</div><div id="candidate_right" class="candidate_right">ROMNEY&nbsp;&nbsp;</div><div style="clear:both"></div>
					<div class="bar"><div class="dem"></div><div class="rep"></div></div>
				</div>
			</div>
		</div>
		<script language="JavaScript">
			var timer = "";
			$(document).ready(function() {
				document.documentElement.style.overflowX = 'hidden';
				document.documentElement.style.overflowY = 'hidden';
				document.body.scroll = "no";	// ie only
				initializeNationalData();
				
				function initializeNationalData(){
					window.clearInterval(timer);
					theSource = "<?=$base_url?>index.php/election2012/fetchResultsForDisplay/";
					theData = "organization=<?=$theorganization?>&scope=<?=$thescope?>&code=<?=$thecode?>&race=<?=$therace?>&size=<?=$thesize?>";
					fetchData(theSource, theData, setNationalColors);
				}
				function setNationalColors(data){
					var demVotes = 0;
					var repVotes = 0;
					$('div[id^="ext_race_wrapper-"]').each(function(){
						$(this).detach();
					});
					$(data).each(function(index){
						var i=0;
						var cannum = 0;
						var party = "";
						var leader = new Array();
						var state_code						= data[index].state_code;
						var state_name						= data[index].state_name;
						var electoral_votes					= data[index].electoral_votes;
						var pct_precincts_reporting			= data[index].pct_precincts_reporting;
						var race_candidates					= data[index].candidates;
						var percent_reporting				= (data[index].pct_precincts_reporting>79)? 'St' : 'Wk';
						var racewrapper 					= addElement("div", new Array("id|ext_race_wrapper-" + data[index].state_code,"class|ext_race_wrapper ext_gone"));
						var raceinfowrapper 				= addElement("div", new Array("id|ext_race_info_wrapper-" + data[index].state_code,"class|ext_race_info_wrapper"));
						var state_code_div					= addElement("div", new Array("id|ext_race_item-" + i,"class|ext_race_item ext_item_county"), data[index].state_name.toUpperCase()); i++;
						var state_title_div					= addElement("div", new Array("id|ext_race_item-" + i,"class|ext_race_item ext_item_title"), 'PRESIDENT'); i++;
						var electoral_votes_div				= addElement("div", new Array("id|ext_race_item-" + i,"class|ext_race_item ext_item_electoral ext_gone"), data[index].electoral_votes); i++;
						var pct_precincts_reporting_div		= addElement("div", new Array("id|ext_race_item-" + i,"class|ext_race_item ext_item_race_percent"), data[index].pct_precincts_reporting + '% Reporting'); i++;
						$(raceinfowrapper).append(state_code_div, state_title_div, electoral_votes_div);
						$("#ext_race_info_full").append(racewrapper);
						var thepath = $('path[id='+state_code+']');
						thepath.attr('style', 'cursor:pointer;');
						thepath.bind('click', function(){ toggleState(state_code, racewrapper, thepath); });
						$(racewrapper).bind('click', function(){ toggleState(state_code, racewrapper); });
						var leader = race_candidates.reduce(function(a, b) {
							return Math.max(a, b.candidate_popular_vote);
						}, 0);
						$.each(race_candidates, function(){
							var j=0;
							var candidate_name					= this.candidate_name;
							var candidate_id					= this.candidate_id;
							var candidate_party					= this.candidate_party;
							var candidate_popular_vote			= this.candidate_popular_vote;
							var candidate_popular_percent		= this.candidate_popular_percent;
							var candidate_won					= this.candidate_won;
							if(candidate_party=='Dem' || candidate_party=='GOP'){
								var candidatewrapper 				= addElement("div", new Array("id|ext_candidate_wrapper-" + this.candidate_id,"class|ext_candidate_wrapper color" + candidate_party));
								var candidate_id_div				= addElement("div", new Array("id|ext_candidate_item-" + this.candidate_id + '-' + j,"class|ext_candidate_item ext_item_candidate_id ext_gone"), this.candidate_id); j++;
								var candidate_name_div				= addElement("div", new Array("id|ext_candidate_item-" + this.candidate_id + '-' + j,"class|ext_candidate_item ext_item_name"), this.candidate_name.toUpperCase()); j++;
								var candidate_party_div				= addElement("div", new Array("id|ext_candidate_item-" + this.candidate_id + '-' + j,"class|ext_candidate_item ext_item_party ext_gone"), this.candidate_party.toUpperCase()); j++;
								var candidate_popular_vote_div		= addElement("div", new Array("id|ext_candidate_item-" + this.candidate_id + '-' + j,"class|ext_candidate_item ext_item_candidate_pop_vote ext_gone"), this.candidate_popular_vote); j++;
								var candidate_popular_percent_div	= addElement("div", new Array("id|ext_candidate_item-" + this.candidate_id + '-' + j,"class|ext_candidate_item ext_item_candidate_percent"), this.candidate_popular_percent + '%'); j++;
								var candidate_won_div				= addElement("div", new Array("id|ext_candidate_item-" + this.candidate_id + '-' + j,"class|ext_candidate_item ext_item_candidate_id ext_gone"), this.candidate_won); j++;
								$(candidatewrapper).append(candidate_id_div, candidate_name_div, candidate_party_div, candidate_popular_vote_div, candidate_popular_percent_div, candidate_won_div);
								$(racewrapper).append(candidatewrapper);
								if(cannum==0){
									party = candidate_party;
								}
							}
							if(parseInt(candidate_popular_vote)==parseInt(leader) && parseInt(candidate_popular_vote)!=0){
								switch(candidate_party){
									case 'Dem':{ demVotes = parseInt(demVotes) + parseInt(this.candidate_won); break; }
									case 'GOP':{ repVotes = parseInt(repVotes) + parseInt(this.candidate_won); break; }
								}
							}
							cannum++;
						});
						var candidate_show = 0;
						$(racewrapper).children().each(function(){
							if(candidate_show>=2){
								$(this).addClass('ext_gone');
							}
							candidate_show++;
						});
						$(racewrapper).append(pct_precincts_reporting_div);
						$(racewrapper).prepend(raceinfowrapper);
						party = (party=='')? 'N' : party;
						var color = {cDemSt:"#0036d0",cDemWk:"#718CD9",cGOPSt:"#c01720",cGOPWk:"#D17D81",cGrnSt:"#336600",cGrnWk:"#99B380",cLibSt:"#580158",cLibWk:"#AC80AC",cCSt:"#FFCC33",cCWk:"#FFCC33",cNSt:"#e2e2e2",cNWk:"#e2e2e2"};
						$('path[id$='+state_code+']').each(function(){
							this.setAttribute('fill', color['c'+party+percent_reporting]);
						});
					});
					var demPct = 100 * (demVotes / 538);
					var repPct = 100 * (repVotes / 538);
					$('.bar .dem').width(demPct + '%').text(demVotes);
					$('.bar .rep').width(repPct + '%').text(repVotes);
					var thestate = $("#ext_selected_path").val();
					var thedivid = $("#ext_selected_div").val();
					var theselecteddiv = $('div[id='+thedivid+']');
					toggleState(thestate, theselecteddiv);
					timer = setInterval(function(){initializeNationalData()},60000);
				}
				function toggleState(state, thediv, thepath) {
					$("#ext_selected_path").val(state);
					$("#ext_selected_div").val($(thediv).attr('id'));
					$('div[id^="ext_race_wrapper"]').each(function(){
						$(this).addClass('ext_gone');
					});
					if($(thediv).hasClass('ext_gone')){
						$(thediv).removeClass('ext_gone');
						$("#ext_race_info_full").removeClass('ext_info_default');
						$("#ext_race_info_full").addClass('ext_info_2can');
					} else {
						$(thediv).addClass('ext_gone');
						$("#ext_race_info_full").addClass('ext_info_default');
						$("#ext_race_info_full").removeClass('ext_info_2can');
					}
					if($("#ext_selected_path").val()=='' && $("#ext_selected_div").val()==''){
						$("#ext_race_info").addClass('ext_info_default');
					}
				}
			});
		</script>
		<?php
		break;
	}
	case 'state':
	{ 
		switch($therace){
			case 'state_senate':
			{
				
			}
			case 'state_house':
			{
				
			}
			default:
			{ ?>
				<div id="map_page_wrapper_outter-president" class="map_page_wrapper_outter gone">
					<div id="map_page_wrapper_inner-president" class="map_page_wrapper_inner">
						<div id="map_frame-president" class="map_frame"><?=$state_svg?></div>
						<div id="race_info-president" class="race_info info_default">
							<input type="hidden" id="selected_div-president" name="selected_div" val="" />
							<input type="hidden" id="selected_path-president" name="selected_path" val="" />
						</div>
					</div>
				</div>
				<div id="map_page_wrapper_outter-governor" class="map_page_wrapper_outter">
					<div id="map_page_wrapper_inner-governor" class="map_page_wrapper_inner">
						<div id="map_frame-governor" class="map_frame"><?=$state_svg?></div>
						<div id="race_info-governor" class="race_info info_default">
							<input type="hidden" id="selected_div-governor" name="selected_div" val="race_wrapper-1620" />
							<input type="hidden" id="selected_path-governor" name="selected_path" val="18097" />
						</div>
					</div>
				</div>
				<div id="map_page_wrapper_outter-senate" class="map_page_wrapper_outter gone">
					<div id="map_page_wrapper_inner-senate" class="map_page_wrapper_inner">
						<div id="map_frame-senate" class="map_frame"><?=$state_svg?></div>
						<div id="race_info-senate" class="race_info info_default">
							<input type="hidden" id="selected_div-senate" name="selected_div" val="" />
							<input type="hidden" id="selected_path-senate" name="selected_path" val="" />
						</div>
					</div>
				</div>
				<div id="nav_controls" class="nav_controls">
					<div id="nav_button-president" class="nav_button">President</div>
					<div id="nav_button-governor" class="nav_button nav_selected">Governor</div>
					<div id="nav_button-senate" class="nav_button">Senate</div>
					<div id="nav_refresh_data" class="nav_refresh_data"></div>
					<input type="hidden" id="nav_current_tab" name="nav_current_tab" value="<?=$therace?>">
				</div>
				<!--<div id="stdwww" class="stdwww"></div>-->
				<script language="JavaScript">
					var timer = "";
					var current_tab = "<?=$therace?>"
					$(document).ready(function() {
						document.documentElement.style.overflowX = 'hidden';
						document.documentElement.style.overflowY = 'hidden';
						document.body.scroll = "no";	// ie only
						$("#nav_button-president").bind('click', function(){ switchTabs('president', this); });
						$("#nav_button-governor").bind('click', function(){ switchTabs('governor', this); });
						$("#nav_button-senate").bind('click', function(){ switchTabs('senate', this); });
						$("#nav_refresh_data").bind('click', function(){ initializeData(); });
						initializeData();
						
						function initializeData(){
							//window.clearInterval(timer);
							var current_tab = $("#nav_current_tab").val();
							theSource = "<?=$base_url?>index.php/election2012/fetchResultsForDisplay/";
							theData = 'organization=<?=$theorganization?>&scope=<?=$thescope?>&code=<?=$thecode?>&race=' + current_tab + '&size=<?=$thesize?>';
							fetchData(theSource, theData, setColors);
						}
						function setColors(data){
							$('div[id^="race_wrapper-"]').each(function(){
								$(this).detach();
							});
							var current_tab = $("#nav_current_tab").val();
							$(data).each(function(index){
								var i=0;
								var county_fip						= data[index].county_fip;
								var race_candidates					= data[index].race_candidates;
								var percent_reporting				= (data[index].race_result_precincts_percent>59)? 'St' : 'Wk';
								var leader							= 0;
								var party							= "";
								var color							= "";
								var cannum							= 0;
								var racewrapper 					= addElement("div", new Array("id|race_wrapper-" + data[index].race_id,"class|race_wrapper gone"));
								var raceinfowrapper 				= addElement("div", new Array("id|race_info_wrapper-" + data[index].race_id,"class|race_info_wrapper"));
								var racetoggle 						= addElement("input", new Array("type|hidden","id|race_toggle-" + data[index].race_id,"class|race_toggle","value|0"));
								var county_name						= addElement("div", new Array("id|race_item-" + i,"class|race_item item_county"), data[index].county_name.toUpperCase() + ' COUNTY'); i++;
								var race_id							= addElement("div", new Array("id|race_item-" + i,"class|race_item item_race_id gone"), data[index].race_id); i++;
								var race_status_id					= addElement("div", new Array("id|race_item-" + i,"class|race_item item_status gone"), data[index].race_status_id); i++;
								var race_title						= addElement("div", new Array("id|race_item-" + i,"class|race_item item_title"), data[index].race_title.toUpperCase()); i++;
								var race_title_short				= addElement("div", new Array("id|race_item-" + i,"class|race_item item_title_short gone"), data[index].race_title_short); i++;
								var race_name						= addElement("div", new Array("id|race_item-" + i,"class|race_item item_name gone"), data[index].race_name); i++;
								var race_updated					= addElement("div", new Array("id|race_item-" + i,"class|race_item item_date gone"), data[index].race_updated); i++;
								var race_result_id					= addElement("div", new Array("id|race_item-" + i,"class|race_item item_result_id gone"), data[index].race_result_id); i++;
								var county_fip_text					= addElement("div", new Array("id|race_item-" + i,"class|race_item item_fips gone"), data[index].county_fip); i++;
								var race_result_precincts_reporting = addElement("div", new Array("id|race_item-" + i,"class|race_item item_reporting gone"), data[index].race_result_precincts_reporting); i++;
								var race_result_precincts_percent	= addElement("div", new Array("id|race_item-" + i,"class|race_item item_race_percent"), data[index].race_result_precincts_percent + '% REPORTING'); i++;
								var race_result_total				= addElement("div", new Array("id|race_item-" + i,"class|race_item item_total gone"), data[index].race_result_total); i++;
								var race_result_total_percent		= addElement("div", new Array("id|race_item-" + i,"class|race_item item_total_percent gone"), data[index].race_result_total_percent); i++;
								var source_id						= addElement("div", new Array("id|race_item-" + i,"class|race_item item_race_source gone"), data[index].source_id); i++;
								var source_value					= addElement("div", new Array("id|race_item-" + i,"class|race_item gone"), data[index].source_value); i++;
								var race_status_name				= addElement("div", new Array("id|race_item-" + i,"class|race_item gone"), data[index].race_status_name); i++;
								$(raceinfowrapper).append(county_name, race_id, race_status_id, race_title, race_title_short, race_name, race_updated, race_result_id, county_fip_text, race_result_precincts_reporting, race_result_total, race_result_total_percent, source_id, source_value, race_status_name, racetoggle);
								var thepath = $('path[id$='+county_fip+']');
								thepath.attr('style', 'cursor:pointer;');
								thepath.bind('click', function(){ toggleCounty(county_fip, racewrapper, thepath); });
								$(racewrapper).bind('click', function(){ toggleCounty(county_fip, racewrapper); });
								var candidate_show = 0;
								$.each(race_candidates, function(){
									var j								= 0;
									var candidate_party_code			= (!this.candidate_party_code)? 'N' : this.candidate_party_code;
									var candidate_result_value			= this.candidate_result_value;
									party 								= (candidate_result_value==0)? 'N' : candidate_party_code;
									var candidate_result_percent		= this.candidate_result_percent;
									var gone 							= (cannum<=1)? '': ' gone';
									if(this.candidate_party_code=='D' || this.candidate_party_code=='R') {
										var candidatewrapper 				= addElement("div", new Array("id|candidate_wrapper-" + this.candidate_id,"class|candidate_wrapper"));
										var candidate_id					= addElement("div", new Array("id|candidate_item-" + j,"class|candidate_item item_candidate_id gone"), this.candidate_id); j++;
										var source_value					= addElement("div", new Array("id|candidate_item-" + j,"class|candidate_item item_candidate_source gone"), this.source_value); j++;
										var candidate_name					= addElement("div", new Array("id|candidate_item-" + j,"class|candidate_item item_name"), this.candidate_first + ' ' + this.candidate_mi + ' ' + this.candidate_last + ' (' + candidate_party_code + ')'); j++;
										var candidate_incumbent				= addElement("div", new Array("id|candidate_item-" + j,"class|candidate_item item_incumbent gone"), this.candidate_incumbent); j++;
										var candidate_winner				= addElement("div", new Array("id|candidate_item-" + j,"class|candidate_item item_winner gone"), this.candidate_winner); j++;
										var candidate_result_value_text		= addElement("div", new Array("id|candidate_item-" + j,"class|candidate_item item_result gone"),  this.candidate_result_value); j++;
										var candidate_result_percent_text	= addElement("div", new Array("id|candidate_item-" + j,"class|candidate_item item_candidate_percent"), this.candidate_result_percent + '%'); j++;
										$(candidatewrapper).append(candidate_id, source_value, candidate_name, candidate_incumbent, candidate_winner, candidate_result_value_text, candidate_result_percent_text);
										$(racewrapper).prepend(candidatewrapper);
									}
									cannum++;
								});
								$(racewrapper).children().each(function(){
									if(candidate_show>1){
										$(this).addClass('gone');
									}
									candidate_show++;
								});
								$(racewrapper).append(race_result_precincts_percent);
								$(racewrapper).prepend(raceinfowrapper);
								$('#race_info-' + current_tab).append(racewrapper);
								party = (party=='')? 'N' : party;
								var color = {cDSt:"#0036d0",cDWk:"#718CD9",cRSt:"#c01720",cRWk:"#D17D81",cGSt:"#336600",cGWk:"#99B380",cLSt:"#580158",cLWk:"#AC80AC",cCSt:"#FFCC33",cCWk:"#FFCC33",cNSt:"#e2e2e2",cNWk:"#e2e2e2"};
								$('#map_frame-' + current_tab + ' path[id$='+county_fip+']').each(function(){
									this.setAttribute('fill', color['c'+party + percent_reporting]);
								});
							});
							var thefip = $('#selected_path-' + current_tab).val();
							var thedivid = $('#selected_div-' + current_tab).val();
							var theselecteddiv = $('div[id='+thedivid+']');
							$('#stdwww').addClass('gone');
							toggleCounty(thefip, theselecteddiv);
							//timer = setInterval(function(){initializeData()},60000);
						}
						function toggleCounty(fip, thediv, thepath) {
							var current_tab = $("#nav_current_tab").val();
							$('#selected_path-' + current_tab).val(fip);
							$('#selected_div-' + current_tab).val($(thediv).attr('id'));
							$('div[id^="race_wrapper"]').each(function(){
								$(this).addClass('gone');
							});
							if($(thediv).hasClass('gone')){
								$(thediv).removeClass('gone');
								$('#race_info-' + current_tab).removeClass('info_default');
								$('#race_info-' + current_tab).addClass('info_2can');
							} else {
								$(thediv).addClass('gone');
								$('#race_info-' + current_tab).addClass('info_default');
								$('#race_info-' + current_tab).removeClass('info_default');
							}
							if($('#selected_path-' + current_tab).val()=='' && $('#selected_div-' + current_tab).val()==''){
								$('#race_info-' + current_tab).addClass('info_default');
							}
						}
						function switchTabs(thetab, thebutton){
							//window.clearInterval(timer);
							$('#stdwww').removeClass('gone');
							$("#nav_current_tab").val(thetab);
							$('div[id^=map_page_wrapper_outter-]').each(function(){
								$(this).addClass('gone');
							});
							$('#map_page_wrapper_outter-' + thetab).removeClass('gone');
							$('div[id^=nav_button-]').each(function(){
								$(this).removeClass('nav_selected');
							});
							$(thebutton).addClass('nav_selected');
							initializeData();
						}
					});
				</script>
				<?php
			}
		}
		break;
	}
	case 'pstate':
	{ 
		switch($therace){
			case 'state_senate':
			{
				
			}
			case 'state_house':
			{
				
			}
			default:
			{ ?>
				<div id="ext_map_page_wrapper_outter" class="ext_map_page_wrapper_outter">
					<div id="ext_map_page_wrapper_inner" class="ext_map_page_wrapper_inner">
						<div id="ext_map_frame" class="ext_map_frame"><?=$state_svg?></div>
						<div id="ext_race_info" class="ext_race_info ext_info_default">
							<input type="hidden" id="ext_selected_div" name="ext_selected_div" val="" />
							<input type="hidden" id="ext_selected_path" name="ext_selected_path" val="" />
						</div>
					</div>
				</div>
				<script language="JavaScript">
					var timer = "";
					$(document).ready(function() {
						document.documentElement.style.overflowX = 'hidden';
						document.documentElement.style.overflowY = 'hidden';
						document.body.scroll = "no";	// ie only
						initializeData();
						
						function initializeData(){
							window.clearInterval(timer);
							theSource = "<?=$base_url?>index.php/election2012/fetchResultsForDisplay/";
							theData = "organization=<?=$theorganization?>&scope=<?=$thescope?>&code=<?=$thecode?>&race=<?=$therace?>&size=<?=$thesize?>";
							fetchData(theSource, theData, setColors);
						}
						function setColors(data){
							$('div[id^="ext_race_wrapper-"]').each(function(){
								$(this).detach();
							});
							$(data).each(function(index){
								var i=0;
								var county_fip						= data[index].county_fip;
								var race_candidates					= data[index].race_candidates;
								var percent_reporting				= (data[index].race_result_precincts_percent>59)? 'St' : 'Wk';
								var leader							= 0;
								var party							= "";
								var color							= "";
								var cannum							= 0;
								var racewrapper 					= addElement("div", new Array("id|ext_race_wrapper-" + data[index].race_id,"class|ext_race_wrapper ext_gone"));
								var raceinfowrapper 				= addElement("div", new Array("id|ext_race_info_wrapper-" + data[index].race_id,"class|ext_race_info_wrapper"));
								var racetoggle 						= addElement("input", new Array("type|hidden","id|ext_race_toggle-" + data[index].race_id,"class|ext_race_toggle","value|0"));
								var county_name						= addElement("div", new Array("id|ext_race_item-" + i,"class|ext_race_item ext_item_county"), data[index].county_name.toUpperCase()); i++;
								var race_id							= addElement("div", new Array("id|ext_race_item-" + i,"class|ext_race_item ext_item_race_id ext_gone"), data[index].race_id); i++;
								var race_status_id					= addElement("div", new Array("id|ext_race_item-" + i,"class|ext_race_item ext_item_status ext_gone"), data[index].race_status_id); i++;
								var race_title						= addElement("div", new Array("id|ext_race_item-" + i,"class|ext_race_item ext_item_title"), data[index].race_title.toUpperCase()); i++;
								var race_title_short				= addElement("div", new Array("id|ext_race_item-" + i,"class|ext_race_item ext_item_title_short ext_gone"), data[index].race_title_short); i++;
								var race_name						= addElement("div", new Array("id|ext_race_item-" + i,"class|ext_race_item ext_item_name gone"), data[index].race_name); i++;
								var race_updated					= addElement("div", new Array("id|ext_race_item-" + i,"class|ext_race_item ext_item_date gone"), data[index].race_updated); i++;
								var race_result_id					= addElement("div", new Array("id|ext_race_item-" + i,"class|ext_race_item ext_item_result_id ext_gone"), data[index].race_result_id); i++;
								var county_fip_text					= addElement("div", new Array("id|ext_race_item-" + i,"class|ext_race_item ext_item_fips gone"), data[index].county_fip); i++;
								var race_result_precincts_reporting = addElement("div", new Array("id|ext_race_item-" + i,"class|ext_race_item ext_item_reporting ext_gone"), data[index].race_result_precincts_reporting); i++;
								var race_result_precincts_percent	= addElement("div", new Array("id|ext_race_item-" + i,"class|ext_race_item ext_item_race_percent"), data[index].race_result_precincts_percent + '% REPORTING'); i++;
								var race_result_total				= addElement("div", new Array("id|ext_race_item-" + i,"class|ext_race_item ext_item_total gone"), data[index].race_result_total); i++;
								var race_result_total_percent		= addElement("div", new Array("id|ext_race_item-" + i,"class|ext_race_item ext_item_total_percent ext_gone"), data[index].race_result_total_percent); i++;
								var source_id						= addElement("div", new Array("id|ext_race_item-" + i,"class|ext_race_item ext_item_race_source ext_gone"), data[index].source_id); i++;
								var source_value					= addElement("div", new Array("id|ext_race_item-" + i,"class|ext_race_item ext_gone"), data[index].source_value); i++;
								var race_status_name				= addElement("div", new Array("id|ext_race_item-" + i,"class|ext_race_item ext_gone"), data[index].race_status_name); i++;
								$(raceinfowrapper).append(county_name, race_id, race_status_id, race_title, race_title_short, race_name, race_updated, race_result_id, county_fip_text, race_result_precincts_reporting, race_result_total, race_result_total_percent, source_id, source_value, race_status_name, racetoggle);
								var thepath = $('path[id$='+county_fip+']');
								thepath.attr('style', 'cursor:pointer;');
								thepath.bind('click', function(){ toggleCounty(county_fip, racewrapper, thepath); });
								$(racewrapper).bind('click', function(){ toggleCounty(county_fip, racewrapper); });
								var candidate_show = 0;
								$.each(race_candidates, function(){
									var j								= 0;
									var candidate_party_code			= new String((!this.candidate_party_code)? 'N' : this.candidate_party_code);
									var candidate_result_value			= this.candidate_result_value;
									party 								= (candidate_result_value==0)? 'N' : candidate_party_code;
									var candidate_result_percent		= this.candidate_result_percent;
									var gone 							= (cannum<=1)? '': ' gone';
									if(candidate_party_code=='D' || 'R') {
										var candidatewrapper 				= addElement("div", new Array("id|ext_candidate_wrapper-" + this.candidate_id,"class|ext_candidate_wrapper color" + candidate_party_code));
										var candidate_id					= addElement("div", new Array("id|ext_candidate_wrapper-" + j,"class|ext_candidate_item ext_item_candidate_id ext_gone"), this.candidate_id); j++;
										var source_value					= addElement("div", new Array("id|ext_candidate_wrapper-" + j,"class|ext_candidate_item ext_item_candidate_source ext_gone"), this.source_value); j++;
										var candidate_name					= addElement("div", new Array("id|ext_candidate_wrapper-" + j,"class|ext_candidate_item ext_item_name"), this.candidate_first + ' ' + this.candidate_mi + ' ' + this.candidate_last + ' (' + candidate_party_code + ')'); j++;
										var candidate_incumbent				= addElement("div", new Array("id|ext_candidate_wrapper-" + j,"class|ext_candidate_item ext_item_incumbent ext_gone"), this.candidate_incumbent); j++;
										var candidate_winner				= addElement("div", new Array("id|ext_candidate_wrapper-" + j,"class|ext_candidate_item ext_item_winner ext_gone"), this.candidate_winner); j++;
										var candidate_result_value_text		= addElement("div", new Array("id|ext_candidate_wrapper-" + j,"class|ext_candidate_item ext_item_result ext_gone"),  this.candidate_result_value); j++;
										var candidate_result_percent_text	= addElement("div", new Array("id|ext_candidate_wrapper-" + j,"class|ext_candidate_item ext_item_candidate_percent"), this.candidate_result_percent + '%'); j++;
										$(candidatewrapper).append(candidate_id, source_value, candidate_name, candidate_incumbent, candidate_winner, candidate_result_value_text, candidate_result_percent_text);
										$(racewrapper).prepend(candidatewrapper);
									}
									cannum++;
								});
								$(racewrapper).children().each(function(){
									if(candidate_show>1){
										$(this).addClass('ext_gone');
									}
									candidate_show++;
								});
								$(racewrapper).append(race_result_precincts_percent);
								$(racewrapper).prepend(raceinfowrapper);
								$("#ext_race_info").append(racewrapper);
								party = (party=='')? 'N' : party;
								var color = {cDSt:"#0036d0",cDWk:"#718CD9",cRSt:"#c01720",cRWk:"#D17D81",cGSt:"#336600",cGWk:"#99B380",cLSt:"#580158",cLWk:"#AC80AC",cCSt:"#FFCC33",cCWk:"#FFCC33",cNSt:"#e2e2e2",cNWk:"#e2e2e2"};
								$('path[id$='+county_fip+']').each(function(){
									this.setAttribute('fill', color['c'+party + percent_reporting]);
								});
							});
							var thefip = $("#ext_selected_path").val();
							var thedivid = $("#ext_selected_div").val();
							var theselecteddiv = $('div[id='+thedivid+']');
							toggleCounty(thefip, theselecteddiv);
							timer = setInterval(function(){initializeData()},60000);
						}
						function toggleCounty(fip, thediv, thepath) {
							$("#ext_selected_path").val(fip);
							$("#ext_selected_div").val($(thediv).attr('id'));
							$('div[id^="ext_race_wrapper"]').each(function(){
								$(this).addClass('ext_gone');
							});
							if($(thediv).hasClass('ext_gone')){
								$(thediv).removeClass('ext_gone');
								$("#ext_race_info").removeClass('ext_info_default');
								$("#ext_race_info").addClass('ext_info_2can');
							} else {
								$(thediv).addClass('ext_gone');
								$("#ext_race_info").addClass('ext_info_default');
								$("#ext_race_info").removeClass('ext_info_2can');
							}
							if($("#ext_selected_path").val()=='' && $("#ext_selected_div").val()==''){
								$("#ext_race_info").addClass('ext_state_info_default');
							}
						}
					});
				</script>
				<?php
			}
		}
		break;
	}
	case "apxml":
	{
		echo($xml);
		exit;
	}
	default:
	{ ?>
		Hello
		<?php
	}
}
?>
<pre>
	
</pre>