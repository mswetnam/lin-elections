<?php
//--------------------------------------
// Election 2012 controller
// Author: Matthew Swetnam
// On behalf of: WISH-TV
//--------------------------------------

class election2012 extends CI_Controller {

	public function index($op=NULL, $id=NULL)
	{
		((!file_exists('ci/application/views/election2012/election2012.php'))? show_404() : '');
		$data = $this->initialize();
		$this->load->library(array('Utilities'));
		$data['op'] = $op = (($op)? $op : '');
		$data['id'] = $id = (($id)? $id : '');
		$data['styles']				= array(
			base_url()."ci/_common/css/coreAdmin.css",
			base_url()."ci/_common/css/election2012.css",
			base_url()."ci/_common/css/anytime.css",
			base_url()."ci/_common/css/jquery-ui-1.8.22.custom.css"
		);
		$data['scripts']			= array(
			"https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js",
			"https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/jquery-ui.min.js",
			"https://cdn.jquerytools.org/1.2.7/all/jquery.tools.min.js",
			base_url()."ci/_common/js/anytime.js",
			base_url().'ci/_common/js/jquery.pajinate.min.js',
			base_url().'ci/_common/js/coreAdmin.js'
		);
		$station_id = (($this->session->userdata('current_station'))? $this->session->userdata('current_station') : ((array_key_exists("wish_user", $data))? ((array_key_exists("wish_prime_station", $data['wish_user']))? $data['wish_user']['wish_prime_station'] : 1) : 1));
		$data['station_id'] = $station_id;
		
		// Final catch-all for header info
		$data['custom_header']		= "";
		// Operation
		$this->load->model('Election2012_model', '', TRUE);
		// Get module preferences and set as variable
		$like_where = array(
			array('where_value'=>'organization_id', 'where_equalto'=>$station_id, 'where_type'=>'and'),
			array('where_value'=>'preference_key', 'where_equalto'=>'preference', 'where_type'=>'like'),
			array('where_value'=>'preference_key', 'where_equalto'=>$data['module_id'], 'where_type'=>'like')
		);
		$module_preferences = $this->Election2012_model->getOrganizationPreference("preference_key, preference_value", NULL, $like_where, NULL, NULL, false);
		$mod_prefs = array();
		foreach($module_preferences as $preference) {
			$temp = array();
			$temp = $preference['preference_key'];
			$temp = explode("|", $temp);
			$preference['preference_key'] = $temp[2];
			$mod_prefs[$preference['preference_key']] = $preference['preference_value'];
		}
		$module_preferences = array();
		$module_preferences = $mod_prefs;
		$data["module_preferences"] = $module_preferences;
		// Set template parts
		$page = "election2012";
		// Set template parts
		//$this->output->enable_profiler(TRUE);
		$this->load->view('templates/header', $data);
		$this->load->view('election2012/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	public function initialize()
	{
		// Load libraries
		$this->load->library('Coreadministration');
		// Set base variables
		if ($this->agent->is_browser()) {
			$agent = $this->agent->browser().'|'.$this->agent->version();
		} elseif ($this->agent->is_robot()) {
			$agent = $this->agent->robot();
		} elseif ($this->agent->is_mobile()) {
			$agent = $this->agent->mobile();
		} else {
			$agent = 'Unidentified';
		}
		$data['agent']				= $agent;
		$data['platform']			= $this->agent->platform();
		$data['wish_user']			= $this->session->all_userdata();
		$data['wish_permissions']		= ((array_key_exists("wish_permissions", $data['wish_user']))? $data['wish_user']['wish_permissions'] : array());
		// Load custom configs
		$this->load->model('Coreadmin_model', '', TRUE);
		$module_id = $this->Coreadmin_model->getModule("module_id", NULL, array('module_dirname'=>$this->uri->segment(1)), NULL, 1, false);
		$data['module_id'] 			= $module_id		= ((array_key_exists(0, $module_id))? ((array_key_exists('module_id', $module_id[0]))? $module_id[0]['module_id'] : 0) : 0);
		$data['configurations'] 	= $configurations	= $this->Coreadmin_model->getConfiguration(NULL, NULL, array('module_id'=>$module_id));
		foreach($configurations as $configuration) {
			if($configuration['configuration_name']=="page_name") {
				$page_array = $this->coreadministration->stringToMulti($configuration['configuration_value']);
				$this->config->set_item("page_name", (($this->uri->segment(2))? $page_array[$this->uri->segment(2)] : 'Core Administration'));
			} else {
				$this->config->set_item($configuration['configuration_name'], $configuration['configuration_value']);
			}
		}
		// Take file name, split the camel case and capitalize first letter to get the page title
		$data['title']				= $this->config->item('page_name');
		// Set meta content variables
		$data['meta_robots']		= $this->config->item('meta_robots');
		$data['meta_keywords']		= $this->config->item('meta_keywords');
		$data['meta_description']	= $this->config->item('meta_description');
		$data['meta_rating']		= $this->config->item('meta_rating');
		$data['meta_author']		= $this->config->item('meta_author');
		$data['meta_copyright']		= $this->config->item('meta_copyright');
		// Set additional header info
		$data['rss']				= "";
		$data['favicon']			= "";
		$data['base_url']			= base_url();
		return $data;
	}

	public function manageElection($op=NULL, $id=NULL)
	{
		((!file_exists('ci/application/views/admin/modules/election2012/election2012.php'))? show_404() : '');
		$data = $this->initialize();
		$this->load->library(array('Utilities','electiondatalib'));
		$data['op'] = $op = (($op)? $op : '');
		$data['id'] = $id = (($id)? $id : '');
		$data['styles']				= array(
			$data['base_url']."ci/_common/css/coreAdmin.css",
			$data['base_url']."ci/_common/css/election2012.css",
			$this->config->item('jquery_ui_css_current'),
			"https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css",
			"https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables_themeroller.css",
			$data['base_url']."ci/_common/css/jquery.jgrowl.min.css",
			$data['base_url']."ci/_common/css/anytime.css"
		);
		$data['scripts']			= array(
			$this->config->item('jquery_current'),
			"https://code.jquery.com/jquery-migrate-1.0.0.js",  // Added to fix .browser deprication in Jquery
			$this->config->item('jquery_ui_local'),
			$this->config->item('jquery_tools_current'),
			$this->config->item('jquery_validation_current'),
			$this->config->item('jquery_datatables_current'),
			$data['base_url']."ci/_common/js/coreAdmin.js",
			$data['base_url']."ci/_common/js/anytime.js"
		);
		// Final catch-all for header info
		$data['custom_header']		= "";
		$data['station_id'] = $station_id = (($this->session->userdata('current_station'))? $this->session->userdata('current_station') : ((array_key_exists("wish_user", $data))? ((array_key_exists("wish_prime_station", $data['wish_user']))? $data['wish_user']['wish_prime_station'] : 1) : 1));
		$this->load->model('Coreadmin_model', '', TRUE);
		$this->load->model('Election2012_model', '', TRUE);
		// Get module preferences and set as variable
		$like_where = array(
			array('where_value'=>'organization_id', 'where_equalto'=>$station_id, 'where_type'=>'and'),
			array('where_value'=>'preference_key', 'where_equalto'=>'preference', 'where_type'=>'like'),
			array('where_value'=>'preference_key', 'where_equalto'=>$data['module_id'], 'where_type'=>'like')
		);
		$data['testpref'] = $module_preferences = $this->Election2012_model->getOrganizationPreference("preference_key, preference_value", NULL, $like_where, NULL, NULL, false);
		$mod_prefs = array();
		foreach($module_preferences as $preference) {
			$temp = array();
			$temp = $preference['preference_key'];
			$temp = explode("|", $temp);
			$preference['preference_key'] = $temp[2];
			$mod_prefs[$preference['preference_key']] = $preference['preference_value'];
		}
		$data["module_preferences"] = $module_preferences = $mod_prefs;
		switch($op)
		{
			// Party functions
			case 'party':
			{
				$data['party_id'] = (($id)? $id : NULL);
				$fields = "party_id, party_name, party_name_short, party_icon";
				$data['parties'] = $this->Election2012_model->getParty($fields, NULL, NULL, NULL, NULL, false);
				$page = "election2012";
				break;
			}
			case 'addparty': { $data['party_id'] = (($id)? $id : ''); $data['parties'] = array(); $page = "election2012"; break; }
			case 'editparty':
			{
				$data['party_id'] = (($id)? $id : NULL);
				if($id) {
					$fields = "party_id, party_name, party_name_short, party_icon";
					$data['parties'] = $this->Election2012_model->getParty($fields, NULL, array('party_id'=>$id), NULL, 1, false);
				} else {
					$data['parties'] = array();
				}
				$page = "election2012";
				break;
			}
			case 'saveparty':
			{
				$party_id			= $_POST['party_id'];
				$party_name			= $_POST['party_name'];
				$party_name_short	= $_POST['party_name_short'];
				$party_icon			= $_POST['party_icon'];
				$party_array = array(
					'party_id'			=> $party_id,
					'party_name'		=> $party_name,
					'party_name_short'	=> $party_name_short,
					'party_icon'		=> $party_icon,
					'party_updated'		=> time(),
					'party_updatedby'	=> $this->session->userdata('wish_user')
				);
				$check = $this->Election2012_model->getParty(NULL, NULL, array('party_id' => $party_id), NULL, true);
				$result = (($check)? $this->Election2012_model->updateParty($party_array, array('party_id'=>$party_array['party_id'])) : $this->Election2012_model->insertParty($party_array));
				//$this->db->cache_delete('election2012','manageElection');
				//$this->db->cache_delete('election2012','fetchPartyData');
				header("Location: ".base_url()."index.php/election2012/manageElection/party");
				/*echo "<pre>";
				print_r(!empty($result));
				echo "</pre>";*/
				exit;
			}
			case 'removeparty':
			{
				$parties = array();
				if($id) {
					$parties[0] = $id; 
				} else {
					$parties = (($_POST)? ((array_key_exists('admin_table_rowselect_hidden', $_POST))? explode(",", $_POST['admin_table_rowselect_hidden']) : array()) : array());
				}
				foreach($parties as $party) {
					$this->Election2012_model->deleteParty(array('party_id' => $party));
				}
				//$this->db->cache_delete('election2012','manageElection');
				//$this->db->cache_delete('election2012','fetchPartyData');
				header("Location: ".base_url()."index.php/election2012/manageElection/party".(($party_id)? '/'.$party_id : ''));
				exit;
			}
			// Race status functions
			case 'racestatus':
			{
				$data['race_status_id'] = (($id)? $id : NULL);
				$fields = "race_status_id, race_status_name, race_status_complete, race_status_flag";
				$data['statuses'] = $this->Election2012_model->getRaceStatus($fields, NULL, NULL, NULL, NULL, false);
				$page = "election2012";
				break;
			}
			case 'addracestatus': { $data['race_status_id'] = (($id)? $id : ''); $data['statuses'] = array(); $page = "election2012"; break; }
			case 'editracestatus':
			{
				$data['race_status_id'] = (($id)? $id : NULL);
				if($id) {
					$fields = "race_status_id, race_status_name, race_status_complete, race_status_flag";
					$data['statuses'] = $this->Election2012_model->getRaceStatus($fields, NULL, array('race_status_id'=>$id), NULL, 1, false);
				} else {
					$data['statuses'] = array();
				}
				$page = "election2012";
				break;
			}
			case 'saveracestatus':
			{
				$race_status_id			= $_POST['race_status_id'];
				$race_status_name		= $_POST['race_status_name'];
				$race_status_complete	= (($_POST['race_status_complete'])? 1 : 0);
				$race_status_flag		= (($_POST['race_status_flag'])? 1 : 0);
				$race_status_array = array(
					'race_status_id'		=> $race_status_id,
					'race_status_name'		=> $race_status_name,
					'race_status_complete'	=> $race_status_complete,
					'race_status_flag'		=> $race_status_flag,
					'race_status_updated'	=> time(),
					'race_status_updatedby'	=> $this->session->userdata('wish_user')
				);
				$check = $this->Election2012_model->getRaceStatus(NULL, NULL, array('race_status_id' => $race_status_id), NULL, true);
				$result = (($check)? $this->Election2012_model->updateRaceStatus($race_status_array, array('race_status_id'=>$race_status_array['race_status_id'])) : $this->Election2012_model->insertRaceStatus($race_status_array));
				//$this->db->cache_delete('election2012','manageElection');
				//$this->db->cache_delete('election2012','fetchRaceStatusData');
				header("Location: ".base_url()."index.php/election2012/manageElection/racestatus");
				exit;
			}
			case 'togglecomplete':
			{
				$statuses = array();
				if($id) {
					$statuses[0] = $id; 
				} else {
					$statuses = (($_POST)? ((array_key_exists('admin_table_rowselect_hidden', $_POST))? explode(",", $_POST['admin_table_rowselect_hidden']) : array()) : array());
				}
				foreach($statuses as $key=>$value) {
					// Set where clause info
					$where_array = array('race_status_id' => $value);
					$current = $this->Election2012_model->getRaceStatus(NULL, NULL, $where_array, NULL, NULL, false);
					$active = (($current)? (($current[0]['race_status_complete']==1)? 0 : 1) : 1);
					$complete_value = $active;
					$toggle_array = array(
						'race_status_complete'	=> $complete_value,
						'race_status_updated'	=>time(),
						'race_status_updatedby'	=>$this->session->userdata('wish_user')
					);
					$displayresult = $this->Election2012_model->updateRaceStatus($toggle_array, $where_array);
				}
				header("Location: ".base_url()."index.php/election2012/manageElection/racestatus");
				exit;
			}
			case 'toggleflag':
			{
				$statuses = array();
				if($id) {
					$statuses[0] = $id; 
				} else {
					$statuses = (($_POST)? ((array_key_exists('admin_table_rowselect_hidden', $_POST))? explode(",", $_POST['admin_table_rowselect_hidden']) : array()) : array());
				}
				foreach($statuses as $key=>$value) {
					// Set where clause info
					$where_array = array('race_status_id' => $value);
					$current = $this->Election2012_model->getRaceStatus(NULL, NULL, $where_array, NULL, NULL, false);
					$active = (($current)? (($current[0]['race_status_flag']==1)? 0 : 1) : 1);
					$flag_value = $active;
					$toggle_array = array(
						'race_status_flag'	=> $flag_value,
						'race_status_updated'	=>time(),
						'race_status_updatedby'	=>$this->session->userdata('wish_user')
					);
					$displayresult = $this->Election2012_model->updateRaceStatus($toggle_array, $where_array);
				}
				header("Location: ".base_url()."index.php/election2012/manageElection/racestatus");
				exit;
			}
			case 'removeracestatus':
			{
				$statuses = array();
				if($id) {
					$statuses[0] = $id; 
				} else {
					$statuses = (($_POST)? ((array_key_exists('admin_table_rowselect_hidden', $_POST))? explode(",", $_POST['admin_table_rowselect_hidden']) : array()) : array());
				}
				foreach($statuses as $status) {
					$this->Election2012_model->deleteRaceStatus(array('race_status_id' => $status));
				}
				//$this->db->cache_delete('election2012','manageElection');
				//$this->db->cache_delete('election2012','fetchRaceStatusData');
				header("Location: ".base_url()."index.php/election2012/manageElection/racestatus");
				exit;
			}
			// Race functions
			case 'race':
			{
				$page = "election2012";
				break;
			}
			case 'addrace':
			{
				$data['race_id'] = (($id)? $id : ''); 
				$data['races'] = array(); 
				$status_options = "race_status_id, race_status_name";
				$data['status_options'] = $this->Election2012_model->getRaceStatus($status_options, NULL, NULL, array('race_status_name'=>'ASC'), NULL, false); 
				break;
			}
			case 'editrace':
			{
				$id = (($_POST)? ((array_key_exists('admin_table_rowselect_hidden', $_POST))? $_POST['admin_table_rowselect_hidden'] : array()) : array());
				$data['race_id'] = $id;
				if($id) {
					$fields = "race_id, race_status_id, county_fip, race_title, race_title_short, race_name";
					$data['races'] = $this->Election2012_model->getRace($fields, NULL, array('race_id'=>$data['race_id']), NULL, 1, false);
				} else {
					$data['races'] = array();
				}
				$status_options = "race_status_id, race_status_name";
				$data['status_options'] = $this->Election2012_model->getRaceStatus($status_options, NULL, NULL, array('race_status_name'=>'ASC'), NULL, false);
				$page = "election2012";
				break;
			}
			case 'saverace':
			{
				$race_id			= $_POST['race_id'];
				$race_status_id		= $_POST['race_status_id'];
				//$county_fip			= $_POST['county_fip'];
				$race_title			= $_POST['race_title'];
				$race_title_short	= $_POST['race_title_short'];
				$race_name			= $_POST['race_name'];
				$race_array = array(
					'race_id'			=> $race_id,
					'race_status_id'	=> $race_status_id,
					//'county_fip'		=> $county_fip,
					'race_title'		=> $race_title,
					'race_title_short'	=> $race_title_short,
					'race_name'			=> $race_name,
					'race_updated'		=> time(),
					'race_updatedby'	=> $this->session->userdata('wish_user')
				);
				$check = $this->Election2012_model->getRace(NULL, NULL, array('race_id' => $race_id), NULL, true);
				$result = (($check)? $this->Election2012_model->updateRace($race_array, array('race_id'=>$race_array['race_id'])) : $this->Election2012_model->insertRace($race_array));
				//$this->db->cache_delete('election2012','manageElection');
				//$this->db->cache_delete('election2012','fetchRaceData');
				header("Location: ".base_url()."index.php/election2012/manageElection/race");
				exit;
			}
			case 'assignracefips':
			{
				$id = (($_POST)? ((array_key_exists('admin_table_rowselect_hidden', $_POST))? $_POST['admin_table_rowselect_hidden'] : 0) : 0);
				$data['race_id'] = $id;
				if($id) {
					$fields = "race_id, race_status_id, county_fip, race_title, race_title_short, race_name";
					$data['races'] = $this->Election2012_model->getRace($fields, NULL, array('race_id'=>$id), NULL, 1, false);
				} else {
					$data['races'] = array();
				}
				$county_options = "county_fip, county_name, state_id";
				$counties = $this->Coreadmin_model->getCounty($county_options, NULL, NULL, array('county_name'=>'ASC'), NULL, false);
				$county_data = array();
				foreach($counties as $county) {
					$check_select = $this->Election2012_model->getRaceCounty(NULL, NULL, array('county_fip'=>$county['county_fip'], 'race_id'=>$id), NULL, 1, false);
					$county['county_selected'] = (($check_select)? 1 : 0);
					$states = $this->Coreadmin_model->getState(NULL, NULL, array('state_id'=>$county['state_id']), NULL, 1, false);
					$county['state_name'] = ((array_key_exists(0, $states))? $states[0]['state_name'] : '');
					$county['state_code'] = ((array_key_exists(0, $states))? $states[0]['state_code'] : '');
					array_push($county_data, $county);
				}
				//$this->db->cache_delete('election2012','manageElection');
				//$this->db->cache_delete('election2012','fetchRaceData');
				$county_data = $this->utilities->multiColSort($county_data, 'state_name', SORT_ASC, 'county_name', SORT_ASC);
				$data['county_options'] = $county_data;
				$page = "election2012";
				break;
			}
			case 'saveracefips':
			{
				$race_id = $_POST['race_id'];
				$county_fips = $_POST['county_fip'];
				$this->Election2012_model->deleteRaceCounty(array('race_id' => $race_id));
				foreach($county_fips as $county_fip) {
					$insert_array = array(
						'race_id' => $race_id,
						'county_fip' => $county_fip
					);
					$this->Election2012_model->insertRaceCounty($insert_array);
				}
				//$this->db->cache_delete('election2012','manageElection');
				//$this->db->cache_delete('election2012','fetchRaceData');
				header("Location: ".base_url()."index.php/election2012/manageElection/race");
				exit;
			}
			case 'removerace':
			{
				$races = array();
				if($id) {
					$races[0] = $id; 
				} else {
					$races = (($_POST)? ((array_key_exists('admin_table_rowselect_hidden', $_POST))? explode(",", $_POST['admin_table_rowselect_hidden']) : array()) : array());
				}
				foreach($races as $race) {
					$this->Election2012_model->deleteRace(array('race_id' => $race));
				}
				//$this->db->cache_delete('election2012','manageElection');
				//$this->db->cache_delete('election2012','fetchRaceData');
				header("Location: ".base_url()."index.php/election2012/manageElection/race");
				exit;
			}
			// Candidate functions
			case 'candidate':
			{
				$page = "election2012";
				break;
			}
			case 'addcandidate': {
				$data['candidate_id'] = (($id)? $id : ''); 
				$data['candidates'] = array(); 
				$party_options = "party_id, party_name_short";
				$data['party_options'] = $this->Election2012_model->getParty($party_options, NULL, NULL, array('party_name_short'=>'ASC'), NULL, false);
				$race_options = "race_id, race_title_short";
				$data['race_options'] = $this->Election2012_model->getRace($race_options, NULL, NULL, array('race_title_short'=>'ASC'), NULL, false);
				$page = "election2012"; 
				break;
			}
			case 'editcandidate':
			{
				$data['candidate_id'] = (($id)? $id : NULL);
				if($id) {
					$fields = "candidate_id, party_id, race_id, candidate_first, candidate_mi, candidate_last, candidate_incumbent, candidate_winner";
					$data['candidates'] = $this->Election2012_model->getCandidate($fields, NULL, array('candidate_id'=>$id), NULL, 1, false);
				} else {
					$data['candidates'] = array();
				}
				$party_options = "party_id, party_name_short";
				$data['party_options'] = $this->Election2012_model->getParty($party_options, NULL, NULL, array('party_name_short'=>'ASC'), NULL, false);
				$race_options = "race_id, race_title_short";
				$data['race_options'] = $this->Election2012_model->getRace($race_options, NULL, NULL, array('race_title_short'=>'ASC'), NULL, false);
				$page = "election2012";
				break;
			}
			case 'savecandidate':
			{
				$candidate_id			= $_POST['candidate_id'];
				$party_id				= $_POST['party_id'];
				$race_id				= $_POST['race_id'];
				$candidate_first		= $_POST['candidate_first'];
				$candidate_mi			= $_POST['candidate_mi'];
				$candidate_last			= $_POST['candidate_last'];
				$candidate_incumbent	= (($_POST['candidate_incumbent'])? 1 : 0);
				$candidate_winner		= (($_POST['candidate_winner'])? 1 : 0);
				$candidate_array = array(
					'candidate_id'			=> $candidate_id,
					'party_id'				=> $party_id,
					'race_id'				=> $race_id,
					'candidate_first'		=> $candidate_first,
					'candidate_mi'			=> $candidate_mi,
					'candidate_last'		=> $candidate_last,
					'candidate_incumbent'	=> $candidate_incumbent,
					'candidate_winner'		=> $candidate_winner,
					'candidate_updated'		=> time(),
					'candidate_updatedby'	=> $this->session->userdata('wish_user')
				);
				$check = $this->Election2012_model->getCandidate(NULL, NULL, array('candidate_id' => $candidate_id), NULL, true);
				$result = (($check)? $this->Election2012_model->updateCandidate($candidate_array, array('candidate_id'=>$candidate_array['candidate_id'])) : $this->Election2012_model->insertCandidate($candidate_array));
				//$this->db->cache_delete('election2012','manageElection');
				//$this->db->cache_delete('election2012','fetchCandidateData');
				header("Location: ".base_url()."index.php/election2012/manageElection/candidate");
				exit;
			}
			case 'mergecandidates':
			{
				$candidates = (($_POST)? ((array_key_exists('admin_table_rowselect_hidden', $_POST))? explode(",", $_POST['admin_table_rowselect_hidden']) : array()) : array());
				$candidate_data = array();
				foreach($candidates as $k=>$v){
					$candidate_join = array(
						array('jn_table'=>'election_candidate_source_map', 'jn_value'=>'election_candidate_source_map.candidate_id', 'jn_equalto'=>'election_candidate.candidate_id')
					);
					$candidate_fields = "election_candidate.candidate_id, election_candidate.party_id, election_candidate.race_id, election_candidate.candidate_first, election_candidate.candidate_mi, election_candidate.candidate_last, election_candidate.candidate_incumbent, election_candidate.candidate_winner, election_candidate_source_map.source_id, election_candidate_source_map.source_value";
					$temp = $this->Election2012_model->getCandidate($candidate_fields, $candidate_join, array('election_candidate.candidate_id' => $v), NULL, false);
					foreach($temp as $temp){
						array_push($candidate_data, $temp);
					}
				}
				$data['candidates'] = $candidate_data;
				//$this->output->enable_profiler(TRUE);
				$page = "election2012";
				break;
				/*echo "<pre>";
				print_r($candidates);
				echo "</pre>";
				exit;*/
			}
			case 'savecandidatemerge':
			{
				$source = (($_POST)? ((array_key_exists("source_id", $_POST))? $_POST["source_id"] : array()) : array());
				$candidate_id = (($_POST)? ((array_key_exists("candidate_id", $_POST))? $_POST["candidate_id"] : 0) : 0);
				foreach($source as $k=>$v){
					list($cand_id, $source_id, $source_value) = explode("|", $v);
					$update_source_result = $this->Election2012_model->updateCandidateDataSource(array('candidate_id'=>$candidate_id), array('source_id'=>$source_id, 'source_value'=>$source_value));
					$update_race_jn_result = $this->Election2012_model->updateRaceCandidate(array('candidate_id'=>$candidate_id), array('candidate_id'=>$cand_id));
					$delete_result = $this->Election2012_model->deleteCandidate(array('candidate_id' => $cand_id));
				}
				//$this->db->cache_delete('election2012','manageElection');
				//$this->db->cache_delete('election2012','fetchCandidateData');
				header("Location: ".base_url()."index.php/election2012/manageElection/candidate");
				exit;
			}
			case 'removecandidate':
			{
				$candidates = array();
				if($id) {
					$candidates[0] = $id; 
				} else {
					$candidates = (($_POST)? ((array_key_exists('admin_table_rowselect_hidden', $_POST))? explode(",", $_POST['admin_table_rowselect_hidden']) : array()) : array());
				}
				foreach($candidates as $candidate) {
					$this->Election2012_model->deleteCandidate(array('candidate_id' => $candidate));
				}
				//$this->db->cache_delete('election2012','manageElection');
				//$this->db->cache_delete('election2012','fetchCandidateData');
				header("Location: ".base_url()."index.php/election2012/manageElection/candidate");
				exit;
			}
			case 'setstation':
			{
				$station_id = (($id)? $id : 1);
				$this->session->set_userdata(array('current_station'=>$station_id));
				header("Location: ".$_POST['admin_redirect']);
				exit;
			}
			case 'refreshdata':
			{
				//$this->db->cache_delete_all();
				header("Location: ".$_POST['admin_redirect']);
				exit;
			}
			case 'preference':
			{
				$preferences = "";
				foreach($data['configurations'] as $configuration) {
					if($configuration['configuration_name']=="module_preference") {
						$preferences = $configuration['configuration_value'];
					}
				}
				$preferences = explode("|", $preferences);
				$preferences_array = array();
				$item = array();
				foreach($preferences as $preference){
					$preference_key_list = "";
					$preference = explode(",", $preference);
					foreach($preference as $value){
						$value = explode(":", $value);
						$item[$value[0]] = $value[1];
					}
					array_push($preferences_array, $item);
				}
				if($module_preferences) {
					$temp = array();
					foreach($preferences_array as $element) {
						$field_value = "";
						if(array_key_exists($element['preference_name'], $module_preferences)==true) {
							$field_value = $module_preferences[$element['preference_name']];
						}
						$element['preference_field_value'] = $field_value;
						array_push($temp, $element);
					}
					$preferences_array = array();
					$preferences_array = $temp;
				}
				$data['preferences'] = $preferences_array;
				$page = "trafficMap";
				break;
			}
			case 'savepreference':
			{
				// Create key
				foreach($_POST as $key=>$value){
					switch($key)
					{
						case 'admin_cp_station':
						case 'admin_btn_edit':
						case 'admin_table_rowselect_hidden':
						case 'admin_redirect':
						{
							break;
						}
						default:
						{
							// Empty variables
							$preference_key = "";
							$where_array = array();
							$preference_array = array();
							// Build new variables
							$preference_key = "preference|".$data['module_id']."|".$key;
							$where_array = array('organization_id' => $_POST['admin_cp_station'], 'preference_key'=>$preference_key);
							$preference_array = array(
								'organization_id'			=>$_POST['admin_cp_station'],
								'preference_key'			=>$preference_key,
								'preference_value'			=>htmlentities($value),
								'preference_updated'		=>time(),
								'preference_updatedby'		=>$this->session->userdata('wish_user')
							);
							$this->Election2012_model->deleteOrganizationPreference($where_array);
							$result = $this->Election2012_model->insertOrganizationPreference($preference_array);
						}
					}
				}
				//$this->db->cache_delete('election2012','manageElection');
				//$this->db->cache_delete('election2012','fetchOrganizationPreference');
				header("Location: ".base_url()."index.php/election2012/manageElection/preference");
				/*echo "<pre>";
				print_r($_POST);
				echo "</pre>";*/
				exit;
			}
			case 'cleardata':
			{
				$i=0;
				$result = array();
				$tables = explode(",", $_POST['tables']);
				foreach($tables as $table) {
					$thefunction = 'clearAll'.$table;
					$clear_result = $this->Election2012_model->$thefunction();
					$temp = array('response'=>(($clear_result)? $table.' cleared successfully' : 'Unable to clear '.$table.' data'));
					array_push($result, $temp);
				}
				echo json_encode($result);
				exit;
			}
			case 'ingestdata':
			{
				log_message('info', 'Ingestion started.');
				$sources = array();
				if($id) {
					$sources[0] = $id; 
				} else {
					$sources = (($_POST)? ((array_key_exists('admin_cp_station', $_POST))? explode(",", $_POST['admin_cp_station']) : array()) : array());
				}
				foreach($sources as $source_id) {
					//$source_id = (($id)? $id : 0);
					if(is_numeric($source_id)) {
						$organization_where = array(array('where_value'=>'organization_id', 'where_equalto'=>$source_id, 'where_type'=>'and'));
						$result = $this->Coreadmin_model->getOrganization("organization_name_short", NULL, $organization_where, NULL, 1, false);
						$result = ((array_key_exists(0, $result))? ((array_key_exists("organization_name_short", $result[0]))? $result[0]['organization_name_short'] : 1) : 1);
						$source_id=$result;
					}
					$results = "";
					$temp = $this->ingestRaceData(strtolower($source_id));
					$results .= (($temp=="Ingestion Complete")? 'Ingestion complete for '.$source_id : 'Unable to ingest data for '.$source_id);
					echo json_encode($results);
				}
				/*echo "<pre>";
				print_r($sources);
				echo "</pre>";*/
				log_message('info', 'Ingestion complete.');
				exit;
			}
			default:
			{
				$pref_where = array(
					array('where_value'=>'preference_key', 'where_equalto'=>'preference|'.$data['module_id'].'|election_xml', 'where_type'=>'like'),
					array('where_value'=>'preference_value', 'where_equalto'=>'', 'where_type'=>'not')
				);
				$all_preferences = $this->Election2012_model->getOrganizationPreference("organization_id", NULL, $pref_where, array('organization_id'=>'ASC'), NULL, false);
				$organizations = array();
				foreach($all_preferences as $e) {
					array_push($organizations, $e['organization_id']);
				}
				$org_where = array(
					array('where_value'=>'organization_id', 'where_equalto'=>$organizations, 'where_type'=>'in')
				);
				$the_stations = $this->Coreadmin_model->getOrganization('organization_id, organization_name', NULL, $org_where, array('organization_name'=>'ASC'), NULL, false);
				$station_list = array();
				foreach($the_stations as $the_station){
					$station_list[$the_station['organization_id']] = $the_station['organization_name'];
				}
				$data['the_stations'] = $station_list;
			}
		}
		$page = "election2012";
		// Set template parts
		//$this->output->enable_profiler(TRUE);
		$this->load->view('templates/header', $data);
		$this->load->view('admin/modules/election2012/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	public function mapDisplay($org='wish', $scope='state', $code='in', $race='', $size='mid')
	{
		// Get variables
		$data['theorganization']	= $theorganization	= (($_POST)? (($_POST['organization'])? $_POST['organization'] : $org) : $org);
		$data['thescope']			= $thescope			= (($_POST)? (($_POST['scope'])? $_POST['scope'] : $scope) : $scope);
		$data['thecode']			= $thecode			= (($_POST)? (($_POST['code'])? $_POST['code'] : $code) : $code);
		$data['therace']			= $therace			= (($_POST)? (($_POST['race'])? $_POST['race'] : $race) : $race);
		$data['thesize']			= $thesize			= (($_POST)? (($_POST['size'])? $_POST['size'] : $size) : $size);
		((!file_exists('ci/application/views/election2012/election2012.php'))? show_404() : '');
		// Load libraries and/or helpers
		$this->load->helper('url','cookie');
		$this->load->library(array('session','user_agent','email','Coreadministration','Utilities','Xmlfeed','Electiondatalib'));
		// Set base variables
		if ($this->agent->is_browser()) {
			$agent = $this->agent->browser().'|'.$this->agent->version();
		} elseif ($this->agent->is_robot()) {
			$agent = $this->agent->robot();
		} elseif ($this->agent->is_mobile()) {
			$agent = $this->agent->mobile();
		} else {
			$agent = 'Unidentified';
		}
		$data['agent']		= $agent;
		$data['platform']	= $this->agent->platform();
		$data['wish_user']	= $this->session->all_userdata();
		// Load custom configs
		$this->load->model('Coreadmin_model', '', TRUE);
		$module_id = $this->Coreadmin_model->getModule("module_id", NULL, array('module_dirname'=>$this->uri->segment(1)), NULL, 1, false);
		$data['module_id'] = (($module_id)? ((array_key_exists(0, $module_id))? ((array_key_exists("module_id", $module_id))? $module_id[0]['module_id'] : 0) : 0) : 0);
		$configurations = $this->Coreadmin_model->getConfiguration(NULL, NULL, array('module_id'=>$module_id));
		foreach($configurations as $configuration) {
			$this->config->set_item($configuration['configuration_name'], $configuration['configuration_value']);
		}
		// Set page name
		$data['title']				= "WISH Maps";
		// Set meta content variables
		$data['meta_robots']		= $this->config->item('meta_robots');
		$data['meta_keywords']		= $this->config->item('meta_keywords');
		$data['meta_description']	= $this->config->item('meta_description');
		$data['meta_rating']		= $this->config->item('meta_rating');
		$data['meta_author']		= $this->config->item('meta_author');
		$data['meta_copyright']		= $this->config->item('meta_copyright');
		// Set additional header info
		$data['rss']				= "";
		$data['favicon']			= "";
		$data['styles']				= array(
			base_url()."ci/_common/css/election2012.css",
			base_url()."ci/_common/css/anytime.css",
			base_url()."ci/_common/css/jquery-ui-1.8.22.custom.css"
		);
		$data['scripts']			= array(
			base_url().'ci/_common/js/svgweb/svg.js',
			"https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js",
			"https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/jquery-ui.min.js",
			"https://cdn.jquerytools.org/1.2.7/all/jquery.tools.min.js",
			base_url()."ci/_common/js/anytime.js",
			base_url().'ci/_common/js/jquery.pajinate.min.js',
			base_url().'ci/_common/js/coreAdmin.js'
		);
		$base_url					= base_url();
		$data['base_url']			= $base_url;
		$station_id = (($this->session->userdata('current_station'))? $this->session->userdata('current_station') : ((array_key_exists("wish_user", $data))? ((array_key_exists("wish_prime_station", $data['wish_user']))? $data['wish_user']['wish_prime_station'] : 1) : 1));
		$data['station_id'] = $station_id;
		
		// Final catch-all for header info
		$data['custom_header']		= "<meta name=\"svg.render.forceflash\" content=\"true\">";
		$data['op']			= $thescope;
		// Operation
		$this->load->model('Election2012_model', '', TRUE);
		// Get module preferences and set as variable
		$like_where = array(
			array('where_value'=>'organization_id', 'where_equalto'=>$station_id, 'where_type'=>'and'),
			array('where_value'=>'preference_key', 'where_equalto'=>'preference', 'where_type'=>'like'),
			array('where_value'=>'preference_key', 'where_equalto'=>$data['module_id'], 'where_type'=>'like')
		);
		$module_preferences = $this->Election2012_model->getOrganizationPreference("preference_key, preference_value", NULL, $like_where, NULL, NULL, false);
		$mod_prefs = array();
		foreach($module_preferences as $preference) {
			$temp = array();
			$temp = $preference['preference_key'];
			$temp = explode("|", $temp);
			$preference['preference_key'] = $temp[2];
			$mod_prefs[$preference['preference_key']] = $preference['preference_value'];
		}
		$module_preferences = array();
		$module_preferences = $mod_prefs;
		$data["module_preferences"] = $module_preferences;
		switch($thescope)
		{
			case 'national':
			{
				// Set cache
				//$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
				//$this->output->cache(300);
				$data['national_svg'] = file_get_contents($base_url.'ci/_common/images/national_'.$size.'.svg');
				$data['national_svg_loc'] = $base_url.'ci/_common/images/national_'.$size.'.svg';
				break;
			}
			case 'pnational':
			{
				// Set cache
				//$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
				//$this->output->cache(300);
				$data['national_svg'] = file_get_contents($base_url.'ci/_common/images/national_'.$size.'.svg');
				$data['national_svg_loc'] = $base_url.'ci/_common/images/national_'.$size.'.svg';
				break;
			}
			case 'state':
			{
				$state = $this->Coreadmin_model->getStateNameByCode($thecode);
				$data['state_svg'] = file_get_contents($base_url.'ci/_common/images/'.strtolower($state).'_'.$size.'.svg');
				break;
			}
			case 'pstate':
			{
				$state = $this->Coreadmin_model->getStateNameByCode($thecode);
				$data['state_svg'] = file_get_contents($base_url.'ci/_common/images/'.strtolower($state).'_'.$size.'.svg');
				break;
			}
			default:
			{
				
			}
		}
		$page = "election2012";
		// Set template parts
		//$this->output->enable_profiler(TRUE);
		$this->load->view('templates/header_elections', $data);
		$this->load->view('election2012/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	public function ingestRaceData() //$op=NULL, $id=NULL
	{
		$data = $this->initialize();
		$this->load->model('Coreadmin_model', '', TRUE);
		$this->load->model('Election2012_model', '', TRUE);
		$this->load->library(array('Utilities'));
		$this->load->library(array('electiondatalib'));
		$org = (($_POST)? (($_POST['org'])?	$_POST['org'] : NULL) : NULL);
		$data['station_id'] = 0;
		$data['station_display'] = "";
		$station_id = $org;
		$station_where = array(array('where_value'=>'organization_id', 'where_equalto'=>$station_id, 'where_type'=>'and'));
		$station_data = $this->Coreadmin_model->getOrganization("organization_id, organization_name", NULL, $station_where, NULL, 1, false);
		$data['station_id'] = $station_id = ((array_key_exists(0, $station_data))? $station_data[0]['organization_id'] : 0);
		$data['station_display'] = ((array_key_exists(0, $station_data))? $station_data[0]['organization_name'] : 0);
		$source_id = $station_id;
		// Get module preferences and set as variable
		$like_where = array(
			array('where_value'=>'organization_id', 'where_equalto'=>$station_id, 'where_type'=>'and'),
			array('where_value'=>'preference_key', 'where_equalto'=>'preference', 'where_type'=>'like'),
			array('where_value'=>'preference_key', 'where_equalto'=>$data['module_id'], 'where_type'=>'like')
		);
		$module_preferences = $this->Election2012_model->getOrganizationPreference("preference_key, preference_value", NULL, $like_where, NULL, NULL, false);
		$mod_prefs = array();
		foreach($module_preferences as $preference) {
			$temp = array();
			$temp = $preference['preference_key'];
			$temp = explode("|", $temp);
			$preference['preference_key'] = $temp[2];
			$mod_prefs[$preference['preference_key']] = $preference['preference_value'];
		}
		$data["module_preferences"] = $module_preferences = $mod_prefs;
		$source_type = $module_preferences['election_feed_type'];
		$source_url = $module_preferences['election_xml'];
		$fip_field = $module_preferences['fip_field'];
		$races = "";
		$candidates = "";
		$response = array();
		$response['org'] = $org;
		$response['error'] = false;
		$response['message'] = array();
		$error = false;
		$theraces2d = array();
		$thecandidates2d = array();
		$beforenow = $starttime = time();
		$totalr = 0;
		$totalc = 0;
		switch($source_type)
		{
			case "leader":
			{
				$xml = $this->electiondatalib->getParsedFeed($this->electiondatalib->downloadXml($source_url));
				$race_num = $this->electiondatalib->countNodes($xml, 'race');
				for($r=0; $r<$race_num; $r++) {
					// Process race info
					$therace = $xml->race[$r]->race_info;
					$leader_race_id				= (int) $therace->race_id;
					$race_title_1				= (string) $therace->race_name1;
					$race_title_2				= (string) $therace->race_name2;
					$race_title_3				= (string) $therace->race_name3;
					$race_precincts_reporting	= (int) $therace->precincts_reporting;
					$race_precincts_percent		= (int) $therace->pct_precincts_reporting;
					$race_total_precincts		= (int) $therace->precincts_total;
					$race_last_leader_update	= strtotime($therace->last_updated);
					$race_total_votes			= (int) $therace->total_vote;
					$jurisdiction_id			= (string) $therace->jurisdiction_id;
					$race_other_1				= (string) $therace->other1;
					$race_other_2				= (string) $therace->other2;
					$race_other_3				= (string) $therace->other3;
					//proposition, yesno1, yesno2, yesno3, yesno4, other1, other2, other3
					// Ingest race info
					$race_source_join = array(
						array('jn_table'=>'election_race', 'jn_value'=>'election_race.race_id', 'jn_equalto'=>'election_race_source_map.race_id')
					);
					$race_data = $this->Election2012_model->getRaceDataSource("election_race.race_id", $race_source_join, array('election_race_source_map.source_id'=>$source_id, 'election_race_source_map.source_value'=>$leader_race_id), NULL, NULL, false);
					$race_id = ((array_key_exists(0, $race_data))? $race_data[0]['race_id'] : NULL);
					// If the race doesn't exist, create it
					if(!$race_id) {
						$race_insert_array = array(
							'race_status_id'	=> 2, 
							'race_title'		=> $race_title_1, 
							'race_title_short'	=> $race_title_2, 
							'race_name'			=> str_replace(" ", "_", strtolower($race_title_1." ".$race_title_2)),
							'race_updated'		=> time()
						);
						try{
							$race_insert_result = $this->Election2012_model->insertRace($race_insert_array);
						} catch(Exception $e) {
							$error = true;
							array_push($response['message'], "Race result save error: ".$e);
						}
						$race_id = mysql_insert_id();
						// Map the source ID with the internal race ID
						$map_insert_array = array(
							'race_id'		=> $race_id,
							'source_id'		=> $source_id,
							'source_value'	=> $leader_race_id
						);
						try{
							$map_insert_result = $this->Election2012_model->insertRaceDataSource($map_insert_array);
						} catch(Exception $e) {
							$error = true;
							array_push($response['message'], "Race data source mapping error: ".$e);
						}
					}
					array_push($theraces2d, $race_id);
					$race_result_insert_array = array(
						'source_id'							=> $source_id,
						'race_id'							=> $race_id,
						'county_fip'						=> ${$fip_field},
						'race_result_precincts_reporting'	=> $race_precincts_reporting,
						'race_result_precincts_percent'		=> $race_precincts_percent,
						'race_result_total'					=> $race_total_votes,
						'race_result_updated'				=> $race_last_leader_update
					);
					$race_result_insert_result = $this->Election2012_model->insertRaceResult($race_result_insert_array);
					$totalr++;
					$new_race_result_id = mysql_insert_id();
					// Process candidate info
					$thecandidates = $xml->race[$r]->candidate_info;
					$candidate_num = $this->electiondatalib->countNodes($thecandidates, 'candidate');
					$candidate_data = array();
					for($c=0; $c<$candidate_num; $c++) {
						$thecandidate = $thecandidates->candidate[$c];
						$leader_candidate_id		= (int) $thecandidate->candidate_id;
						$candidate_first			= (string) $thecandidate->first_name;
						$candidate_mi				= (string) $thecandidate->initial;
						$candidate_last				= (string) $thecandidate->last_name;
						$candidate_party			= $this->Election2012_model->getPartyByCode((string) $thecandidate->party_abbr);
						$candidate_incumbent		= (((string) $thecandidate->incumbent=='True')? 1 : 0);
						$candidate_winner			= (((string) $thecandidate->winner=='True')? 1 : 0);
						$candidate_projected_winner	= (((int) $thecandidate->projected_winner=='True')? 1 : 0);
						$candidate_cand_vote		= (int) str_replace(",", "", $thecandidate->cand_vote);
						$candidate_vote_percent		= (int) $thecandidate->vote_percent;
						//$cother1, $cother2, $cother3
						$candidate_source_join = array(
							array('jn_table'=>'election_candidate', 'jn_value'=>'election_candidate.candidate_id', 'jn_equalto'=>'election_candidate_source_map.candidate_id')
						);
						$candidate_data = $this->Election2012_model->getCandidateDataSource("election_candidate.candidate_id", $candidate_source_join, array('election_candidate_source_map.source_id'=>$source_id, 'election_candidate_source_map.source_value'=>$leader_race_id.$leader_candidate_id), NULL, NULL, false);
						$candidate_id = ((array_key_exists(0, $candidate_data))? $candidate_data[0]['candidate_id'] : NULL);
						// If the candidate doesn't exist, create it
						if(!$candidate_id) {
							$candidate_insert_array = array(
								'party_id'				=> $candidate_party,
								'candidate_first'		=> $candidate_first,
								'candidate_mi'			=> $candidate_mi,
								'candidate_last'		=> $candidate_last,
								'candidate_incumbent'	=> $candidate_incumbent,
								'candidate_winner'		=> $candidate_winner,
								'candidate_updated'		=> time()
							);
							$candidate_id = 0;
							try{
								$candidate_insert_result = $this->Election2012_model->insertCandidate($candidate_insert_array);
								$candidate_id = mysql_insert_id();
							} catch(Exception $e) {
								$error = true;
								array_push($response['message'], "Candidate save error: ".$e);
							}
							// Map the source ID with the internal race ID
							$cmap_insert_array = array(
								'candidate_id'	=> $candidate_id,
								'source_id'		=> $source_id,
								'source_value'	=> $leader_race_id.$leader_candidate_id
							);
							try{
								$cmap_insert_result = $this->Election2012_model->insertCandidateDataSource($cmap_insert_array);
							} catch(Exception $e) {
								$error = true;
								array_push($response['message'], "Candidate data source mapping error: ".$e);
							}
						}
						$check_race_candidate = $this->Election2012_model->getRaceCandidate(NULL, NULL, array('race_id'=>$race_id, 'candidate_id'=>$candidate_id), NULL, NULL, true);
						if(!$check_race_candidate) {
							$race_candidate_array = array(
								'race_id'		=> $race_id,
								'candidate_id'	=> $candidate_id
							);
							try{
								$race_candidate_result = $this->Election2012_model->insertRaceCandidate($race_candidate_array);
							} catch(Exception $e) {
								$error = true;
								array_push($response['message'], "Race/candidate mapping error: ".$e);
							}
						}
						array_push($thecandidates2d, $candidate_id);
						$candidate_result_insert_array = array(
							'source_id'					=> $source_id,
							'race_result_id'			=> $new_race_result_id,
							'candidate_id'				=> $candidate_id,
							'party_id'					=> $candidate_party,
							'candidate_result_value'	=> $candidate_cand_vote,
							'candidate_result_percent'	=> $candidate_vote_percent,
							'candidate_result_winner'	=> $candidate_winner,
							'candidate_result_updated'	=> time()
						);
						try{
							$candidate_result_insert_result = $this->Election2012_model->insertCandidateResult($candidate_result_insert_array);
						} catch(Exception $e) {
							$error = true;
							array_push($response['message'], "Candidate result save error: ".$e);
						}
						$totalc++;
					}
				}
				// Remove old race result records
				$delete_race_where_array = array(
					array('where_value'=>'race_result_updated <', 'where_equalto'=>$beforenow, 'where_type'=>'and'),
					array('where_value'=>'source_id', 'where_equalto'=>$source_id, 'where_type'=>'and')
				);
				$race_result_delete_result = $this->Election2012_model->deleteRaceResult($delete_race_where_array);
				// Remove old candidate result records
				$delete_candidate_where_array = array(
					array('where_value'=>'candidate_result_updated <', 'where_equalto'=>$beforenow, 'where_type'=>'and'),
					array('where_value'=>'source_id', 'where_equalto'=>$source_id, 'where_type'=>'and')
				);
				$candidate_result_delete_result = $this->Election2012_model->deleteCandidateResult($delete_candidate_where_array);
				//log_message('info', 'Ingestion complete.');
				$elapsed = date("i:s", time() - $starttime);
				array_push($response['message'], "Ingestion Complete for ".$data['station_display'].".");
				array_push($response['message'], "Processing time for ".$data['station_display']." - ".$elapsed.".");
				array_push($response['message'], $totalr." races processed for ".$data['station_display'].".");
				array_push($response['message'], $totalc." candidates processed for ".$data['station_display'].".");
				echo json_encode($response);
				exit;
			}
			case "newsticker":
			{
				$source_url = $module_preferences['election_xml'];
				$xml = $this->electiondatalib->getParsedFeed($this->electiondatalib->downloadXml($source_url));
				$race_num = $this->electiondatalib->countNodes($xml, 'RACE');
				for($r=0; $r<$race_num; $r++) {
					// Process race info
					$therace = $xml->RACE[$r]->RACE_INFO;
					$newsticker_race_id			= (int) $therace->RACE_ID;
					$race_title_1				= (string) $therace->RACE_NAME1;
					$race_title_2				= (string) $therace->RACE_NAME2;
					$race_title_3				= (string) $therace->RACE_NAME3;
					$race_title_4				= (string) $therace->RACE_NAME4;
					$race_prompt_1				= (string) $therace->PROMPT_LINES1;
					$race_prompt_2				= (string) $therace->PROMPT_LINES2;
					$race_precincts_reporting	= (int) $therace->PRECINCTS_REPORTING;
					$race_precincts_percent		= (int) $therace->PCT_PRECINCTS_REPORTING;
					$race_total_precincts		= (int) $therace->PRECINCTS_TOTAL;
					$race_last_newsticker_update= strtotime($therace->UPDATED);
					$race_total_votes			= (int) $therace->ALLCAND_VOTES;
					$jurisdiction_id			= (string) $therace->JURISDICTION_ID;
					//proposition, yesno1, yesno2, yesno3, yesno4, other1, other2, other3
					// Ingest race info
					$race_source_join = array(
						array('jn_table'=>'election_race', 'jn_value'=>'election_race.race_id', 'jn_equalto'=>'election_race_source_map.race_id')
					);
					$race_data = $this->Election2012_model->getRaceDataSource("election_race.race_id", $race_source_join, array('election_race_source_map.source_id'=>$source_id, 'election_race_source_map.source_value'=>$newsticker_race_id), NULL, NULL, false);
					$race_id = ((array_key_exists(0, $race_data))? $race_data[0]['race_id'] : NULL);
					// If the race doesn't exist, create it
					if(!$race_id) {
						$race_insert_array = array(
							'race_status_id'	=> 2, 
							'race_title'		=> $race_title_1.", ".$race_title_2, 
							'race_title_short'	=> $race_title_1, 
							'race_name'			=> str_replace(" ", "_", strtolower($race_title_1." ".$race_title_2)),
							'race_updated'		=> time()
						);
						try{
							$race_insert_result = $this->Election2012_model->insertRace($race_insert_array);
						} catch(Exception $e) {
							$error .= $e."\n";
						}
						$race_id = mysql_insert_id();
						// Map the source ID with the internal race ID
						$map_insert_array = array(
							'race_id'		=> $race_id,
							'source_id'		=> $source_id,
							'source_value'	=> $newsticker_race_id
						);
						try{
							$map_insert_result = $this->Election2012_model->insertRaceDataSource($map_insert_array);
						} catch(Exception $e) {
							$error = true;
							array_push($response['message'], "Race data source mapping error: ".$e);
						}
					}
					array_push($theraces2d, $race_id);
					$race_result_insert_array = array(
						'source_id'							=> $source_id,
						'race_id'							=> $race_id,
						'county_fip'						=> ${$fip_field},
						'race_result_precincts_reporting'	=> $race_precincts_reporting,
						'race_result_precincts_percent'		=> $race_precincts_percent,
						'race_result_total'					=> $race_total_votes,
						'race_result_updated'				=> time()
					);
					$race_result_insert_result = $this->Election2012_model->insertRaceResult($race_result_insert_array);
					$totalr++;
					$new_race_result_id = mysql_insert_id();
					// Process candidate info
					$thecandidates = $xml->RACE[$r]->CANDIDATE_INFO;
					$candidate_num = $this->electiondatalib->countNodes($thecandidates, 'CANDIDATE');
					$candidate_data = array();
					for($c=0; $c<$candidate_num; $c++) {
						$thecandidate = $thecandidates->CANDIDATE[$c];
						$pabbr = ((strstr((string) $thecandidate->FIRST_NAME, "-")!=false)? substr((string) $thecandidate->FIRST_NAME, 0, 1) : NULL);
						$newsticker_candidate_id	= (int) $thecandidate->CANDIDATE_ID;
						$candidate_display_order	= (int) $thecandidate->DISPLAY_ORDER;
						$candidate_visible			= (int) $thecandidate->VISIBLE;
						$candidate_first			= (string) ((strstr((string) $thecandidate->FIRST_NAME, "-")!=false)? substr((string) $thecandidate->FIRST_NAME, 2) : $thecandidate->FIRST_NAME);
						$candidate_last				= (string) $thecandidate->LAST_NAME;
						$candidate_party			= $this->Election2012_model->getPartyByCode((($pabbr)? $pabbr : (string) $thecandidate->PARTY_ABBR));
						$candidate_party2			= (string) $thecandidate->PARTY_NAME1;
						$candidate_party3			= (string) $thecandidate->PARTY_NAME2;
						$candidate_incumbent		= (((string) $thecandidate->INCUMBENT=='Y')? 1 : 0);
						$candidate_winner			= (((string) $thecandidate->WINNER=='Y' || (string) $thecandidate->WINNER=='Z')? 1 : 0);
						$candidate_cand_vote		= (int) $thecandidate->VOTE_TOTAL;
						$candidate_vote_percent		= (int) $thecandidate->PCT_TOTAL;
						//$cother1, $cother2, $cother3
						$candidate_source_join = array(
							array('jn_table'=>'election_candidate', 'jn_value'=>'election_candidate.candidate_id', 'jn_equalto'=>'election_candidate_source_map.candidate_id')
						);
						$candidate_id = $this->Election2012_model->getCandidateDataSource("election_candidate.candidate_id", $candidate_source_join, array('election_candidate_source_map.source_id'=>$source_id, 'election_candidate_source_map.source_value'=>$newsticker_race_id.$newsticker_candidate_id), NULL, NULL, false);
						$candidate_id = ((array_key_exists(0, $candidate_id))? $candidate_id[0]['candidate_id'] : NULL);
						// If the candidate doesn't exist, create it
						if(!$candidate_id) {
							$candidate_insert_array = array(
								'party_id'				=> $candidate_party,
								'candidate_first'		=> $candidate_first,
								'candidate_last'		=> $candidate_last,
								'candidate_incumbent'	=> $candidate_incumbent,
								'candidate_winner'		=> $candidate_winner,
								'candidate_updated'		=> time()
							);
							$candidate_id = 0;
							try{
								$candidate_insert_result = $this->Election2012_model->insertCandidate($candidate_insert_array);
								$candidate_id = mysql_insert_id();
							} catch(Exception $e) {
								$error .= $e."\n";
							}
							// Map the source ID with the internal race ID
							$cmap_insert_array = array(
								'candidate_id'	=> $candidate_id,
								'source_id'		=> $source_id,
								'source_value'	=> $newsticker_race_id.$newsticker_candidate_id
							);
							try{
								$cmap_insert_result = $this->Election2012_model->insertCandidateDataSource($cmap_insert_array);
							} catch(Exception $e) {
								$error = true;
								array_push($response['message'], "Candidate data source mapping error: ".$e);
							}
						}
						$check_race_candidate = $this->Election2012_model->getRaceCandidate(NULL, NULL, array('race_id'=>$race_id, 'candidate_id'=>$candidate_id), NULL, NULL, true);
						if(!$check_race_candidate) {
							$race_candidate_array = array(
								'race_id'		=> $race_id,
								'candidate_id'	=> $candidate_id
							);
							try{
								$race_candidate_result = $this->Election2012_model->insertRaceCandidate($race_candidate_array);
							} catch(Exception $e) {
								$error = true;
								array_push($response['message'], "Race/candidate mapping error: ".$e);
							}
						}
						array_push($thecandidates2d, $candidate_id);
						$candidate_result_insert_array = array(
							'source_id'					=> $source_id,
							'race_result_id'			=> $new_race_result_id,
							'candidate_id'				=> $candidate_id,
							'party_id'					=> $candidate_party,
							'candidate_result_value'	=> $candidate_cand_vote,
							'candidate_result_percent'	=> $candidate_vote_percent,
							'candidate_result_winner'	=> $candidate_winner,
							'candidate_result_updated'	=> time()
						);
						try{
							$candidate_result_insert_result = $this->Election2012_model->insertCandidateResult($candidate_result_insert_array);
						} catch(Exception $e) {
							$error = true;
							array_push($response['message'], "Candidate result save error: ".$e);
						}
						$totalc++;
					}
				}
				// Remove old race result records
				$delete_race_where_array = array(
					array('where_value'=>'race_result_updated <', 'where_equalto'=>$beforenow, 'where_type'=>'and'),
					array('where_value'=>'source_id', 'where_equalto'=>$source_id, 'where_type'=>'and')
				);
				$race_result_delete_result = $this->Election2012_model->deleteRaceResult($delete_race_where_array);
				// Remove old candidate result records
				$delete_candidate_where_array = array(
					array('where_value'=>'candidate_result_updated <', 'where_equalto'=>$beforenow, 'where_type'=>'and'),
					array('where_value'=>'source_id', 'where_equalto'=>$source_id, 'where_type'=>'and')
				);
				$candidate_result_delete_result = $this->Election2012_model->deleteCandidateResult($delete_candidate_where_array);
				//log_message('info', 'Ingestion complete.');
				$elapsed = date("i:s", time() - $starttime);
				array_push($response['message'], "Ingestion Complete for ".$data['station_display'].".");
				array_push($response['message'], "Processing time for ".$data['station_display']." - ".$elapsed.".");
				array_push($response['message'], $totalr." races processed for ".$data['station_display'].".");
				array_push($response['message'], $totalc." candidates processed for ".$data['station_display'].".");
				echo json_encode($response);
				exit;
			}
			default:
			{
				echo "<pre>";
				print_r($source_url);
				echo "</pre>";
				exit;
			}
		}
		return false;
	}

	public function exportRaceData($orgid=NULL, $exformat='') 
	{
		// Get variables
		log_message('info', 'Export started. Organization: '.$orgid.', Format: '.$exformat);
		$data = $this->initialize();
		$this->load->library(array('Utilities'));
		$data['op'] = $op = (($op)? $op : '');
		//$data['id'] = $id = (($id)? $id : '');
		// Load custom configs
		$this->load->model('Coreadmin_model', '', TRUE);
		$module_id = $this->Coreadmin_model->getModule("module_id", NULL, array('module_dirname'=>$this->uri->segment(1)), NULL, 1, false);
		$module_id = $module_id[0]['module_id'];
		$configurations = $this->Coreadmin_model->getConfiguration(NULL, NULL, array('module_id'=>$module_id));
		foreach($configurations as $configuration) {
			$this->config->set_item($configuration['configuration_name'], $configuration['configuration_value']);
		}
		$this->load->model('Election2012_model', '', TRUE);
		$response = "";
		try{
			// Get preferences
			$preferences_list = $this->fetchOrganizationPreference($organization, 'race', 'featured');
			// Begin fetching data
			$race_join = array(
				array('jn_table'=>'election_race_result', 'jn_value'=>'election_race_result.race_id', 'jn_equalto'=>'election_race.race_id'),
				array('jn_table'=>'election_race_source_map', 'jn_value'=>'election_race_source_map.race_id', 'jn_equalto'=>'election_race.race_id')
			);
			$fields = "election_race.race_id, election_race.race_status_id, election_race.race_title, election_race.race_title_short, election_race.race_name, election_race.race_updated, election_race_result.race_result_id, election_race_result.county_fip, election_race_result.race_result_precincts_reporting, election_race_result.race_result_precincts_percent, election_race_result.race_result_total, election_race_result.race_result_total_percent, election_race_source_map.source_value";
			$total_records = $this->Election2012_model->getRace($fields, $race_join, NULL, NULL, NULL, true);
			$races = $this->Election2012_model->getRace($fields, $race_join, NULL, NULL, NULL, false);
			$race_data = array();
			foreach($races as $race) {
				$race['race_featured'] = ((in_array($race['race_id'], $preferences_list))? 1 : 0);
				$statuses = $this->Election2012_model->getRaceStatus(NULL, NULL, array('race_status_id'=>$race['race_status_id']), NULL, 1, false);
				foreach($statuses as $status) {
					$race['race_status_name'] = $status['race_status_name'];
				}
				$fips = $this->Election2012_model->getRaceCounty(NULL, NULL, array('race_id'=>$race['race_id']), NULL, 1, false);
				foreach($fips as $fip) {
					$race['county_fip'] = $fip['county_fip'];
				}
				$candidate_join = array(
					array('jn_table'=>'election_candidate', 'jn_value'=>'election_candidate.candidate_id', 'jn_equalto'=>'election_race_candidate_jn.candidate_id'),
					array('jn_table'=>'election_party', 'jn_value'=>'election_party.party_id', 'jn_equalto'=>'election_candidate.party_id'),
					array('jn_table'=>'election_candidate_result', 'jn_value'=>'election_candidate_result.candidate_id', 'jn_equalto'=>'election_candidate.candidate_id')
				);
				$candidate_where = array(
					array('where_value'=>'election_race_candidate_jn.race_id', 'where_equalto'=>$race['race_id'], 'where_type'=>'and'),
					array('where_value'=>'election_candidate_result.race_result_id', 'where_equalto'=>$race['race_result_id'], 'where_type'=>'and')
				);
				$candidate_fields = "election_candidate.candidate_id, election_candidate.party_id, election_candidate.race_id, election_candidate.candidate_first, election_candidate.candidate_mi, election_candidate.candidate_last, election_candidate.candidate_incumbent, election_candidate.candidate_winner, election_party.party_code, election_candidate_result.candidate_result_value";
				$candidates = $this->Election2012_model->getRaceCandidate($candidate_fields, $candidate_join, $candidate_where, NULL, NULL, false);
				foreach($candidates as $candidate) {
					$race['race_candidates']['candidate-'.$candidate['candidate_id']] = array(
						'candidate_id'				=> $candidate['candidate_id'],
						'candidate_first'			=> $candidate['candidate_first'],
						'candidate_mi'				=> $candidate['candidate_mi'],
						'candidate_last'			=> $candidate['candidate_last'],
						'candidate_party_code'		=> $candidate['party_code'],
						'candidate_incumbent'		=> $candidate['candidate_incumbent'],
						'candidate_winner'			=> $candidate['candidate_winner'],
						'candidate_result_value'	=> $candidate['candidate_result_value']
					);
				}
				array_push($race_data, $race);
			}
			$race_data = $this->utilities->multiColSort($race_data, 'race_title', SORT_ASC);
			$races = array();
			$races = $race_data;
			$data['races'] = $races;
		} catch (Exception $e) {
			return $e;
		}
		$page = "election2012_test";
		$race_info = 
			"<?xml version='1.0' encoding='ISO-8859-1'?>\n".
			"<data>\n";
		foreach($races as $race){
			$race_info .=
				"	<race>\n".
				"		<race_info>\n".
				"			<race_id>".$race['race_id']."</race_id>\n".
				"			<race_name1>".htmlspecialchars($race['race_title_short'])."</race_name1>\n".
				"			<race_name2>".htmlspecialchars($race['race_title'])."</race_name2>\n".
				"			<race_name3></race_name3>\n".
				"			<precincts_reporting>".number_format($race['race_result_precincts_reporting'])."</precincts_reporting>\n".
				"			<pct_precincts_reporting>".number_format($race['race_result_precincts_percent'])."</pct_precincts_reporting>\n".
				"			<precincts_total>".number_format($race['race_result_total'])."</precincts_total>\n".
				"			<last_updated>".str_replace("PM", "p.m.", str_replace("AM", "a.m.", date("M d, Y g:i A", time())))."</last_updated>\n". //Nov 03, 2010  11:30 AM
				"			<total_vote/>\n".
				"			<other1/>\n".
				"			<other2/>\n".
				"			<other3/>\n".
				"			<jurisdiction_id>".$race['county_fip']."</jurisdiction_id>\n".
				"		</race_info>\n".
				"		<candidate_info>\n";
			$candidates = ((array_key_exists("race_candidates", $race))? $race['race_candidates'] : array());
			foreach($candidates as $candidate){
				$race_info .=
					"			<candidate>\n".
					"				<candidate_id>".$candidate['candidate_id']."</candidate_id>\n".
					"				<first_name>".htmlspecialchars($candidate['candidate_first'])."</first_name>\n".
					"				<initial>".htmlspecialchars($candidate['candidate_mi'])."</initial>\n".
					"				<last_name>".htmlspecialchars($candidate['candidate_last'])."</last_name>\n".
					"				<incumbent>".$candidate['candidate_incumbent']."</incumbent>\n".
					"				<party_abbr>".$candidate['candidate_party_code']."</party_abbr>\n".
					"				<cand_vote>".number_format($candidate['candidate_result_value'])."</cand_vote>\n".
					"				<vote_percent></vote_percent>\n".
					"				<winner>".$candidate['candidate_winner']."</winner>\n".
					"				<projected_winner></projected_winner>\n".
					"				<cother1></cother1>\n".
					"				<cother2></cother2>\n".
					"				<cother3></cother3>\n".
					"			</candidate>\n";
			}
			$race_info .=
				"		</candidate_info>\n".
				"	</race>\n";
		}
		$race_info .= "</data>\n";
		switch(strtolower($format)){
			case 'xml': {
				$ftp_address = $this->config->item('ftp_address');
				$ftp_user = $this->config->item('ftp_username');
				$ftp_pass = $this->config->item('ftp_password');
				$ftp_path = $this->config->item('ftp_path');
				$ftp_conn = 'ftp://'.$ftp_user.':'.$ftp_pass.'@'.$ftp_address.$ftp_path.'election_data_xml.xml';
				$stream_options = array('ftp' => array('overwrite' => true));
				$stream_context = stream_context_create($stream_options);
				$stream_context = stream_context_create($stream_options);
				if(! $f = fopen($ftp_conn,'w',0,$stream_context)) {
					$response = 'Could not open XML file for writing.';
				} else {
					if(! fwrite($f,$race_info,strlen($race_info))){
						$response = 'Could not write to XML file.';
					} else {
						$response = 'XML File written!';
					}
				}
				fclose($f);
				echo json_encode($response);
				exit;
			}
			case 'json': {
				$race_info = json_encode((string) $race_info);
				$ftp_address = $this->config->item('ftp_address');
				$ftp_user = $this->config->item('ftp_username');
				$ftp_pass = $this->config->item('ftp_password');
				$ftp_path = $this->config->item('ftp_path');
				$ftp_conn = 'ftp://'.$ftp_user.':'.$ftp_pass.'@'.$ftp_address.$ftp_path.'election_data_json.txt';
				$stream_options = array('ftp' => array('overwrite' => true));
				$stream_context = stream_context_create($stream_options);
				if(! $f = fopen($ftp_conn,'w',0,$stream_context)) {
					$response = 'Could not open file.';
				} else {
					if(! fwrite($f,$race_info,strlen($race_info))){
						$response = 'Could not write JSON data to file.';
					} else {
						$response = 'JSON File written!';
					}
				}
				fclose($f);
				echo json_encode($response);
				exit;
			}
			default: {
				$this->output->set_content_type('text/xml');
				$this->load->view('election2012/'.$page, $data);
			}
		}
		log_message('info', 'Export completed.');
		echo json_encode($response);
	}

	public function exportSpecifiedData() //$org=NULL, $form='', $state=NULL, $type=NULL
	{
		$data = $this->initialize();
		$this->load->model('Coreadmin_model', '', TRUE);
		$this->load->model('Election2012_model', '', TRUE);
		$this->load->library(array('Utilities'));
		$this->load->library(array('electiondatalib'));
		// Get variables
		$theorganization	= $org		= (($_POST)? (($_POST['org'])? $_POST['org'] : $org) : $org);
		$theformat			= $format	= (($_POST)? (($_POST['format'])? $_POST['format'] : $form) : $form);
		$thestate			= (($_POST)? ((array_key_exists("state", $_POST))? $_POST['state'] : NULL) : NULL);
		$thetype			= (($_POST)? ((array_key_exists("type", $_POST))? $_POST['type'] : NULL) : NULL);
		// Load custom configs
		$data['station_id'] = 0;
		$data['station_display'] = "";
		$station_id = $org;
		/*$station_where = array(array('where_value'=>'organization_id', 'where_equalto'=>$station_id, 'where_type'=>'and'));
		$station_data = $this->Coreadmin_model->getOrganization("organization_id, organization_name", NULL, $station_where, NULL, 1, false);
		$data['station_id'] = $station_id = ((array_key_exists(0, $station_data))? $station_data[0]['organization_id'] : 0);
		$data['station_display'] = ((array_key_exists(0, $station_data))? $station_data[0]['organization_name'] : 0);*/
		if(!is_numeric($station_id)) {
			$station_where = array(array('where_value'=>'organization_name', 'where_equalto'=>$station_id, 'where_type'=>'like'));
			$station_data = $this->Coreadmin_model->getOrganization("organization_id, organization_name", NULL, $station_where, NULL, 1, false);
			$data['station_id'] = $station_id = ((array_key_exists(0, $station_data))? $station_data[0]['organization_id'] : 0);
			$data['station_display'] = $station_display  = ((array_key_exists(0, $station_data))? $station_data[0]['organization_name'] : 0);
		} else {
			$station_where = array(array('where_value'=>'organization_id', 'where_equalto'=>$station_id, 'where_type'=>'and'));
			$station_data = $this->Coreadmin_model->getOrganization("organization_id, organization_name", NULL, $station_where, NULL, 1, false);
			$data['station_id'] = $station_id = ((array_key_exists(0, $station_data))? $station_data[0]['organization_id'] : 0);
			$data['station_display'] = $station_display = ((array_key_exists(0, $station_data))? $station_data[0]['organization_name'] : 0);
		}
		$source_id = $station_id;
		// Get module preferences and set as variable
		$like_where = array(
			array('where_value'=>'organization_id', 'where_equalto'=>$station_id, 'where_type'=>'and'),
			array('where_value'=>'preference_key', 'where_equalto'=>'preference', 'where_type'=>'like'),
			array('where_value'=>'preference_key', 'where_equalto'=>$data['module_id'], 'where_type'=>'like')
		);
		$module_preferences = $this->Election2012_model->getOrganizationPreference("preference_key, preference_value", NULL, $like_where, NULL, NULL, false);
		$mod_prefs = array();
		foreach($module_preferences as $preference) {
			$temp = array();
			$temp = $preference['preference_key'];
			$temp = explode("|", $temp);
			$preference['preference_key'] = $temp[2];
			$mod_prefs[$preference['preference_key']] = $preference['preference_value'];
		}
		$data["module_preferences"] = $module_preferences = $mod_prefs;
		$response = array();
		$response['org'] = $org;
		$response['error'] = false;
		$response['message'] = array();
		try{
			if($races = $this->getRaceIDsByType($theorganization, $thestate, $thetype)){
				$race_join = array(
					array('jn_table'=>'election_race_result', 'jn_value'=>'election_race_result.race_id', 'jn_equalto'=>'election_race.race_id'),
					array('jn_table'=>'election_race_source_map', 'jn_value'=>'election_race_source_map.race_id', 'jn_equalto'=>'election_race.race_id')
				);
				$race_where = array(
					array('where_value'=>'election_race_result.race_id', 'where_equalto'=>$races, 'where_type'=>'in')
				);
				$fields = "election_race.race_id, election_race.race_status_id, election_race.race_title, election_race.race_title_short, election_race.race_name, election_race.race_updated, election_race_result.race_result_id, election_race_result.county_fip, election_race_result.race_result_precincts_reporting, election_race_result.race_result_precincts_percent, election_race_result.race_result_total, election_race_result.race_result_total_percent, election_race_source_map.source_value";
				$total_records = $this->Election2012_model->getRace($fields, $race_join, $race_where, NULL, NULL, true);
				$races = $this->Election2012_model->getRace($fields, $race_join, $race_where, NULL, NULL, false);
				$race_data = array();
				foreach($races as $race) {
					$race['county_name'] = $this->Election2012_model->getSourceLabelByFIPS($race['county_fip']);
					$statuses = $this->Election2012_model->getRaceStatus(NULL, NULL, array('race_status_id'=>$race['race_status_id']), NULL, 1, false);
					foreach($statuses as $status) {
						$race['race_status_name'] = $status['race_status_name'];
					}
					$candidate_join = array(
						array('jn_table'=>'election_candidate', 'jn_value'=>'election_candidate.candidate_id', 'jn_equalto'=>'election_race_candidate_jn.candidate_id'),
						array('jn_table'=>'election_candidate_source_map', 'jn_value'=>'election_candidate_source_map.candidate_id', 'jn_equalto'=>'election_race_candidate_jn.candidate_id'),
						array('jn_table'=>'election_party', 'jn_value'=>'election_party.party_id', 'jn_equalto'=>'election_candidate.party_id'),
						array('jn_table'=>'election_candidate_result', 'jn_value'=>'election_candidate_result.candidate_id', 'jn_equalto'=>'election_candidate.candidate_id')
					);
					$candidate_where = array(
						array('where_value'=>'election_race_candidate_jn.race_id', 'where_equalto'=>$race['race_id'], 'where_type'=>'and'),
						array('where_value'=>'election_candidate_result.race_result_id', 'where_equalto'=>$race['race_result_id'], 'where_type'=>'and')
					);
					$candidate_fields = "election_candidate.candidate_id, election_candidate.party_id, election_candidate.race_id, election_candidate.candidate_first, election_candidate.candidate_mi, election_candidate.candidate_last, election_candidate.candidate_incumbent, election_candidate.candidate_winner, election_party.party_code, election_candidate_result.candidate_result_value, election_candidate_result.candidate_result_percent, election_candidate_result.candidate_result_winner, election_candidate_source_map.source_value";
					$candidates = $this->Election2012_model->getRaceCandidate($candidate_fields, $candidate_join, $candidate_where, NULL, NULL, false);
					foreach($candidates as $candidate) {
						$race['race_candidates']['candidate-'.$candidate['candidate_id']] = array(
							'candidate_id'				=> $candidate['candidate_id'],
							'source_value'				=> $candidate['source_value'],
							'candidate_first'			=> $candidate['candidate_first'],
							'candidate_mi'				=> $candidate['candidate_mi'],
							'candidate_last'			=> $candidate['candidate_last'],
							'candidate_party_code'		=> $candidate['party_code'],
							'candidate_incumbent'		=> $candidate['candidate_incumbent'],
							'candidate_winner'			=> $candidate['candidate_result_winner'],
							'candidate_result_value'	=> $candidate['candidate_result_value'],
							'candidate_result_percent'	=> $candidate['candidate_result_percent']
						);
					}
					array_push($race_data, $race);
				}
				$race_data = $this->utilities->multiColSort($race_data, 'source_value', SORT_ASC, 'county_fip', SORT_ASC, 'race_id', SORT_ASC);
				$races = $race_data;
				//$response = $race_data;
				//$data['races'] = $races;
			} else {
				$races = array();
			}
		} catch (Exception $e) {
			return $e;
		}
		$page = "election2012_test";
		switch(strtolower($theformat)){
			case 'xml': {
				$race_info = 
					"<?xml version='1.0' encoding='ISO-8859-1'?>\n".
					"<data>\n";
				foreach($races as $race){
					$race_info .=
						"	<race>\n".
						"		<race_info>\n".
						"			<race_id>".$race['source_value']."</race_id>\n".
						"			<race_name1><![CDATA[".htmlspecialchars($race['race_title'])."]]></race_name1>\n".
						"			<race_name2><![CDATA[".htmlspecialchars($race['race_title_short'])."]]></race_name2>\n".
						"			<race_name3></race_name3>\n".
						"			<precincts_reporting>".number_format($race['race_result_precincts_reporting'])."</precincts_reporting>\n".
						"			<pct_precincts_reporting>".number_format($race['race_result_precincts_percent'])."</pct_precincts_reporting>\n".
						"			<precincts_total>".number_format($race['race_result_total'])."</precincts_total>\n".
						"			<last_updated>".str_replace("PM", "p.m.", str_replace("AM", "a.m.", date("M d, Y g:i A", time())))."</last_updated>\n". //Nov 03, 2010  11:30 AM
						"			<timestamp>".time()."</timestamp>\n". 
						"			<total_vote/>\n".
						"			<other1/>\n".
						"			<other2/>\n".
						"			<other3>".$race['county_name']."</other3>\n".
						"			<jurisdiction_id>".$race['county_fip']."</jurisdiction_id>\n".
						"		</race_info>\n".
						"		<candidate_info>\n";
					$candidates = ((array_key_exists("race_candidates", $race))? $race['race_candidates'] : array());
					foreach($candidates as $candidate){
						$race_info .=
							"			<candidate>\n".
							"				<candidate_id>".$candidate['source_value']."</candidate_id>\n".
							"				<first_name><![CDATA[".htmlspecialchars($candidate['candidate_first'])."]]></first_name>\n".
							"				<initial><![CDATA[".htmlspecialchars($candidate['candidate_mi'])."]]></initial>\n".
							"				<last_name><![CDATA[".htmlspecialchars($candidate['candidate_last'])."]]></last_name>\n".
							"				<incumbent>".$candidate['candidate_incumbent']."</incumbent>\n".
							"				<party_abbr>".$candidate['candidate_party_code']."</party_abbr>\n".
							"				<cand_vote>".number_format($candidate['candidate_result_value'])."</cand_vote>\n".
							"				<vote_percent>".number_format($candidate['candidate_result_percent'])."</vote_percent>\n".
							"				<winner>".$candidate['candidate_winner']."</winner>\n".
							"				<projected_winner></projected_winner>\n".
							"				<cother1></cother1>\n".
							"				<cother2></cother2>\n".
							"				<cother3></cother3>\n".
							"			</candidate>\n";
					}
					$race_info .=
						"		</candidate_info>\n".
						"	</race>\n";
				}
				$race_info .= "</data>\n";
				$thefile = strtolower(str_replace("-TV", "", $station_display)).'.xml';
				$thelocalfile = (string) FCPATH.'ci/_common/xml/'.$thefile;
				if (!write_file($thelocalfile, $race_info)) {
					$response['error'] = true;
					array_push($response['message'], "Unable to write the file");
				} else {
					array_push($response['message'], "File ".$thefile." written. ");
					$ftp_address = $this->config->item('ftp_address');
					$ftp_user = $this->config->item('ftp_username');
					$ftp_pass = $this->config->item('ftp_password');
					$ftp_path = $this->config->item('ftp_path');
					$ftp_port = $this->config->item('ftp_port');
					$ftp_conn = ftp_connect($ftp_address, $ftp_port) or array_push($response['message'], "Could not connect to host.");
					ftp_login($ftp_conn, $ftp_user, $ftp_pass) or array_push($response['message'], "Could not log in.");
					//ftp_chdir($ftp_conn, $ftp_path);
					if(ftp_put($ftp_conn, $ftp_path.$thefile, $thelocalfile, FTP_BINARY)==true) {
						array_push($response['message'], $thefile." uploaded to ".$this->config->item('ftp_address').$this->config->item('ftp_path').".");
					} else {
						$response['error'] = true;
						array_push($response['message'], "Unable to upload ".$thefile.".");
					}
					ftp_close($ftp_conn);
				}
				//log_message('info', 'Export completed.');
				echo json_encode($response);
				exit;
			}
			case 'json': {
				$race_info = array();
				$i=0;
				foreach($races as $race){
					$race_info[$i]['race_id'] = $race['source_value'];
					$race_info[$i]['race_name1'] = htmlspecialchars($race['race_title']);
					$race_info[$i]['race_name2'] = htmlspecialchars($race['race_title_short']);
					$race_info[$i]['race_name3'] = "";
					$race_info[$i]['precincts_reporting'] = number_format($race['race_result_precincts_reporting']);
					$race_info[$i]['pct_precincts_reporting'] = number_format($race['race_result_precincts_percent']);
					$race_info[$i]['precincts_total'] = number_format($race['race_result_total']);
					$race_info[$i]['last_updated'] = str_replace("PM", "p.m.", str_replace("AM", "a.m.", date("M d, Y g:i A", $race['race_updated']))); //Nov 03, 2010  11:30 AM
					$race_info[$i]['timestamp'] = $race['race_updated'];
					$race_info[$i]['total_vote'] = "";
					$race_info[$i]['other1'] = "";
					$race_info[$i]['other2'] = "";
					$race_info[$i]['other3'] = "";
					$c=0;
					$candidates = ((array_key_exists("race_candidates", $race))? $race['race_candidates'] : array());
					foreach($candidates as $candidate){
						$race_info[$i]['candidate_info'][$c]['candidate_id'] = $candidate['source_value'];
						$race_info[$i]['candidate_info'][$c]['first_name'] = htmlspecialchars($candidate['candidate_first']);
						$race_info[$i]['candidate_info'][$c]['initial'] = htmlspecialchars($candidate['candidate_mi']);
						$race_info[$i]['candidate_info'][$c]['last_name'] = htmlspecialchars($candidate['candidate_last']);
						$race_info[$i]['candidate_info'][$c]['incumbent'] = $candidate['candidate_incumbent'];
						$race_info[$i]['candidate_info'][$c]['party_abbr'] = $candidate['candidate_party_code'];
						$race_info[$i]['candidate_info'][$c]['cand_vote'] = number_format($candidate['candidate_result_value']);
						$race_info[$i]['candidate_info'][$c]['vote_percent'] = "";
						$race_info[$i]['candidate_info'][$c]['winner'] = $candidate['candidate_winner'];
						$race_info[$i]['candidate_info'][$c]['projected_winner'] = "";
						$race_info[$i]['candidate_info'][$c]['cother1'] = "";
						$race_info[$i]['candidate_info'][$c]['cother2'] = "";
						$race_info[$i]['candidate_info'][$c]['cother3'] = "";
						$c++;
					}
					$i++;
				}
				$race_info = json_encode($race_info);
				$thefile = strtolower(str_replace("-TV", "", $station_display)).'.txt';
				$thelocalfile = (string) FCPATH.'ci/_common/xml/'.$thefile;
				if (!write_file($thelocalfile, $race_info)) {
					$response['error'] = true;
					array_push($response['message'], "Unable to write the file.");
				} else {
					$response = 'File '.$thefile.' written. ';
					$ftp_address = $this->config->item('ftp_address');
					$ftp_user = $this->config->item('ftp_username');
					$ftp_pass = $this->config->item('ftp_password');
					$ftp_path = $this->config->item('ftp_path');
					$ftp_port = $this->config->item('ftp_port');
					$ftp_conn = ftp_connect($ftp_address, $ftp_port) or array_push($response['message'], "Could not connect to host.");
					ftp_login($ftp_conn, $ftp_user, $ftp_pass) or array_push($response['message'], "Could not log in.");
					//ftp_chdir($ftp_conn, $ftp_path);
					if(ftp_put($ftp_conn, $ftp_path.$thefile, $thelocalfile, FTP_BINARY)==true) {
						array_push($response['message'], $thefile." uploaded to ".$this->config->item('ftp_address').$this->config->item('ftp_path').".");
					} else {
						$response['error'] = true;
						array_push($response['message'], "Unable to upload ".$thefile.".");
					}
					ftp_close($ftp_conn);
				}
				log_message('info', 'Export completed.');
				echo json_encode($response);
				exit;
			}
			default: {
				$this->output->set_content_type('text/xml');
				$this->load->view('election2012/'.$page, $data);
			}
		}
		log_message('error', 'Unable to determine format.');
		echo json_encode($response);
	}

	public function transferAPData($file) 
	{
		log_message('info', 'Transfer started.');
		$data = $this->initialize();
		$this->load->library(array('Utilities'));
		$data['op'] = $op = (($op)? $op : '');
		$data['id'] = $id = (($id)? $id : '');
		$thefile = (($_POST)? (($_POST['file'])? $_POST['file'] : $file) : $file);
		// Load custom configs
		$this->load->model('Coreadmin_model', '', TRUE);
		$module_id = $this->Coreadmin_model->getModule("module_id", NULL, array('module_dirname'=>$this->uri->segment(1)), NULL, 1, false);
		$module_id = $module_id[0]['module_id'];
		$configurations = $this->Coreadmin_model->getConfiguration(NULL, NULL, array('module_id'=>$module_id));
		foreach($configurations as $configuration) {
			$this->config->set_item($configuration['configuration_name'], $configuration['configuration_value']);
		}
		$this->load->model('Election2012_model', '', TRUE);
		$response = "";
		$race_data = array();
		try{
			$xml = $this->electiondatalib->downloadXml("ftp://wishtveln:wishtveln@electionsonline.ap.org/Pres_Reports/xml/".$thefile.".xml");
			$thefile = $thefile.'.xml';
			$thelocalfile = (string) FCPATH.'ci/_common/xml/'.$thefile;
			if (!write_file($thelocalfile, $xml)) {
				$response = 'Unable to write the file';
			} else {
				$response = 'File '.$thefile.'.xml written. ';
				$ftp_address = $this->config->item('ftp_address');
				$ftp_user = $this->config->item('ftp_username');
				$ftp_pass = $this->config->item('ftp_password');
				$ftp_path = $this->config->item('ftp_path');
				$ftp_port = $this->config->item('ftp_port');
				$ftp_conn = ftp_connect($ftp_address, $ftp_port) or $response .= 'Could not connect to host. ';
				ftp_login($ftp_conn, $ftp_user, $ftp_pass) or $response .= 'Could not log in. ';
				//ftp_chdir($ftp_conn, $ftp_path);
				if(!ftp_put($ftp_conn, $ftp_path.$thefile, $thelocalfile, FTP_ASCII)) {
					$response .= 'Unable to upload '.$thefile.'. ';
				} else {
					$response .= $thefile.' uploaded to the server. ';
				}
				ftp_close($ftp_conn);
			}
		} catch (Exception $e) {
			return $e;
		}
		echo json_encode($response);
	}

	public function fetchPartyData()
	{
		try{
			// Load libraries and model for data retrieval
			$this->load->library(array('Utilities'));
			$this->load->model('Election2012_model', '', TRUE);
			// Get variables
			$id				= (($_POST)? (($_POST['id'])? $_POST['id'] : NULL) : NULL);
			$name			= (($_POST)? (($_POST['name'])? $_POST['name'] : NULL) : NULL);
			$short			= (($_POST)? (($_POST['short'])? $_POST['short'] : NULL) : NULL);
			$icon			= (($_POST)? (($_POST['icon'])? $_POST['icon'] : NULL) : NULL);
			$org			= (($_POST)? (($_POST['org'])? $_POST['org'] : NULL) : NULL);
			$day_format		= (($_POST)? (($_POST['dformat'])? $_POST['dayformat'] : 'l, F j') : 'l, F j');
			$time_format	= (($_POST)? (($_POST['tformat'])? $_POST['timeformat'] : 'g:i a') : 'g:i a');
			// Get preferences
			$preferences_list = $this->fetchOrganizationPreference($org, 'party', 'featured');
			// Begin fetching data
			$fields = "party_id, party_name, party_name_short, party_icon";
			$where_array = array();
			((!is_null($id))?		array_push($where_array, array('where_value'=>'party_id', 'where_equalto'=>$id, 'where_type'=>'and')) : NULL);
			((!is_null($name))?		array_push($where_array, array('where_value'=>'party_name', 'where_equalto'=>$name, 'where_type'=>'like')) : NULL);
			((!is_null($short))?	array_push($where_array, array('where_value'=>'party_name_short', 'where_equalto'=>$short, 'where_type'=>'like')) : NULL);
			((!is_null($icon))?		array_push($where_array, array('where_value'=>'party_icon', 'where_equalto'=>$icon, 'where_type'=>'like')) : NULL);
			$parties_records = $this->Election2012_model->getParty($fields, NULL, $where_array, NULL, NULL, true);
			$parties = $this->Election2012_model->getParty($fields, NULL, $where_array, NULL, NULL, false);
			$result = array();
			foreach($parties as $party) {
				$party['party_featured'] = ((in_array($party['party_id'], $preferences_list))? 1 : 0);
				array_push($result, $party);
			}
			$result = $this->utilities->multiColSort($result, 'party_name', SORT_DESC);
			echo json_encode($result);
		} catch (Exception $e) {
			return $e;
		}
	}

	public function fetchCandidateData()
	{
		try{
			// Load libraries and model for data retrieval
			$this->load->library(array('Utilities'));
			$this->load->model('Election2012_model', '', TRUE);
			// Get variables
			$id				= (($_POST)? (($_POST['id'])? $_POST['id'] : NULL) : NULL);
			$rid			= (($_POST)? (($_POST['rid'])? $_POST['rid'] : NULL) : NULL);
			$pid			= (($_POST)? (($_POST['pid'])? $_POST['pid'] : NULL) : NULL);
			$pname			= (($_POST)? (($_POST['pname'])? $_POST['pname'] : NULL) : NULL);
			$first			= (($_POST)? (($_POST['first'])? $_POST['first'] : NULL) : NULL);
			$mi				= (($_POST)? (($_POST['mi'])? $_POST['mi'] : NULL) : NULL);
			$last			= (($_POST)? (($_POST['last'])? $_POST['last'] : NULL) : NULL);
			$incumbent		= (($_POST)? (($_POST['incumbent'])? $_POST['incumbent'] : NULL) : NULL);
			$winner			= (($_POST)? (($_POST['winner'])? $_POST['winner'] : NULL) : NULL);
			$rname			= (($_POST)? (($_POST['rname'])? $_POST['rname'] : NULL) : NULL);
			$org			= (($_POST)? (($_POST['org'])? $_POST['org'] : NULL) : NULL);
			$day_format		= (($_POST)? (($_POST['dformat'])? $_POST['dayformat'] : 'l, F j') : 'l, F j');
			$time_format	= (($_POST)? (($_POST['tformat'])? $_POST['timeformat'] : 'g:i a') : 'g:i a');
			// Get preferences
			$preferences_list = $this->fetchOrganizationPreference($org, 'party', 'featured');
			// Begin fetching data
			$fields = "election_candidate.candidate_id,election_candidate.race_id,election_candidate.party_id,election_party.party_name_short,election_candidate.candidate_first,election_candidate.candidate_mi,election_candidate.candidate_last,election_candidate.candidate_incumbent,election_candidate.candidate_winner,election_race_candidate_jn.race_candidate_id,election_race.race_title_short";
			$join_array = array(
				array('jn_table'=>'election_party', 'jn_value'=>'election_party.party_id', 'jn_equalto'=>'election_candidate.party_id'),
				array('jn_table'=>'election_race_candidate_jn', 'jn_value'=>'election_race_candidate_jn.candidate_id', 'jn_equalto'=>'election_candidate.candidate_id'),
				array('jn_table'=>'election_race', 'jn_value'=>'election_race.race_id', 'jn_equalto'=>'election_race_candidate_jn.race_id')
			);
			$where_array = array();
			((!is_null($id))?			array_push($where_array, array('where_value'=>'candidate_id', 'where_equalto'=>$id, 'where_type'=>'and')) : NULL);
			((!is_null($rid))?			array_push($where_array, array('where_value'=>'race_id', 'where_equalto'=>$rid, 'where_type'=>'and')) : NULL);
			((!is_null($pid))?			array_push($where_array, array('where_value'=>'party_id', 'where_equalto'=>$pid, 'where_type'=>'and')) : NULL);
			((!is_null($pname))?		array_push($where_array, array('where_value'=>'party_name_short', 'where_equalto'=>$pname, 'where_type'=>'like')) : NULL);
			((!is_null($first))?		array_push($where_array, array('where_value'=>'candidate_first', 'where_equalto'=>$first, 'where_type'=>'like')) : NULL);
			((!is_null($mi))?			array_push($where_array, array('where_value'=>'candidate_mi', 'where_equalto'=>$mi, 'where_type'=>'like')) : NULL);
			((!is_null($last))?			array_push($where_array, array('where_value'=>'candidate_last', 'where_equalto'=>$last, 'where_type'=>'like')) : NULL);
			((!is_null($incumbent))?	array_push($where_array, array('where_value'=>'candidate_incumbent', 'where_equalto'=>$incumbent, 'where_type'=>'and')) : NULL);
			((!is_null($winner))?		array_push($where_array, array('where_value'=>'candidate_winner', 'where_equalto'=>$winner, 'where_type'=>'and')) : NULL);
			((!is_null($rname))?		array_push($where_array, array('where_value'=>'race_title_short', 'where_equalto'=>$rname, 'where_type'=>'and')) : NULL);
			$candidates_records = $this->Election2012_model->getCandidate($fields, $join_array, $where_array, NULL, NULL, true);
			$candidates = $this->Election2012_model->getCandidate($fields, $join_array, $where_array, NULL, NULL, false);
			$result = array();
			foreach($candidates as $candidate) {
				$candidate['party_featured'] = ((in_array($candidate['candidate_id'], $preferences_list))? 1 : 0);
				array_push($result, $candidate);
			}
			$result = $this->utilities->multiColSort($result, 'candidate_last', SORT_DESC);
			echo json_encode($result);
		} catch (Exception $e) {
			return $e;
		}
	}

	public function fetchRaceStatusData()
	{
		try{
			// Load libraries and model for data retrieval
			$this->load->library(array('Utilities'));
			$this->load->model('Election2012_model', '', TRUE);
			// Get variables
			$id				= (($_POST)? (($_POST['id'])? $_POST['id'] : NULL) : NULL);
			$name			= (($_POST)? (($_POST['name'])? $_POST['name'] : NULL) : NULL);
			$complete		= (($_POST)? (($_POST['complete'])? $_POST['complete'] : NULL) : NULL);
			$flag			= (($_POST)? (($_POST['flag'])? $_POST['flag'] : NULL) : NULL);
			$org			= (($_POST)? (($_POST['org'])? $_POST['org'] : NULL) : NULL);
			$day_format		= (($_POST)? (($_POST['dformat'])? $_POST['dayformat'] : 'l, F j') : 'l, F j');
			$time_format	= (($_POST)? (($_POST['tformat'])? $_POST['timeformat'] : 'g:i a') : 'g:i a');
			// Get preferences
			$preferences_list = $this->fetchOrganizationPreference($org, 'racestatus', 'featured');
			// Begin fetching data
			$fields = "race_status_id, race_status_name, race_status_complete, race_status_flag";
			$where_array = array();
			((!is_null($id))?		array_push($where_array, array('where_value'=>'race_status_id', 'where_equalto'=>$id, 'where_type'=>'and')) : NULL);
			((!is_null($name))?		array_push($where_array, array('where_value'=>'race_status_name', 'where_equalto'=>$name, 'where_type'=>'like')) : NULL);
			((!is_null($complete))?	array_push($where_array, array('where_value'=>'race_status_complete', 'where_equalto'=>$complete, 'where_type'=>'and')) : NULL);
			((!is_null($flag))?		array_push($where_array, array('where_value'=>'race_status_flag', 'where_equalto'=>$flag, 'where_type'=>'and')) : NULL);
			$statuses_records = $this->Election2012_model->getRaceStatus($fields, NULL, $where_array, NULL, NULL, true);
			$statuses = $this->Election2012_model->getRaceStatus($fields, NULL, $where_array, NULL, NULL, false);
			$result = array();
			foreach($statuses as $status) {
				$status['status_featured'] = ((in_array($status['race_status_id'], $preferences_list))? 1 : 0);
				array_push($result, $status);
			}
			$result = $this->utilities->multiColSort($result, 'race_status_name', SORT_DESC);
			echo json_encode($result);
		} catch (Exception $e) {
			return $e;
		}
	}

	public function fetchRaceData()
	{
		try{
			// Load libraries and model for data retrieval
			$this->load->library(array('Utilities'));
			$this->load->model('Election2012_model', '', TRUE);
			// Get variables
			$rid			= (($_POST)? (($_POST['rid'])? $_POST['rid'] : NULL) : NULL);
			$rsid			= (($_POST)? (($_POST['rsid'])? $_POST['rsid'] : NULL) : NULL);
			$rrid			= (($_POST)? (($_POST['rrid'])? $_POST['rrid'] : NULL) : NULL);
			$rrfip			= (($_POST)? (($_POST['rrfip'])? $_POST['rrfip'] : NULL) : NULL);
			$title			= (($_POST)? (($_POST['title'])? $_POST['title'] : NULL) : NULL);
			$short			= (($_POST)? (($_POST['short'])? $_POST['short'] : NULL) : NULL);
			$name			= (($_POST)? (($_POST['name'])? $_POST['name'] : NULL) : NULL);
			$rbegin			= (($_POST)? (($_POST['rbegin'])? $_POST['rbegin'] : NULL) : NULL);
			$rend			= (($_POST)? (($_POST['rend'])? $_POST['rend'] : NULL) : NULL);
			$report			= (($_POST)? (($_POST['report'])? $_POST['report'] : NULL) : NULL);
			$percent		= (($_POST)? (($_POST['percent'])? $_POST['percent'] : NULL) : NULL);
			$total			= (($_POST)? (($_POST['total'])? $_POST['total'] : NULL) : NULL);
			$tpercent		= (($_POST)? (($_POST['tpercent'])? $_POST['tpercent'] : NULL) : NULL);
			$source			= (($_POST)? (($_POST['source'])? $_POST['source'] : NULL) : NULL);
			$org			= (($_POST)? (($_POST['org'])? $_POST['org'] : NULL) : NULL);
			$day_format		= (($_POST)? (($_POST['dformat'])? $_POST['dayformat'] : 'l, F j') : 'l, F j');
			$time_format	= (($_POST)? (($_POST['tformat'])? $_POST['timeformat'] : 'g:i a') : 'g:i a');
			// Get preferences
			$preferences_list = $this->fetchOrganizationPreference($org, 'race', 'featured');
			// Begin fetching data
			$fields = "election_race.race_id, election_race.race_status_id, election_race_result.race_result_id, election_race_result.county_fip, election_race.race_title, election_race.race_title_short, election_race.race_name, election_race.race_updated, election_race_result.race_result_precincts_reporting, election_race_result.race_result_precincts_percent, election_race_result.race_result_total, election_race_result.race_result_total_percent, election_race_source_map.source_value";
			$join_array = array(
				array('jn_table'=>'election_race_result', 'jn_value'=>'election_race_result.race_id', 'jn_equalto'=>'election_race.race_id'),
				array('jn_table'=>'election_race_source_map', 'jn_value'=>'election_race_source_map.race_id', 'jn_equalto'=>'election_race.race_id')
			);
			$where_array = array();
			((!is_null($rid))?		array_push($where_array, array('where_value'=>'election_race.race_id', 'where_equalto'=>$rid, 'where_type'=>'and')) : NULL);
			((!is_null($rsid))?		array_push($where_array, array('where_value'=>'election_race.race_status_id', 'where_equalto'=>$rsid, 'where_type'=>'like')) : NULL);
			((!is_null($rrid))?		array_push($where_array, array('where_value'=>'election_race_result.race_result_id', 'where_equalto'=>$rrid, 'where_type'=>'and')) : NULL);
			((!is_null($rrfip))?	array_push($where_array, array('where_value'=>'election_race_result.county_fip', 'where_equalto'=>$rrfip, 'where_type'=>'and')) : NULL);
			((!is_null($title))?	array_push($where_array, array('where_value'=>'election_race.race_title', 'where_equalto'=>$title, 'where_type'=>'and')) : NULL);
			((!is_null($short))?	array_push($where_array, array('where_value'=>'election_race.race_title_short', 'where_equalto'=>$short, 'where_type'=>'and')) : NULL);
			((!is_null($name))?		array_push($where_array, array('where_value'=>'election_race.race_name', 'where_equalto'=>$name, 'where_type'=>'and')) : NULL);
			((!is_null($rbegin))?	array_push($where_array, array('where_value'=>'election_race.race_updated >=', 'where_equalto'=>$rbegin, 'where_type'=>'and')) : NULL);
			((!is_null($rend))?		array_push($where_array, array('where_value'=>'election_race.race_updated =<', 'where_equalto'=>$rend, 'where_type'=>'and')) : NULL);
			((!is_null($report))?	array_push($where_array, array('where_value'=>'election_race_result.race_result_precincts_reporting', 'where_equalto'=>$report, 'where_type'=>'and')) : NULL);
			((!is_null($percent))?	array_push($where_array, array('where_value'=>'election_race_result.race_result_precincts_percent', 'where_equalto'=>$percent, 'where_type'=>'and')) : NULL);
			((!is_null($total))?	array_push($where_array, array('where_value'=>'election_race_result.race_result_total', 'where_equalto'=>$total, 'where_type'=>'and')) : NULL);
			((!is_null($tpercent))?	array_push($where_array, array('where_value'=>'election_race_result.race_result_total_percent', 'where_equalto'=>$tpercent, 'where_type'=>'and')) : NULL);
			((!is_null($source))?	array_push($where_array, array('where_value'=>'election_race_source_map.source_value', 'where_equalto'=>$source, 'where_type'=>'and')) : NULL);
			$races_records = $this->Election2012_model->getRace($fields, $join_array, $where_array, NULL, NULL, true);
			$races = $this->Election2012_model->getRace($fields, $join_array, $where_array, NULL, NULL, false);
			$result = array();
			foreach($races as $race) {
				$race['race_featured'] = ((in_array($race['race_id'], $preferences_list))? 1 : 0);
				/*$event_fields			= "am_stage_event_jn.am_stage_event_id,am_stage_event_jn.am_stage_id,am_stage_event_jn.am_event_id,am_event.organization_id,am_event.am_event_title,am_event.am_event_description,am_event.am_event_start,am_event.am_event_end,am_event.am_event_duration,am_event.am_event_recurringrule,am_event.am_event_allday,am_event.am_event_active,am_event.am_event_note,am_event.am_event_updated,am_event.am_event_updatedby";
				$event_join				= array(array('jn_table'=>'am_event', 'jn_value'=>'am_event.am_event_id', 'jn_equalto'=>'am_stage_event_jn.am_event_id'));
				$event_where			= array(array('where_value'=>'am_stage_id','where_equalto'=>$stage['am_stage_id'], 'where_type'=>'and'));
				$theevent				= $this->Agendamate_model->getAgendaStageEvent($event_fields,$event_join,$event_where,NULL,NULL,false);
				$stage['am_event_info']	= (($theevent)? $theevent : array());
				array_push($result, $stage);*/
				array_push($result, $race);
			}
			$result = $this->utilities->multiColSort($result, 'race_title', SORT_DESC);
			echo json_encode($result);
		} catch (Exception $e) {
			return $e;
		}
	}
	
	public function fetchOrganizationPreference($station, $subject, $type)
	{
		try{
			// Load model for data retrieval
			$this->load->model('Coreadmin_model', '', TRUE);
			$this->load->model('Election2012_model', '', TRUE);
			// Get station ID
			if(!is_numeric($station)) {
				$station_where = array(array('where_value'=>'organization_name', 'where_equalto'=>$station, 'where_type'=>'like'));
				$station_id = $this->Coreadmin_model->getOrganization("organization_id", NULL, $station_where, NULL, 1, false);
				$station_id = $station_id[0]['organization_id'];
				$station = $station_id;
			}
			$data['station'] = $station;
			$like_where = array(
				array('where_value'=>'preference_key', 'where_equalto'=>$subject, 'where_type'=>'like'),
				array('where_value'=>'preference_key', 'where_equalto'=>$type, 'where_type'=>'like'),
				array('where_value'=>'organization_id', 'where_equalto'=>$station, 'where_type'=>'and')
			);
			$preferences = $this->Election2012_model->getOrganizationPreference("preference_key", NULL, $like_where, array('preference_value'=>'ASC'), NULL, false);
			$preferences_list = "";
			foreach($preferences as $preference) {
				$gameid = explode("|", $preference['preference_key']);
				$preferences_list .= $gameid[1].",";
			}
			$preferences_list = explode(",", trim(substr($preferences_list, 0, -1)));
			return $preferences_list;
		} catch (Exception $e) {
			return $e;
		}
	}

	public function fetchResultsForDisplay($org='wish', $scope='state', $code='in', $race='', $size='mid')
	{
		try{
			// Get variables
			$theorganization	= (($_POST)? (($_POST['organization'])? $_POST['organization'] : $org) : $org);
			$thescope			= (($_POST)? (($_POST['scope'])? $_POST['scope'] : $scope) : $scope);
			$thecode			= (($_POST)? (($_POST['code'])? $_POST['code'] : $code) : $code);
			$therace			= (($_POST)? (($_POST['race'])? $_POST['race'] : $race) : $race);
			$thesize			= (($_POST)? (($_POST['size'])? $_POST['size'] : $size) : $size);
			// Load libraries and/or helpers
			$this->load->helper('url','cookie');
			$this->load->library(array('session','user_agent','email','Coreadministration','Utilities','Xmlfeed','Electiondatalib','Appres'));
			// Load custom configs
			$this->load->model('Coreadmin_model', '', TRUE);
			$module_id = $this->Coreadmin_model->getModule("module_id", NULL, array('module_dirname'=>$this->uri->segment(1)), NULL, 1, false);
			$module_id = $module_id[0]['module_id'];
			$configurations = $this->Coreadmin_model->getConfiguration(NULL, NULL, array('module_id'=>$module_id));
			foreach($configurations as $configuration) {
				$this->config->set_item($configuration['configuration_name'], $configuration['configuration_value']);
			}
			$base_url					= base_url();
			$station_id = 0;
			if(!is_numeric($theorganization)) {
				$organization_where = array(array('where_value'=>'organization_name_short', 'where_equalto'=>$theorganization, 'where_type'=>'like'));
				$result = $this->Coreadmin_model->getOrganization("organization_id", NULL, $organization_where, NULL, 1, false);
				$result = ((array_key_exists(0, $result))? ((array_key_exists("organization_id", $result[0]))? $result[0]['organization_id'] : 1) : 1);
				$station_id = $result;
			}
			$this->load->model('Election2012_model', '', TRUE);
			// Get module preferences and set as variable
			$like_where = array(
				array('where_value'=>'organization_id', 'where_equalto'=>$station_id, 'where_type'=>'and'),
				array('where_value'=>'preference_key', 'where_equalto'=>'preference', 'where_type'=>'like'),
				array('where_value'=>'preference_key', 'where_equalto'=>$data['module_id'], 'where_type'=>'like')
			);
			$module_preferences = $this->Election2012_model->getOrganizationPreference("preference_key, preference_value", NULL, $like_where, NULL, NULL, false);
			$mod_prefs = array();
			foreach($module_preferences as $preference) {
				$temp = array();
				$temp = $preference['preference_key'];
				$temp = explode("|", $temp);
				$preference['preference_key'] = $temp[2];
				$mod_prefs[$preference['preference_key']] = $preference['preference_value'];
			}
			$module_preferences = array();
			$module_preferences = $mod_prefs;
			switch($thescope)
			{
				case 'national':
				case 'pnational':
				{
					$xml = $this->electiondatalib->getParsedFeed($this->electiondatalib->downloadXml("ftp://wishtveln:wishtveln@electionsonline.ap.org/Pres_Reports/xml/statebystate_pres.xml"));
					$state_num = $this->electiondatalib->countNodes($xml, 'State');
					$states = array();
					for($s=0; $s<$state_num; $s++) {
						$state_code = (string) $this->electiondatalib->getApStateCode($xml, $s);
						$state_name = $this->Coreadmin_model->getStateNameByCode($state_code);
						$states[$s]['state_code'] = $state_code;
						$states[$s]['state_name'] = $state_name;
						$states[$s]['electoral_votes'] = (string) $this->electiondatalib->getApStateTotal($xml, $s);
						$states[$s]['pct_precincts_reporting'] = (int) $this->electiondatalib->getApStatePrecincts($xml, $s);
						$candidate_num = $this->electiondatalib->countNodes($xml->State[$s], 'Cand');
						for($c=0; $c<$candidate_num; $c++) {
							$states[$s]['candidates'][$c]['candidate_name'] = (string) $this->electiondatalib->getApCandidateName($xml, $s, $c);
							$states[$s]['candidates'][$c]['candidate_id'] = (string) $this->electiondatalib->getApCandidateID($xml, $s, $c);
							$states[$s]['candidates'][$c]['candidate_party'] = (string) $this->electiondatalib->getApCandidateParty($xml, $s, $c);
							$states[$s]['candidates'][$c]['candidate_popular_vote'] = (int) $this->electiondatalib->getApCandidatePopular($xml, $s, $c);
							$states[$s]['candidates'][$c]['candidate_popular_percent'] = (int) $this->electiondatalib->getApCandidatePopPercent($xml, $s, $c);
							$states[$s]['candidates'][$c]['candidate_won'] = (string) $this->electiondatalib->getApCandidateWinner($xml, $s, $c);
							$states[$s]['candidates'] = $this->utilities->multiColSort($states[$s]['candidates'], 'candidate_popular_percent', SORT_DESC);
						}
					}
					$result = json_encode($states);
					echo($result);
					exit;
				}
				case 'state':
				case 'pstate':
				{
					$state = $this->Coreadmin_model->getStateNameByCode($thecode);
					$race_data = array();
					try{
						$race_join = array(
							array('jn_table'=>'election_race_result', 'jn_value'=>'election_race_result.race_id', 'jn_equalto'=>'election_race.race_id'),
							array('jn_table'=>'election_race_source_map', 'jn_value'=>'election_race_source_map.race_id', 'jn_equalto'=>'election_race.race_id')
						);
						$race_where = array(
							array('where_value'=>'election_race_source_map.source_id', 'where_equalto'=>$station_id, 'where_type'=>'and'),
							array('where_value'=>'election_race_result.county_fip >', 'where_equalto'=>10000, 'where_type'=>'and'),
							array('where_value'=>'election_race.race_name', 'where_equalto'=>$therace, 'where_type'=>'like')
						);
						$fields = "election_race.race_id, election_race.race_status_id, election_race.race_title, election_race.race_title_short, election_race.race_name, election_race.race_updated, election_race_result.race_result_id, election_race_result.county_fip, election_race_result.race_result_precincts_reporting, election_race_result.race_result_precincts_percent, election_race_result.race_result_total, election_race_result.race_result_total_percent, election_race_source_map.source_id, election_race_source_map.source_value";
						$total_records = $this->Election2012_model->getRace($fields, $race_join, $race_where, NULL, NULL, true);
						$races = $this->Election2012_model->getRace($fields, $race_join, $race_where, NULL, NULL, false);
						foreach($races as $race) {
							$race['county_name'] = strtoupper($this->Coreadmin_model->getCountyNameByFIPS($race['county_fip']));
							$statuses = $this->Election2012_model->getRaceStatus(NULL, NULL, array('race_status_id'=>$race['race_status_id']), NULL, 1, false);
							foreach($statuses as $status) {
								$race['race_status_name'] = $status['race_status_name'];
							}
							$candidate_join = array(
								array('jn_table'=>'election_candidate', 'jn_value'=>'election_candidate.candidate_id', 'jn_equalto'=>'election_race_candidate_jn.candidate_id'),
								array('jn_table'=>'election_candidate_source_map', 'jn_value'=>'election_candidate_source_map.candidate_id', 'jn_equalto'=>'election_race_candidate_jn.candidate_id'),
								array('jn_table'=>'election_candidate_result', 'jn_value'=>'election_candidate_result.candidate_id', 'jn_equalto'=>'election_candidate.candidate_id')
							);
							$candidate_where = array(
								array('where_value'=>'election_race_candidate_jn.race_id', 'where_equalto'=>$race['race_id'], 'where_type'=>'and'),
								array('where_value'=>'election_candidate_result.race_result_id', 'where_equalto'=>$race['race_result_id'], 'where_type'=>'and')
							);
							$candidate_fields = "election_candidate.candidate_id, election_candidate.party_id, election_candidate.race_id, election_candidate.candidate_first, election_candidate.candidate_mi, election_candidate.candidate_last, election_candidate.party_id, election_candidate.candidate_incumbent, election_candidate.candidate_winner, election_candidate_result.candidate_result_value, election_candidate_result.candidate_result_percent, election_candidate_source_map.source_value";
							$candidates = $this->Election2012_model->getRaceCandidate($candidate_fields, $candidate_join, $candidate_where, array('election_candidate_result.candidate_result_percent'=>'ASC'), NULL, false);
							foreach($candidates as $candidate) {
								$race['race_candidates']['candidate-'.$candidate['candidate_id']] = array(
									'candidate_id'				=> $candidate['candidate_id'],
									'source_value'				=> $candidate['source_value'],
									'candidate_first'			=> strtoupper($candidate['candidate_first']),
									'candidate_mi'				=> strtoupper($candidate['candidate_mi']),
									'candidate_last'			=> strtoupper($candidate['candidate_last']),
									'candidate_party_code'		=> $this->Election2012_model->getCodeByID($candidate['party_id']),
									'candidate_incumbent'		=> $candidate['candidate_incumbent'],
									'candidate_winner'			=> $candidate['candidate_winner'],
									'candidate_result_value'	=> $candidate['candidate_result_value'],
									'candidate_result_percent'	=> $candidate['candidate_result_percent']
								);
							}
							array_push($race_data, $race);
						}
						$result = $this->utilities->multiColSort($race_data, 'race_title', SORT_ASC);
					} catch (Exception $e) {
						return $e;
					}
					break;
				}
				default:
				{
					$result = "Hello";
				}
			}
			//$this->db->close();
		} catch (Exception $e) {
			$result = array();
		}
		echo json_encode($result);
	}

	public function getRaceIDsByType($org='0', $state='0', $type='0') {
		$org = (($org=='0')? 'wish' : $org);
		if(!is_numeric($org)) {
			$org_where = array(array('where_value'=>'organization_name_short', 'where_equalto'=>$org, 'where_type'=>'and'));
			$org_id = $this->Coreadmin_model->getOrganization("organization_id", NULL, $org_where, NULL, 1, false);
			$org_id = (($org_id)? ((array_key_exists(0, $org_id))? $org_id[0]['organization_id'] : 1) : 1);
			$org = $org_id;
		}
		/*$state = (($state=='0')? 'in' : str_replace("_", ",", $state));
		$state = explode(",", $state);
		$state_list = "";
		foreach($state as $st) {
			if(!is_numeric($st)) {
				$state_where = array(array('where_value'=>'state_code', 'where_equalto'=>$st, 'where_type'=>'and'));
				$state_id = $this->Coreadmin_model->getState("state_id", NULL, $state_where, NULL, 1, false);
				$state_id = (($state_id)? ((array_key_exists(0, $state_id))? $state_id[0]['state_id'] : 1) : 1);
				$state_list .= $state_id.",";
			}
		}
		$state_list = array();
		$state_join = array(
			array('jn_table'=>'core_state', 'jn_value'=>'core_state.state_id', 'jn_equalto'=>'core_organization_state_jn.state_id')
		);
		$states = $this->Coreadmin_model->getOrganizationState("core_state.state_id, core_state.state_name", $state_join, array('organization_id'=>$org), NULL, NULL, false);
		$state_info = array();
		$i=0;
		foreach($states as $key=>$value) {
			$state_list[$i] = $value['state_id']; $i++;
		}
		$type = (($type=='0')? 'statewide' : str_replace("_", " ", $type));*/
		$where_array = array(
			array('where_value'=>'source_id', 'where_equalto'=>$org, 'where_type'=>'and'),
			//array('where_value'=>'state_id', 'where_equalto'=>$state_list, 'where_type'=>'in'),
			//array('where_value'=>'source_fips_label', 'where_equalto'=>$type, 'where_type'=>'and')
		);
		$fips = $this->Election2012_model->getSourceLocalFips("fips_id", NULL, $where_array, NULL, NULL, false);
		$fips_list = array();
		$i=0;
		if($fips) {
			foreach($fips as $fip) {
				$fips_list[$i] = $fip['fips_id'].","; $i++;
			}
		}
		$race_where = array(
			array('where_value'=>'source_id', 'where_equalto'=>$org, 'where_type'=>'and'),
			array('where_value'=>'county_fip', 'where_equalto'=>$fips_list, 'where_type'=>'in')
		);
		$races = $this->Election2012_model->getRaceResult("race_id", NULL, $race_where, NULL, NULL, false);
		$response = array();
		$i=0;
		foreach($races as $race) {
			$response[$i] = $race['race_id']; $i++;
		}
		return $response;
	}

	public function APXML($file){
		// Load libraries and/or helpers
		$this->load->helper('url','cookie');
		$this->load->library(array('session','user_agent','email','Coreadministration','Utilities','Xmlfeed','Electiondatalib','Appres'));
		$xml = $this->electiondatalib->downloadXml("ftp://wishtveln:wishtveln@electionsonline.ap.org/Pres_Reports/xml/statebystate_pres.xml");
		$data['op'] = "apxml";
		$data['xml'] = $xml;
		$this->output->set_content_type('text/xml');
		$this->load->view('election2012/election2012', $data);
	}

	public function feedTest(){
		$this->load->library(array('Utilities','Electiondatalib'));
		$this->load->model('Electiondata_model', '', TRUE);
		$xml = $this->electiondatalib->getParsedFeed($this->electiondatalib->downloadXml("ftp://wishtveln:wishtveln@electionsonline.ap.org/Pres_Reports/xml/pres_summary_all.xml"));
		echo($xml);
	}
	
	public function electionIngestCron($id=NULL){
		$data = $this->initialize();
		$this->load->model('Coreadmin_model', '', TRUE);
		$this->load->model('Election2012_model', '', TRUE);
		$this->load->library(array('Utilities'));
		$this->load->library(array('electiondatalib'));
		// Load custom configs
		$response = array();
		try{
			// Establish which organizations should be included in this update
			$org_array = array();
			$orgs = (($id)? $id : NULL);
			// Only process if orgs are present, otherwise kill the script
			if($orgs){
				if (strpos($orgs,':') !== false){
					$org_array = explode(":", $orgs);
				} else {
					$org_array[0] = $orgs;
				}
				// Begin processing organization election results
				foreach($org_array as $theorg){
					$data['station_id'] = 0;
					$data['station_display'] = "";
					$station_id = $theorg;
					if(!is_numeric($station_id)) {
						$station_where = array(array('where_value'=>'organization_name', 'where_equalto'=>$station_id, 'where_type'=>'like'));
						$station_data = $this->Coreadmin_model->getOrganization("organization_id, organization_name", NULL, $station_where, NULL, 1, false);
						$data['station_id'] = $station_id = ((array_key_exists(0, $station_data))? $station_data[0]['organization_id'] : 0);
						$data['station_display'] = $station_display  = ((array_key_exists(0, $station_data))? $station_data[0]['organization_name'] : 0);
					} else {
						$station_where = array(array('where_value'=>'organization_id', 'where_equalto'=>$station_id, 'where_type'=>'and'));
						$station_data = $this->Coreadmin_model->getOrganization("organization_id, organization_name", NULL, $station_where, NULL, 1, false);
						$data['station_id'] = $station_id = ((array_key_exists(0, $station_data))? $station_data[0]['organization_id'] : 0);
						$data['station_display'] = $station_display = ((array_key_exists(0, $station_data))? $station_data[0]['organization_name'] : 0);
					}
					$source_id = $station_id;
					// Get module preferences and set as variable
					$like_where = array(
						array('where_value'=>'organization_id', 'where_equalto'=>$station_id, 'where_type'=>'and'),
						array('where_value'=>'preference_key', 'where_equalto'=>'preference', 'where_type'=>'like'),
						array('where_value'=>'preference_key', 'where_equalto'=>$data['module_id'], 'where_type'=>'like')
					);
					$module_preferences = $this->Election2012_model->getOrganizationPreference("preference_key, preference_value", NULL, $like_where, NULL, NULL, false);
					$mod_prefs = array();
					foreach($module_preferences as $preference) {
						$temp = array();
						$temp = $preference['preference_key'];
						$temp = explode("|", $temp);
						$preference['preference_key'] = $temp[2];
						$mod_prefs[$preference['preference_key']] = $preference['preference_value'];
					}
					$data["module_preferences"] = $module_preferences = $mod_prefs;
					$proceed = (($mod_prefs)? ((array_key_exists("enable_cron", $mod_prefs))? (($mod_prefs["enable_cron"]==1)? true : false) : false) : false);
					if($proceed==true){
						$source_type = $mod_prefs['election_feed_type'];
						$source_url = $mod_prefs['election_xml'];
						$fip_field = $mod_prefs['fip_field'];
						$races = "";
						$candidates = "";
						$response[$theorg]['org'] = $station_id;
						$response[$theorg]['error'] = false;
						$response[$theorg]['message'] = array();
						$error = false;
						$theraces2d = array();
						$thecandidates2d = array();
						$beforenow = $starttime = time();
						$totalr = 0;
						$totalc = 0;
						switch($source_type)
						{
							case "leader":
							{
								if($xml = $this->electiondatalib->getParsedFeed($this->electiondatalib->downloadXml($source_url))){
									$race_num = $this->electiondatalib->countNodes($xml, 'race');
									// Remove old race result records
									$delete_race_where_array = array(
										array('where_value'=>'source_id', 'where_equalto'=>$source_id, 'where_type'=>'and')
									);
									$race_result_delete_result = $this->Election2012_model->deleteRaceResult($delete_race_where_array);
									// Remove old candidate result records
									$delete_candidate_where_array = array(
										array('where_value'=>'source_id', 'where_equalto'=>$source_id, 'where_type'=>'and')
									);
									$candidate_result_delete_result = $this->Election2012_model->deleteCandidateResult($delete_candidate_where_array);
									for($r=0; $r<$race_num; $r++) {
										// Process race info
										$therace = $xml->race[$r]->race_info;
										$leader_race_id				= (int) $therace->race_id;
										$race_title_1				= (string) $therace->race_name1;
										$race_title_2				= (string) $therace->race_name2;
										$race_title_3				= (string) $therace->race_name3;
										$race_precincts_reporting	= (int) $therace->precincts_reporting;
										$race_precincts_percent		= (int) $therace->pct_precincts_reporting;
										$race_total_precincts		= (int) $therace->precincts_total;
										$race_last_leader_update	= strtotime($therace->last_updated);
										$race_total_votes			= (int) $therace->total_vote;
										$jurisdiction_id			= (string) $therace->jurisdiction_id;
										$race_other_1				= (string) $therace->other1;
										$race_other_2				= (string) $therace->other2;
										$race_other_3				= (string) $therace->other3;
										//proposition, yesno1, yesno2, yesno3, yesno4, other1, other2, other3
										// Ingest race info
										$race_source_join = array(
											array('jn_table'=>'election_race', 'jn_value'=>'election_race.race_id', 'jn_equalto'=>'election_race_source_map.race_id')
										);
										$race_data = $this->Election2012_model->getRaceDataSource("election_race.race_id", $race_source_join, array('election_race_source_map.source_id'=>$source_id, 'election_race_source_map.source_value'=>$leader_race_id), NULL, NULL, false);
										$race_id = (($race_data)? ((array_key_exists(0, $race_data))? ((array_key_exists("race_id", $race_data[0]))? $race_data[0]['race_id'] : NULL) : NULL) : NULL);
										// If the race doesn't exist, create it
										if(!$race_id) {
											$race_insert_array = array(
												'race_status_id'	=> 2, 
												'race_title'		=> $race_title_1, 
												'race_title_short'	=> $race_title_2, 
												'race_name'			=> str_replace(" ", "_", strtolower($race_title_1." ".$race_title_2)),
												'race_updated'		=> time()
											);
											try{
												$race_insert_result = $this->Election2012_model->insertRace($race_insert_array);
											} catch(Exception $e) {
												$error = true;
												array_push($response[$theorg]['message'], "Race result save error: ".$e);
											}
											$race_id = mysql_insert_id();
											// Map the source ID with the internal race ID
											$map_insert_array = array(
												'race_id'		=> $race_id,
												'source_id'		=> $source_id,
												'source_value'	=> $leader_race_id
											);
											try{
												$map_insert_result = $this->Election2012_model->insertRaceDataSource($map_insert_array);
											} catch(Exception $e) {
												$error = true;
												array_push($response[$theorg]['message'], "Race data source mapping error: ".$e);
											}
										}
										array_push($theraces2d, $race_id);
										$race_result_insert_array = array(
											'source_id'							=> $source_id,
											'race_id'							=> $race_id,
											'county_fip'						=> ${$fip_field},
											'race_result_precincts_reporting'	=> $race_precincts_reporting,
											'race_result_precincts_percent'		=> $race_precincts_percent,
											'race_result_total'					=> $race_total_votes,
											'race_result_updated'				=> $race_last_leader_update
										);
										$race_result_insert_result = $this->Election2012_model->insertRaceResult($race_result_insert_array);
										$totalr++;
										$new_race_result_id = mysql_insert_id();
										// Process candidate info
										$thecandidates = $xml->race[$r]->candidate_info;
										$candidate_num = $this->electiondatalib->countNodes($thecandidates, 'candidate');
										$candidate_data = array();
										for($c=0; $c<$candidate_num; $c++) {
											$thecandidate = $thecandidates->candidate[$c];
											$leader_candidate_id		= (int) $thecandidate->candidate_id;
											$candidate_first			= (string) $thecandidate->first_name;
											$candidate_mi				= (string) $thecandidate->initial;
											$candidate_last				= (string) $thecandidate->last_name;
											$candidate_party			= $this->Election2012_model->getPartyByCode((string) $thecandidate->party_abbr);
											$candidate_incumbent		= (((string) $thecandidate->incumbent=='True')? 1 : 0);
											$candidate_winner			= (((string) $thecandidate->winner=='True')? 1 : 0);
											$candidate_projected_winner	= (((int) $thecandidate->projected_winner=='True')? 1 : 0);
											$candidate_cand_vote		= (int) str_replace(",", "", $thecandidate->cand_vote);
											$candidate_vote_percent		= (int) $thecandidate->vote_percent;
											//$cother1, $cother2, $cother3
											$candidate_source_join = array(
												array('jn_table'=>'election_candidate', 'jn_value'=>'election_candidate.candidate_id', 'jn_equalto'=>'election_candidate_source_map.candidate_id')
											);
											$candidate_data = $this->Election2012_model->getCandidateDataSource("election_candidate.candidate_id", $candidate_source_join, array('election_candidate_source_map.source_id'=>$source_id, 'election_candidate_source_map.source_value'=>$leader_race_id.$leader_candidate_id), NULL, NULL, false);
											$candidate_id = ((array_key_exists(0, $candidate_data))? $candidate_data[0]['candidate_id'] : NULL);
											// If the candidate doesn't exist, create it
											if(!$candidate_id) {
												$candidate_insert_array = array(
													'party_id'				=> $candidate_party,
													'candidate_first'		=> $candidate_first,
													'candidate_mi'			=> $candidate_mi,
													'candidate_last'		=> $candidate_last,
													'candidate_incumbent'	=> $candidate_incumbent,
													'candidate_winner'		=> $candidate_winner,
													'candidate_updated'		=> time()
												);
												$candidate_id = 0;
												try{
													$candidate_insert_result = $this->Election2012_model->insertCandidate($candidate_insert_array);
													$candidate_id = mysql_insert_id();
												} catch(Exception $e) {
													$error = true;
													array_push($response[$theorg]['message'], "Candidate save error: ".$e);
												}
												// Map the source ID with the internal race ID
												$cmap_insert_array = array(
													'candidate_id'	=> $candidate_id,
													'source_id'		=> $source_id,
													'source_value'	=> $leader_race_id.$leader_candidate_id
												);
												try{
													$cmap_insert_result = $this->Election2012_model->insertCandidateDataSource($cmap_insert_array);
												} catch(Exception $e) {
													$error = true;
													array_push($response[$theorg]['message'], "Candidate data source mapping error: ".$e);
												}
											}
											$check_race_candidate = $this->Election2012_model->getRaceCandidate(NULL, NULL, array('race_id'=>$race_id, 'candidate_id'=>$candidate_id), NULL, NULL, true);
											if(!$check_race_candidate) {
												$race_candidate_array = array(
													'race_id'		=> $race_id,
													'candidate_id'	=> $candidate_id
												);
												try{
													$race_candidate_result = $this->Election2012_model->insertRaceCandidate($race_candidate_array);
												} catch(Exception $e) {
													$error = true;
													array_push($response[$theorg]['message'], "Race/candidate mapping error: ".$e);
												}
											}
											array_push($thecandidates2d, $candidate_id);
											$candidate_result_insert_array = array(
												'source_id'					=> $source_id,
												'race_result_id'			=> $new_race_result_id,
												'candidate_id'				=> $candidate_id,
												'party_id'					=> $candidate_party,
												'candidate_result_value'	=> $candidate_cand_vote,
												'candidate_result_percent'	=> $candidate_vote_percent,
												'candidate_result_winner'	=> $candidate_winner,
												'candidate_result_updated'	=> time()
											);
											try{
												$candidate_result_insert_result = $this->Election2012_model->insertCandidateResult($candidate_result_insert_array);
											} catch(Exception $e) {
												$error = true;
												array_push($response[$theorg]['message'], "Candidate result save error: ".$e);
											}
											$totalc++;
										}
									}
									//log_message('info', 'Ingestion complete.');
									$elapsed = date("i:s", time() - $starttime);
									array_push($response[$theorg]['message'], "Ingestion Complete for ".$data['station_display'].".");
									array_push($response[$theorg]['message'], "Processing time for ".$data['station_display']." - ".$elapsed.".");
									array_push($response[$theorg]['message'], $totalr." races processed for ".$data['station_display'].".");
									array_push($response[$theorg]['message'], $totalc." candidates processed for ".$data['station_display'].".");
								} else {
									$error = true;
									array_push($response[$theorg]['message'], "Unable to download XML feed");
								}
								break;
							}
							case "newsticker":
							{
								$source_url = $module_preferences['election_xml'];
								if($xml = $this->electiondatalib->getParsedFeed($this->electiondatalib->downloadXml($source_url))){
									$race_num = $this->electiondatalib->countNodes($xml, 'RACE');
									// Remove old race result records
									$delete_race_where_array = array(
										array('where_value'=>'source_id', 'where_equalto'=>$source_id, 'where_type'=>'and')
									);
									$race_result_delete_result = $this->Election2012_model->deleteRaceResult($delete_race_where_array);
									// Remove old candidate result records
									$delete_candidate_where_array = array(
										array('where_value'=>'source_id', 'where_equalto'=>$source_id, 'where_type'=>'and')
									);
									$candidate_result_delete_result = $this->Election2012_model->deleteCandidateResult($delete_candidate_where_array);
									for($r=0; $r<$race_num; $r++) {
										// Process race info
										$therace = $xml->RACE[$r]->RACE_INFO;
										$newsticker_race_id			= (int) $therace->RACE_ID;
										$race_title_1				= (string) $therace->RACE_NAME1;
										$race_title_2				= (string) $therace->RACE_NAME2;
										$race_title_3				= (string) $therace->RACE_NAME3;
										$race_title_4				= (string) $therace->RACE_NAME4;
										$race_prompt_1				= (string) $therace->PROMPT_LINES1;
										$race_prompt_2				= (string) $therace->PROMPT_LINES2;
										$race_precincts_reporting	= (int) $therace->PRECINCTS_REPORTING;
										$race_precincts_percent		= (int) $therace->PCT_PRECINCTS_REPORTING;
										$race_total_precincts		= (int) $therace->PRECINCTS_TOTAL;
										$race_last_newsticker_update= strtotime($therace->UPDATED);
										$race_total_votes			= (int) $therace->ALLCAND_VOTES;
										$jurisdiction_id			= (string) $therace->JURISDICTION_ID;
										//proposition, yesno1, yesno2, yesno3, yesno4, other1, other2, other3
										// Ingest race info
										$race_source_join = array(
											array('jn_table'=>'election_race', 'jn_value'=>'election_race.race_id', 'jn_equalto'=>'election_race_source_map.race_id')
										);
										$race_data = $this->Election2012_model->getRaceDataSource("election_race.race_id", $race_source_join, array('election_race_source_map.source_id'=>$source_id, 'election_race_source_map.source_value'=>$newsticker_race_id), NULL, NULL, false);
										$race_id = ((array_key_exists(0, $race_data))? $race_data[0]['race_id'] : NULL);
										// If the race doesn't exist, create it
										if(!$race_id) {
											$race_insert_array = array(
												'race_status_id'	=> 2, 
												'race_title'		=> $race_title_1.", ".$race_title_2, 
												'race_title_short'	=> $race_title_1, 
												'race_name'			=> str_replace(" ", "_", strtolower($race_title_1." ".$race_title_2)),
												'race_updated'		=> time()
											);
											try{
												$race_insert_result = $this->Election2012_model->insertRace($race_insert_array);
											} catch(Exception $e) {
												$error .= $e."\n";
											}
											$race_id = mysql_insert_id();
											// Map the source ID with the internal race ID
											$map_insert_array = array(
												'race_id'		=> $race_id,
												'source_id'		=> $source_id,
												'source_value'	=> $newsticker_race_id
											);
											try{
												$map_insert_result = $this->Election2012_model->insertRaceDataSource($map_insert_array);
											} catch(Exception $e) {
												$error = true;
												array_push($response[$theorg]['message'], "Race data source mapping error: ".$e);
											}
										}
										array_push($theraces2d, $race_id);
										$race_result_insert_array = array(
											'source_id'							=> $source_id,
											'race_id'							=> $race_id,
											'county_fip'						=> ${$fip_field},
											'race_result_precincts_reporting'	=> $race_precincts_reporting,
											'race_result_precincts_percent'		=> $race_precincts_percent,
											'race_result_total'					=> $race_total_votes,
											'race_result_updated'				=> time()
										);
										$race_result_insert_result = $this->Election2012_model->insertRaceResult($race_result_insert_array);
										$totalr++;
										$new_race_result_id = mysql_insert_id();
										// Process candidate info
										$thecandidates = $xml->RACE[$r]->CANDIDATE_INFO;
										$candidate_num = $this->electiondatalib->countNodes($thecandidates, 'CANDIDATE');
										$candidate_data = array();
										for($c=0; $c<$candidate_num; $c++) {
											$thecandidate = $thecandidates->CANDIDATE[$c];
											$pabbr = ((strstr((string) $thecandidate->FIRST_NAME, "-")!=false)? substr((string) $thecandidate->FIRST_NAME, 0, 1) : NULL);
											$newsticker_candidate_id	= (int) $thecandidate->CANDIDATE_ID;
											$candidate_display_order	= (int) $thecandidate->DISPLAY_ORDER;
											$candidate_visible			= (int) $thecandidate->VISIBLE;
											$candidate_first			= (string) ((strstr((string) $thecandidate->FIRST_NAME, "-")!=false)? substr((string) $thecandidate->FIRST_NAME, 2) : $thecandidate->FIRST_NAME);
											$candidate_last				= (string) $thecandidate->LAST_NAME;
											$candidate_party			= $this->Election2012_model->getPartyByCode((($pabbr)? $pabbr : (string) $thecandidate->PARTY_ABBR));
											$candidate_party2			= (string) $thecandidate->PARTY_NAME1;
											$candidate_party3			= (string) $thecandidate->PARTY_NAME2;
											$candidate_incumbent		= (((string) $thecandidate->INCUMBENT=='Y')? 1 : 0);
											$candidate_winner			= (((string) $thecandidate->WINNER=='Y' || (string) $thecandidate->WINNER=='Z')? 1 : 0);
											$candidate_cand_vote		= (int) $thecandidate->VOTE_TOTAL;
											$candidate_vote_percent		= (int) $thecandidate->PCT_TOTAL;
											//$cother1, $cother2, $cother3
											$candidate_source_join = array(
												array('jn_table'=>'election_candidate', 'jn_value'=>'election_candidate.candidate_id', 'jn_equalto'=>'election_candidate_source_map.candidate_id')
											);
											$candidate_id = $this->Election2012_model->getCandidateDataSource("election_candidate.candidate_id", $candidate_source_join, array('election_candidate_source_map.source_id'=>$source_id, 'election_candidate_source_map.source_value'=>$newsticker_race_id.$newsticker_candidate_id), NULL, NULL, false);
											$candidate_id = ((array_key_exists(0, $candidate_id))? $candidate_id[0]['candidate_id'] : NULL);
											// If the candidate doesn't exist, create it
											if(!$candidate_id) {
												$candidate_insert_array = array(
													'party_id'				=> $candidate_party,
													'candidate_first'		=> $candidate_first,
													'candidate_last'		=> $candidate_last,
													'candidate_incumbent'	=> $candidate_incumbent,
													'candidate_winner'		=> $candidate_winner,
													'candidate_updated'		=> time()
												);
												$candidate_id = 0;
												try{
													$candidate_insert_result = $this->Election2012_model->insertCandidate($candidate_insert_array);
													$candidate_id = mysql_insert_id();
												} catch(Exception $e) {
													$error .= $e."\n";
												}
												// Map the source ID with the internal race ID
												$cmap_insert_array = array(
													'candidate_id'	=> $candidate_id,
													'source_id'		=> $source_id,
													'source_value'	=> $newsticker_race_id.$newsticker_candidate_id
												);
												try{
													$cmap_insert_result = $this->Election2012_model->insertCandidateDataSource($cmap_insert_array);
												} catch(Exception $e) {
													$error = true;
													array_push($response[$theorg]['message'], "Candidate data source mapping error: ".$e);
												}
											}
											$check_race_candidate = $this->Election2012_model->getRaceCandidate(NULL, NULL, array('race_id'=>$race_id, 'candidate_id'=>$candidate_id), NULL, NULL, true);
											if(!$check_race_candidate) {
												$race_candidate_array = array(
													'race_id'		=> $race_id,
													'candidate_id'	=> $candidate_id
												);
												try{
													$race_candidate_result = $this->Election2012_model->insertRaceCandidate($race_candidate_array);
												} catch(Exception $e) {
													$error = true;
													array_push($response[$theorg]['message'], "Race/candidate mapping error: ".$e);
												}
											}
											array_push($thecandidates2d, $candidate_id);
											$candidate_result_insert_array = array(
												'source_id'					=> $source_id,
												'race_result_id'			=> $new_race_result_id,
												'candidate_id'				=> $candidate_id,
												'party_id'					=> $candidate_party,
												'candidate_result_value'	=> $candidate_cand_vote,
												'candidate_result_percent'	=> $candidate_vote_percent,
												'candidate_result_winner'	=> $candidate_winner,
												'candidate_result_updated'	=> time()
											);
											try{
												$candidate_result_insert_result = $this->Election2012_model->insertCandidateResult($candidate_result_insert_array);
											} catch(Exception $e) {
												$error = true;
												array_push($response[$theorg]['message'], "Candidate result save error: ".$e);
											}
											$totalc++;
										}
									}
									//log_message('info', 'Ingestion complete.');
									$elapsed = date("i:s", time() - $starttime);
									array_push($response[$theorg]['message'], "Ingestion Complete for ".$data['station_display'].".");
									array_push($response[$theorg]['message'], "Processing time for ".$data['station_display']." - ".$elapsed.".");
									array_push($response[$theorg]['message'], $totalr." races processed for ".$data['station_display'].".");
									array_push($response[$theorg]['message'], $totalc." candidates processed for ".$data['station_display'].".");
								} else {
									$error = true;
									array_push($response[$theorg]['message'], "Unable to download XML feed.");
								}
								break;
							}
							default:
							{
								$error = true;
								array_push($response[$theorg]['message'], "Election feed type not defined.");
							}
						}
					} else {
						$error = true;
						array_push($response[$theorg]["message"], $data['station_display']." does not have the election refresh enabled.");
					}
				}
			} else {
				die("Election refresh failed. No organization IDs were supplied.");
			}
		} catch (Exception $e) {
			die($e);
		}
		echo "<pre>";
		print_r($response);
		echo "</pre>";
	}

	public function electionExportCron($id=NULL, $form=NULL, $state=NULL, $type=NULL){
		$data = $this->initialize();
		$this->load->model('Coreadmin_model', '', TRUE);
		$this->load->model('Election2012_model', '', TRUE);
		$this->load->library(array('Utilities'));
		$this->load->library(array('electiondatalib'));
		date_default_timezone_set('America/Indianapolis');
		// Load custom configs
		$response = array();
		try{
			// Establish which organizations should be included in this update
			$org_array = array();
			$orgs = (($id)? $id : NULL);
			// Only process if orgs are present, otherwise kill the script
			if($orgs){
				if (strpos($orgs,':') !== false){
					$org_array = explode(":", $orgs);
				} else {
					$org_array[0] = $orgs;
				}
				// Begin processing organization election results
				foreach($org_array as $theorg){
					$data['station_id'] = 0;
					$data['station_display'] = "";
					$station_id = $theorg;
					if(!is_numeric($station_id)) {
						$station_where = array(array('where_value'=>'organization_name', 'where_equalto'=>$station_id, 'where_type'=>'like'));
						$station_data = $this->Coreadmin_model->getOrganization("organization_id, organization_name", NULL, $station_where, NULL, 1, false);
						$data['station_id'] = $station_id = ((array_key_exists(0, $station_data))? $station_data[0]['organization_id'] : 0);
						$data['station_display'] = $station_display  = ((array_key_exists(0, $station_data))? $station_data[0]['organization_name'] : 0);
					} else {
						$station_where = array(array('where_value'=>'organization_id', 'where_equalto'=>$station_id, 'where_type'=>'and'));
						$station_data = $this->Coreadmin_model->getOrganization("organization_id, organization_name", NULL, $station_where, NULL, 1, false);
						$data['station_id'] = $station_id = ((array_key_exists(0, $station_data))? $station_data[0]['organization_id'] : 0);
						$data['station_display'] = $station_display = ((array_key_exists(0, $station_data))? $station_data[0]['organization_name'] : 0);
					}
					$source_id = $station_id;
					// Get module preferences and set as variable
					$like_where = array(
						array('where_value'=>'organization_id', 'where_equalto'=>$station_id, 'where_type'=>'and'),
						array('where_value'=>'preference_key', 'where_equalto'=>'preference', 'where_type'=>'like'),
						array('where_value'=>'preference_key', 'where_equalto'=>$data['module_id'], 'where_type'=>'like')
					);
					$module_preferences = $this->Election2012_model->getOrganizationPreference("preference_key, preference_value", NULL, $like_where, NULL, NULL, false);
					$mod_prefs = array();
					foreach($module_preferences as $preference) {
						$temp = array();
						$temp = $preference['preference_key'];
						$temp = explode("|", $temp);
						$preference['preference_key'] = $temp[2];
						$mod_prefs[$preference['preference_key']] = $preference['preference_value'];
					}
					$data["module_preferences"] = $module_preferences = $mod_prefs;
					$proceed = (($mod_prefs)? ((array_key_exists("enable_cron", $mod_prefs))? (($mod_prefs["enable_cron"]==1)? true : false) : false) : false);
					if($proceed==true){
						$source_type = $mod_prefs['election_feed_type'];
						$source_url = $mod_prefs['election_xml'];
						$fip_field = $mod_prefs['fip_field'];
						$races = "";
						$candidates = "";
						$response[$theorg]['org'] = $station_id;
						$response[$theorg]['error'] = false;
						$response[$theorg]['message'] = array();
						$error = false;
						$theraces2d = array();
						$thecandidates2d = array();
						$beforenow = $starttime = time();
						$totalr = 0;
						$totalc = 0;
						switch($source_type)
						{
							case "leader":
							{
								if($xml = $this->electiondatalib->getParsedFeed($this->electiondatalib->downloadXml($source_url))){
									$race_num = $this->electiondatalib->countNodes($xml, 'race');
									// Remove old race result records
									$delete_race_where_array = array(
										array('where_value'=>'source_id', 'where_equalto'=>$source_id, 'where_type'=>'and')
									);
									$race_result_delete_result = $this->Election2012_model->deleteRaceResult($delete_race_where_array);
									// Remove old candidate result records
									$delete_candidate_where_array = array(
										array('where_value'=>'source_id', 'where_equalto'=>$source_id, 'where_type'=>'and')
									);
									$candidate_result_delete_result = $this->Election2012_model->deleteCandidateResult($delete_candidate_where_array);
									for($r=0; $r<$race_num; $r++) {
										// Process race info
										$therace = $xml->race[$r]->race_info;
										$leader_race_id				= (int) $therace->race_id;
										$race_title_1				= (string) $therace->race_name1;
										$race_title_2				= (string) $therace->race_name2;
										$race_title_3				= (string) $therace->race_name3;
										$race_precincts_reporting	= (int) $therace->precincts_reporting;
										$race_precincts_percent		= (int) $therace->pct_precincts_reporting;
										$race_total_precincts		= (int) $therace->precincts_total;
										$race_last_leader_update	= strtotime($therace->last_updated);
										$race_total_votes			= (int) $therace->total_vote;
										$jurisdiction_id			= (string) $therace->jurisdiction_id;
										$race_other_1				= (string) $therace->other1;
										$race_other_2				= (string) $therace->other2;
										$race_other_3				= (string) $therace->other3;
										//proposition, yesno1, yesno2, yesno3, yesno4, other1, other2, other3
										// Ingest race info
										$race_source_join = array(
											array('jn_table'=>'election_race', 'jn_value'=>'election_race.race_id', 'jn_equalto'=>'election_race_source_map.race_id')
										);
										$race_data = $this->Election2012_model->getRaceDataSource("election_race.race_id", $race_source_join, array('election_race_source_map.source_id'=>$source_id, 'election_race_source_map.source_value'=>$leader_race_id), NULL, NULL, false);
										$race_id = (($race_data)? ((array_key_exists(0, $race_data))? ((array_key_exists("race_id", $race_data[0]))? $race_data[0]['race_id'] : NULL) : NULL) : NULL);
										// If the race doesn't exist, create it
										if(!$race_id) {
											$race_insert_array = array(
												'race_status_id'	=> 2, 
												'race_title'		=> $race_title_1, 
												'race_title_short'	=> $race_title_2, 
												'race_name'			=> str_replace(" ", "_", strtolower($race_title_1." ".$race_title_2)),
												'race_updated'		=> time()
											);
											try{
												$race_insert_result = $this->Election2012_model->insertRace($race_insert_array);
											} catch(Exception $e) {
												$error = true;
												array_push($response[$theorg]['message'], "Race result save error: ".$e);
											}
											$race_id = mysql_insert_id();
											// Map the source ID with the internal race ID
											$map_insert_array = array(
												'race_id'		=> $race_id,
												'source_id'		=> $source_id,
												'source_value'	=> $leader_race_id
											);
											try{
												$map_insert_result = $this->Election2012_model->insertRaceDataSource($map_insert_array);
											} catch(Exception $e) {
												$error = true;
												array_push($response[$theorg]['message'], "Race data source mapping error: ".$e);
											}
										}
										array_push($theraces2d, $race_id);
										$race_result_insert_array = array(
											'source_id'							=> $source_id,
											'race_id'							=> $race_id,
											'county_fip'						=> ${$fip_field},
											'race_result_precincts_reporting'	=> $race_precincts_reporting,
											'race_result_precincts_percent'		=> $race_precincts_percent,
											'race_result_total'					=> $race_total_votes,
											'race_result_updated'				=> $race_last_leader_update
										);
										$race_result_insert_result = $this->Election2012_model->insertRaceResult($race_result_insert_array);
										$totalr++;
										$new_race_result_id = mysql_insert_id();
										// Process candidate info
										$thecandidates = $xml->race[$r]->candidate_info;
										$candidate_num = $this->electiondatalib->countNodes($thecandidates, 'candidate');
										$candidate_data = array();
										for($c=0; $c<$candidate_num; $c++) {
											$thecandidate = $thecandidates->candidate[$c];
											$leader_candidate_id		= (int) $thecandidate->candidate_id;
											$candidate_first			= (string) $thecandidate->first_name;
											$candidate_mi				= (string) $thecandidate->initial;
											$candidate_last				= (string) $thecandidate->last_name;
											$candidate_party			= $this->Election2012_model->getPartyByCode((string) $thecandidate->party_abbr);
											$candidate_incumbent		= (((string) $thecandidate->incumbent=='True')? 1 : 0);
											$candidate_winner			= (((string) $thecandidate->winner=='True')? 1 : 0);
											$candidate_projected_winner	= (((int) $thecandidate->projected_winner=='True')? 1 : 0);
											$candidate_cand_vote		= (int) str_replace(",", "", $thecandidate->cand_vote);
											$candidate_vote_percent		= (int) $thecandidate->vote_percent;
											//$cother1, $cother2, $cother3
											$candidate_source_join = array(
												array('jn_table'=>'election_candidate', 'jn_value'=>'election_candidate.candidate_id', 'jn_equalto'=>'election_candidate_source_map.candidate_id')
											);
											$candidate_data = $this->Election2012_model->getCandidateDataSource("election_candidate.candidate_id", $candidate_source_join, array('election_candidate_source_map.source_id'=>$source_id, 'election_candidate_source_map.source_value'=>$leader_race_id.$leader_candidate_id), NULL, NULL, false);
											$candidate_id = ((array_key_exists(0, $candidate_data))? $candidate_data[0]['candidate_id'] : NULL);
											// If the candidate doesn't exist, create it
											if(!$candidate_id) {
												$candidate_insert_array = array(
													'party_id'				=> $candidate_party,
													'candidate_first'		=> $candidate_first,
													'candidate_mi'			=> $candidate_mi,
													'candidate_last'		=> $candidate_last,
													'candidate_incumbent'	=> $candidate_incumbent,
													'candidate_winner'		=> $candidate_winner,
													'candidate_updated'		=> time()
												);
												$candidate_id = 0;
												try{
													$candidate_insert_result = $this->Election2012_model->insertCandidate($candidate_insert_array);
													$candidate_id = mysql_insert_id();
												} catch(Exception $e) {
													$error = true;
													array_push($response[$theorg]['message'], "Candidate save error: ".$e);
												}
												// Map the source ID with the internal race ID
												$cmap_insert_array = array(
													'candidate_id'	=> $candidate_id,
													'source_id'		=> $source_id,
													'source_value'	=> $leader_race_id.$leader_candidate_id
												);
												try{
													$cmap_insert_result = $this->Election2012_model->insertCandidateDataSource($cmap_insert_array);
												} catch(Exception $e) {
													$error = true;
													array_push($response[$theorg]['message'], "Candidate data source mapping error: ".$e);
												}
											}
											$check_race_candidate = $this->Election2012_model->getRaceCandidate(NULL, NULL, array('race_id'=>$race_id, 'candidate_id'=>$candidate_id), NULL, NULL, true);
											if(!$check_race_candidate) {
												$race_candidate_array = array(
													'race_id'		=> $race_id,
													'candidate_id'	=> $candidate_id
												);
												try{
													$race_candidate_result = $this->Election2012_model->insertRaceCandidate($race_candidate_array);
												} catch(Exception $e) {
													$error = true;
													array_push($response[$theorg]['message'], "Race/candidate mapping error: ".$e);
												}
											}
											array_push($thecandidates2d, $candidate_id);
											$candidate_result_insert_array = array(
												'source_id'					=> $source_id,
												'race_result_id'			=> $new_race_result_id,
												'candidate_id'				=> $candidate_id,
												'party_id'					=> $candidate_party,
												'candidate_result_value'	=> $candidate_cand_vote,
												'candidate_result_percent'	=> $candidate_vote_percent,
												'candidate_result_winner'	=> $candidate_winner,
												'candidate_result_updated'	=> time()
											);
											try{
												$candidate_result_insert_result = $this->Election2012_model->insertCandidateResult($candidate_result_insert_array);
											} catch(Exception $e) {
												$error = true;
												array_push($response[$theorg]['message'], "Candidate result save error: ".$e);
											}
											$totalc++;
										}
									}
									//log_message('info', 'Ingestion complete.');
									$elapsed = date("i:s", time() - $starttime);
									array_push($response[$theorg]['message'], "Ingestion Complete for ".$data['station_display'].".");
									array_push($response[$theorg]['message'], "Processing time for ".$data['station_display']." - ".$elapsed.".");
									array_push($response[$theorg]['message'], $totalr." races processed for ".$data['station_display'].".");
									array_push($response[$theorg]['message'], $totalc." candidates processed for ".$data['station_display'].".");
								} else {
									$error = true;
									array_push($response[$theorg]['message'], "Unable to download XML feed");
								}
								break;
							}
							case "newsticker":
							{
								$source_url = $module_preferences['election_xml'];
								if($xml = $this->electiondatalib->getParsedFeed($this->electiondatalib->downloadXml($source_url))){
									$race_num = $this->electiondatalib->countNodes($xml, 'RACE');
									// Remove old race result records
									$delete_race_where_array = array(
										array('where_value'=>'source_id', 'where_equalto'=>$source_id, 'where_type'=>'and')
									);
									$race_result_delete_result = $this->Election2012_model->deleteRaceResult($delete_race_where_array);
									// Remove old candidate result records
									$delete_candidate_where_array = array(
										array('where_value'=>'source_id', 'where_equalto'=>$source_id, 'where_type'=>'and')
									);
									$candidate_result_delete_result = $this->Election2012_model->deleteCandidateResult($delete_candidate_where_array);
									for($r=0; $r<$race_num; $r++) {
										// Process race info
										$therace = $xml->RACE[$r]->RACE_INFO;
										$newsticker_race_id			= (int) $therace->RACE_ID;
										$race_title_1				= (string) $therace->RACE_NAME1;
										$race_title_2				= (string) $therace->RACE_NAME2;
										$race_title_3				= (string) $therace->RACE_NAME3;
										$race_title_4				= (string) $therace->RACE_NAME4;
										$race_prompt_1				= (string) $therace->PROMPT_LINES1;
										$race_prompt_2				= (string) $therace->PROMPT_LINES2;
										$race_precincts_reporting	= (int) $therace->PRECINCTS_REPORTING;
										$race_precincts_percent		= (int) $therace->PCT_PRECINCTS_REPORTING;
										$race_total_precincts		= (int) $therace->PRECINCTS_TOTAL;
										$race_last_newsticker_update= strtotime($therace->UPDATED);
										$race_total_votes			= (int) $therace->ALLCAND_VOTES;
										$jurisdiction_id			= (string) $therace->JURISDICTION_ID;
										//proposition, yesno1, yesno2, yesno3, yesno4, other1, other2, other3
										// Ingest race info
										$race_source_join = array(
											array('jn_table'=>'election_race', 'jn_value'=>'election_race.race_id', 'jn_equalto'=>'election_race_source_map.race_id')
										);
										$race_data = $this->Election2012_model->getRaceDataSource("election_race.race_id", $race_source_join, array('election_race_source_map.source_id'=>$source_id, 'election_race_source_map.source_value'=>$newsticker_race_id), NULL, NULL, false);
										$race_id = ((array_key_exists(0, $race_data))? $race_data[0]['race_id'] : NULL);
										// If the race doesn't exist, create it
										if(!$race_id) {
											$race_insert_array = array(
												'race_status_id'	=> 2, 
												'race_title'		=> $race_title_1.", ".$race_title_2, 
												'race_title_short'	=> $race_title_1, 
												'race_name'			=> str_replace(" ", "_", strtolower($race_title_1." ".$race_title_2)),
												'race_updated'		=> time()
											);
											try{
												$race_insert_result = $this->Election2012_model->insertRace($race_insert_array);
											} catch(Exception $e) {
												$error .= $e."\n";
											}
											$race_id = mysql_insert_id();
											// Map the source ID with the internal race ID
											$map_insert_array = array(
												'race_id'		=> $race_id,
												'source_id'		=> $source_id,
												'source_value'	=> $newsticker_race_id
											);
											try{
												$map_insert_result = $this->Election2012_model->insertRaceDataSource($map_insert_array);
											} catch(Exception $e) {
												$error = true;
												array_push($response[$theorg]['message'], "Race data source mapping error: ".$e);
											}
										}
										array_push($theraces2d, $race_id);
										$race_result_insert_array = array(
											'source_id'							=> $source_id,
											'race_id'							=> $race_id,
											'county_fip'						=> ${$fip_field},
											'race_result_precincts_reporting'	=> $race_precincts_reporting,
											'race_result_precincts_percent'		=> $race_precincts_percent,
											'race_result_total'					=> $race_total_votes,
											'race_result_updated'				=> time()
										);
										$race_result_insert_result = $this->Election2012_model->insertRaceResult($race_result_insert_array);
										$totalr++;
										$new_race_result_id = mysql_insert_id();
										// Process candidate info
										$thecandidates = $xml->RACE[$r]->CANDIDATE_INFO;
										$candidate_num = $this->electiondatalib->countNodes($thecandidates, 'CANDIDATE');
										$candidate_data = array();
										for($c=0; $c<$candidate_num; $c++) {
											$thecandidate = $thecandidates->CANDIDATE[$c];
											$pabbr = ((strstr((string) $thecandidate->FIRST_NAME, "-")!=false)? substr((string) $thecandidate->FIRST_NAME, 0, 1) : NULL);
											$newsticker_candidate_id	= (int) $thecandidate->CANDIDATE_ID;
											$candidate_display_order	= (int) $thecandidate->DISPLAY_ORDER;
											$candidate_visible			= (int) $thecandidate->VISIBLE;
											$candidate_first			= (string) ((strstr((string) $thecandidate->FIRST_NAME, "-")!=false)? substr((string) $thecandidate->FIRST_NAME, 2) : $thecandidate->FIRST_NAME);
											$candidate_last				= (string) $thecandidate->LAST_NAME;
											$candidate_party			= $this->Election2012_model->getPartyByCode((($pabbr)? $pabbr : (string) $thecandidate->PARTY_ABBR));
											$candidate_party2			= (string) $thecandidate->PARTY_NAME1;
											$candidate_party3			= (string) $thecandidate->PARTY_NAME2;
											$candidate_incumbent		= (((string) $thecandidate->INCUMBENT=='Y')? 1 : 0);
											$candidate_winner			= (((string) $thecandidate->WINNER=='Y' || (string) $thecandidate->WINNER=='Z')? 1 : 0);
											$candidate_cand_vote		= (int) $thecandidate->VOTE_TOTAL;
											$candidate_vote_percent		= (int) $thecandidate->PCT_TOTAL;
											//$cother1, $cother2, $cother3
											$candidate_source_join = array(
												array('jn_table'=>'election_candidate', 'jn_value'=>'election_candidate.candidate_id', 'jn_equalto'=>'election_candidate_source_map.candidate_id')
											);
											$candidate_id = $this->Election2012_model->getCandidateDataSource("election_candidate.candidate_id", $candidate_source_join, array('election_candidate_source_map.source_id'=>$source_id, 'election_candidate_source_map.source_value'=>$newsticker_race_id.$newsticker_candidate_id), NULL, NULL, false);
											$candidate_id = ((array_key_exists(0, $candidate_id))? $candidate_id[0]['candidate_id'] : NULL);
											// If the candidate doesn't exist, create it
											if(!$candidate_id) {
												$candidate_insert_array = array(
													'party_id'				=> $candidate_party,
													'candidate_first'		=> $candidate_first,
													'candidate_last'		=> $candidate_last,
													'candidate_incumbent'	=> $candidate_incumbent,
													'candidate_winner'		=> $candidate_winner,
													'candidate_updated'		=> time()
												);
												$candidate_id = 0;
												try{
													$candidate_insert_result = $this->Election2012_model->insertCandidate($candidate_insert_array);
													$candidate_id = mysql_insert_id();
												} catch(Exception $e) {
													$error .= $e."\n";
												}
												// Map the source ID with the internal race ID
												$cmap_insert_array = array(
													'candidate_id'	=> $candidate_id,
													'source_id'		=> $source_id,
													'source_value'	=> $newsticker_race_id.$newsticker_candidate_id
												);
												try{
													$cmap_insert_result = $this->Election2012_model->insertCandidateDataSource($cmap_insert_array);
												} catch(Exception $e) {
													$error = true;
													array_push($response[$theorg]['message'], "Candidate data source mapping error: ".$e);
												}
											}
											$check_race_candidate = $this->Election2012_model->getRaceCandidate(NULL, NULL, array('race_id'=>$race_id, 'candidate_id'=>$candidate_id), NULL, NULL, true);
											if(!$check_race_candidate) {
												$race_candidate_array = array(
													'race_id'		=> $race_id,
													'candidate_id'	=> $candidate_id
												);
												try{
													$race_candidate_result = $this->Election2012_model->insertRaceCandidate($race_candidate_array);
												} catch(Exception $e) {
													$error = true;
													array_push($response[$theorg]['message'], "Race/candidate mapping error: ".$e);
												}
											}
											array_push($thecandidates2d, $candidate_id);
											$candidate_result_insert_array = array(
												'source_id'					=> $source_id,
												'race_result_id'			=> $new_race_result_id,
												'candidate_id'				=> $candidate_id,
												'party_id'					=> $candidate_party,
												'candidate_result_value'	=> $candidate_cand_vote,
												'candidate_result_percent'	=> $candidate_vote_percent,
												'candidate_result_winner'	=> $candidate_winner,
												'candidate_result_updated'	=> time()
											);
											try{
												$candidate_result_insert_result = $this->Election2012_model->insertCandidateResult($candidate_result_insert_array);
											} catch(Exception $e) {
												$error = true;
												array_push($response[$theorg]['message'], "Candidate result save error: ".$e);
											}
											$totalc++;
										}
									}
									//log_message('info', 'Ingestion complete.');
									$elapsed = date("i:s", time() - $starttime);
									array_push($response[$theorg]['message'], "Ingestion Complete for ".$data['station_display'].".");
									array_push($response[$theorg]['message'], "Ingestion time for ".$data['station_display']." - ".$elapsed.".");
									array_push($response[$theorg]['message'], $totalr." races processed for ".$data['station_display'].".");
									array_push($response[$theorg]['message'], $totalc." candidates processed for ".$data['station_display'].".");
								} else {
									$error = true;
									array_push($response[$theorg]['message'], "Unable to download XML feed.");
								}
								break;
							}
							case "btileader":
							{
								if($xml = $this->electiondatalib->getParsedFeed($this->electiondatalib->downloadXml($source_url))){
									$race_num = $this->electiondatalib->countNodes($xml, 'Race');
									// Remove old race result records
									$delete_race_where_array = array(
										array('where_value'=>'source_id', 'where_equalto'=>$source_id, 'where_type'=>'and')
									);
									$race_result_delete_result = $this->Election2012_model->deleteRaceResult($delete_race_where_array);
									// Remove old candidate result records
									$delete_candidate_where_array = array(
										array('where_value'=>'source_id', 'where_equalto'=>$source_id, 'where_type'=>'and')
									);
									$candidate_result_delete_result = $this->Election2012_model->deleteCandidateResult($delete_candidate_where_array);
									for($r=0; $r<$race_num; $r++) {
										// Process race info
										$therace = $xml->race[$r];
										$btileader_race_id			= (int) $therace->RaceNumber;
										$race_title_1				= (string) $therace->Name1;
										$race_title_2				= (string) $therace->Name2;
										$race_title_3				= (string) $therace->Name3;
										$race_precincts_reporting	= (int) $therace->PrecinctsReported;
										$race_precincts_percent		= (int) $therace->PrecinctsPercentage;
										$race_total_precincts		= (int) $therace->TotalPrecincts;
										$race_last_btileader_update	= strtotime("Today at ".$therace->LastUpdate);
										$race_total_votes			= (int) $therace->VotesCast;
										$jurisdiction_id			= (string) $therace->jurisdiction_id;
										//$race_other_1				= (string) $therace->other1;
										//$race_other_2				= (string) $therace->other2;
										//$race_other_3				= (string) $therace->other3;
										//proposition, yesno1, yesno2, yesno3, yesno4, other1, other2, other3
										// Ingest race info
										$race_source_join = array(
											array('jn_table'=>'election_race', 'jn_value'=>'election_race.race_id', 'jn_equalto'=>'election_race_source_map.race_id')
										);
										$race_data = $this->Election2012_model->getRaceDataSource("election_race.race_id", $race_source_join, array('election_race_source_map.source_id'=>$source_id, 'election_race_source_map.source_value'=>$leader_race_id), NULL, NULL, false);
										$race_id = (($race_data)? ((array_key_exists(0, $race_data))? ((array_key_exists("race_id", $race_data[0]))? $race_data[0]['race_id'] : NULL) : NULL) : NULL);
										// If the race doesn't exist, create it
										if(!$race_id) {
											$race_insert_array = array(
												'race_status_id'	=> 2, 
												'race_title'		=> $race_title_2, 
												'race_title_short'	=> $race_title_3, 
												'race_name'			=> str_replace(" ", "_", strtolower($race_title_1)),
												'race_updated'		=> time()
											);
											try{
												$race_insert_result = $this->Election2012_model->insertRace($race_insert_array);
											} catch(Exception $e) {
												$error = true;
												array_push($response[$theorg]['message'], "Race result save error: ".$e);
											}
											$race_id = mysql_insert_id();
											// Map the source ID with the internal race ID
											$map_insert_array = array(
												'race_id'		=> $race_id,
												'source_id'		=> $source_id,
												'source_value'	=> $btileader_race_id
											);
											try{
												$map_insert_result = $this->Election2012_model->insertRaceDataSource($map_insert_array);
											} catch(Exception $e) {
												$error = true;
												array_push($response[$theorg]['message'], "Race data source mapping error: ".$e);
											}
										}
										array_push($theraces2d, $race_id);
										$race_result_insert_array = array(
											'source_id'							=> $source_id,
											'race_id'							=> $race_id,
											'county_fip'						=> ${$fip_field},
											'race_result_precincts_reporting'	=> $race_precincts_reporting,
											'race_result_precincts_percent'		=> $race_precincts_percent,
											'race_result_total'					=> $race_total_votes,
											'race_result_updated'				=> $race_last_btileader_update
										);
										$race_result_insert_result = $this->Election2012_model->insertRaceResult($race_result_insert_array);
										$totalr++;
										$new_race_result_id = mysql_insert_id();
										// Process candidate info
										$thecandidates = $xml->race[$r]->candidate_info;
										$candidate_num = $this->electiondatalib->countNodes($thecandidates, 'candidate');
										$candidate_data = array();
										for($c=0; $c<$candidate_num; $c++) {
											$thecandidate = $thecandidates->candidate[$c];
											$leader_candidate_id		= (int) $thecandidate->candidate_id;
											$candidate_first			= (string) $thecandidate->first_name;
											$candidate_mi				= (string) $thecandidate->initial;
											$candidate_last				= (string) $thecandidate->last_name;
											$candidate_party			= $this->Election2012_model->getPartyByCode((string) $thecandidate->party_abbr);
											$candidate_incumbent		= (((string) $thecandidate->incumbent=='True')? 1 : 0);
											$candidate_winner			= (((string) $thecandidate->winner=='True')? 1 : 0);
											$candidate_projected_winner	= (((int) $thecandidate->projected_winner=='True')? 1 : 0);
											$candidate_cand_vote		= (int) str_replace(",", "", $thecandidate->cand_vote);
											$candidate_vote_percent		= (int) $thecandidate->vote_percent;
											//$cother1, $cother2, $cother3
											$candidate_source_join = array(
												array('jn_table'=>'election_candidate', 'jn_value'=>'election_candidate.candidate_id', 'jn_equalto'=>'election_candidate_source_map.candidate_id')
											);
											$candidate_data = $this->Election2012_model->getCandidateDataSource("election_candidate.candidate_id", $candidate_source_join, array('election_candidate_source_map.source_id'=>$source_id, 'election_candidate_source_map.source_value'=>$leader_race_id.$leader_candidate_id), NULL, NULL, false);
											$candidate_id = ((array_key_exists(0, $candidate_data))? $candidate_data[0]['candidate_id'] : NULL);
											// If the candidate doesn't exist, create it
											if(!$candidate_id) {
												$candidate_insert_array = array(
													'party_id'				=> $candidate_party,
													'candidate_first'		=> $candidate_first,
													'candidate_mi'			=> $candidate_mi,
													'candidate_last'		=> $candidate_last,
													'candidate_incumbent'	=> $candidate_incumbent,
													'candidate_winner'		=> $candidate_winner,
													'candidate_updated'		=> time()
												);
												$candidate_id = 0;
												try{
													$candidate_insert_result = $this->Election2012_model->insertCandidate($candidate_insert_array);
													$candidate_id = mysql_insert_id();
												} catch(Exception $e) {
													$error = true;
													array_push($response[$theorg]['message'], "Candidate save error: ".$e);
												}
												// Map the source ID with the internal race ID
												$cmap_insert_array = array(
													'candidate_id'	=> $candidate_id,
													'source_id'		=> $source_id,
													'source_value'	=> $leader_race_id.$leader_candidate_id
												);
												try{
													$cmap_insert_result = $this->Election2012_model->insertCandidateDataSource($cmap_insert_array);
												} catch(Exception $e) {
													$error = true;
													array_push($response[$theorg]['message'], "Candidate data source mapping error: ".$e);
												}
											}
											$check_race_candidate = $this->Election2012_model->getRaceCandidate(NULL, NULL, array('race_id'=>$race_id, 'candidate_id'=>$candidate_id), NULL, NULL, true);
											if(!$check_race_candidate) {
												$race_candidate_array = array(
													'race_id'		=> $race_id,
													'candidate_id'	=> $candidate_id
												);
												try{
													$race_candidate_result = $this->Election2012_model->insertRaceCandidate($race_candidate_array);
												} catch(Exception $e) {
													$error = true;
													array_push($response[$theorg]['message'], "Race/candidate mapping error: ".$e);
												}
											}
											array_push($thecandidates2d, $candidate_id);
											$candidate_result_insert_array = array(
												'source_id'					=> $source_id,
												'race_result_id'			=> $new_race_result_id,
												'candidate_id'				=> $candidate_id,
												'party_id'					=> $candidate_party,
												'candidate_result_value'	=> $candidate_cand_vote,
												'candidate_result_percent'	=> $candidate_vote_percent,
												'candidate_result_winner'	=> $candidate_winner,
												'candidate_result_updated'	=> time()
											);
											try{
												$candidate_result_insert_result = $this->Election2012_model->insertCandidateResult($candidate_result_insert_array);
											} catch(Exception $e) {
												$error = true;
												array_push($response[$theorg]['message'], "Candidate result save error: ".$e);
											}
											$totalc++;
										}
									}
									//log_message('info', 'Ingestion complete.');
									$elapsed = date("i:s", time() - $starttime);
									array_push($response[$theorg]['message'], "Ingestion Complete for ".$data['station_display'].".");
									array_push($response[$theorg]['message'], "Processing time for ".$data['station_display']." - ".$elapsed.".");
									array_push($response[$theorg]['message'], $totalr." races processed for ".$data['station_display'].".");
									array_push($response[$theorg]['message'], $totalc." candidates processed for ".$data['station_display'].".");
								} else {
									$error = true;
									array_push($response[$theorg]['message'], "Unable to download XML feed");
								}
								break;
							}
							default:
							{
								$error = true;
								array_push($response[$theorg]['message'], "Election feed type not defined.");
							}
						}
					} else {
						$error = true;
						array_push($response[$theorg]["message"], $data['station_display']." does not have the election refresh enabled.");
					}
				}
			} else {
				die("Election refresh failed. No organization IDs were supplied.");
			}
		} catch (Exception $e) {
			die($e);
		}
		// Start Export
		try{
			// Establish which organizations should be included in this update
			$org_array = array();
			$orgs = (($id)? $id : NULL);
			// Only process if orgs are present, otherwise kill the script
			if($orgs){
				if (strpos($orgs,':') !== false){
					$org_array = explode(":", $orgs);
				} else {
					$org_array[0] = $orgs;
				}
				// Begin processing organization election results
				foreach($org_array as $theorg){
					$data['station_id'] = 0;
					$data['station_display'] = "";
					$station_id = $theorg;
					if(!is_numeric($station_id)) {
						$station_where = array(array('where_value'=>'organization_name', 'where_equalto'=>$station_id, 'where_type'=>'like'));
						$station_data = $this->Coreadmin_model->getOrganization("organization_id, organization_name", NULL, $station_where, NULL, 1, false);
						$data['station_id'] = $station_id = ((array_key_exists(0, $station_data))? $station_data[0]['organization_id'] : 0);
						$data['station_display'] = $station_display  = ((array_key_exists(0, $station_data))? $station_data[0]['organization_name'] : 0);
					} else {
						$station_where = array(array('where_value'=>'organization_id', 'where_equalto'=>$station_id, 'where_type'=>'and'));
						$station_data = $this->Coreadmin_model->getOrganization("organization_id, organization_name", NULL, $station_where, NULL, 1, false);
						$data['station_id'] = $station_id = ((array_key_exists(0, $station_data))? $station_data[0]['organization_id'] : 0);
						$data['station_display'] = $station_display = ((array_key_exists(0, $station_data))? $station_data[0]['organization_name'] : 0);
					}
					$source_id = $station_id;
					// Get module preferences and set as variable
					$like_where = array(
						array('where_value'=>'organization_id', 'where_equalto'=>$station_id, 'where_type'=>'and'),
						array('where_value'=>'preference_key', 'where_equalto'=>'preference', 'where_type'=>'like'),
						array('where_value'=>'preference_key', 'where_equalto'=>$data['module_id'], 'where_type'=>'like')
					);
					$module_preferences = $this->Election2012_model->getOrganizationPreference("preference_key, preference_value", NULL, $like_where, NULL, NULL, false);
					$mod_prefs = array();
					foreach($module_preferences as $preference) {
						$temp = array();
						$temp = $preference['preference_key'];
						$temp = explode("|", $temp);
						$preference['preference_key'] = $temp[2];
						$mod_prefs[$preference['preference_key']] = $preference['preference_value'];
					}
					$data["module_preferences"] = $module_preferences = $mod_prefs;
					$proceed = (($mod_prefs)? ((array_key_exists("enable_cron", $mod_prefs))? (($mod_prefs["enable_cron"]==1)? true : false) : false) : false);
					if($proceed==true){
						/*$response[$theorg]['org'] = $station_id;
						$response[$theorg]['error'] = false;
						$response[$theorg]['message'] = array();*/
						if($races = $this->getRaceIDsByType($station_id, $state, $type)){
							$race_join = array(
								array('jn_table'=>'election_race_result', 'jn_value'=>'election_race_result.race_id', 'jn_equalto'=>'election_race.race_id'),
								array('jn_table'=>'election_race_source_map', 'jn_value'=>'election_race_source_map.race_id', 'jn_equalto'=>'election_race.race_id')
							);
							$race_where = array(
								array('where_value'=>'election_race_result.race_id', 'where_equalto'=>$races, 'where_type'=>'in')
							);
							$fields = "election_race.race_id, election_race.race_status_id, election_race.race_title, election_race.race_title_short, election_race.race_name, election_race.race_updated, election_race_result.race_result_id, election_race_result.county_fip, election_race_result.race_result_precincts_reporting, election_race_result.race_result_precincts_percent, election_race_result.race_result_total, election_race_result.race_result_total_percent, election_race_source_map.source_value";
							$total_records = $this->Election2012_model->getRace($fields, $race_join, $race_where, NULL, NULL, true);
							$races = $this->Election2012_model->getRace($fields, $race_join, $race_where, NULL, NULL, false);
							$race_data = array();
							foreach($races as $race) {
								$race['county_name'] = $this->Election2012_model->getSourceLabelByFIPS($race['county_fip']);
								$statuses = $this->Election2012_model->getRaceStatus(NULL, NULL, array('race_status_id'=>$race['race_status_id']), NULL, 1, false);
								foreach($statuses as $status) {
									$race['race_status_name'] = $status['race_status_name'];
								}
								$candidate_join = array(
									array('jn_table'=>'election_candidate', 'jn_value'=>'election_candidate.candidate_id', 'jn_equalto'=>'election_race_candidate_jn.candidate_id'),
									array('jn_table'=>'election_candidate_source_map', 'jn_value'=>'election_candidate_source_map.candidate_id', 'jn_equalto'=>'election_race_candidate_jn.candidate_id'),
									array('jn_table'=>'election_party', 'jn_value'=>'election_party.party_id', 'jn_equalto'=>'election_candidate.party_id'),
									array('jn_table'=>'election_candidate_result', 'jn_value'=>'election_candidate_result.candidate_id', 'jn_equalto'=>'election_candidate.candidate_id')
								);
								$candidate_where = array(
									array('where_value'=>'election_race_candidate_jn.race_id', 'where_equalto'=>$race['race_id'], 'where_type'=>'and'),
									array('where_value'=>'election_candidate_result.race_result_id', 'where_equalto'=>$race['race_result_id'], 'where_type'=>'and')
								);
								$candidate_fields = "election_candidate.candidate_id, election_candidate.party_id, election_candidate.race_id, election_candidate.candidate_first, election_candidate.candidate_mi, election_candidate.candidate_last, election_candidate.candidate_incumbent, election_candidate.candidate_winner, election_party.party_code, election_candidate_result.candidate_result_value, election_candidate_result.candidate_result_percent, election_candidate_result.candidate_result_winner, election_candidate_source_map.source_value";
								$candidates = $this->Election2012_model->getRaceCandidate($candidate_fields, $candidate_join, $candidate_where, NULL, NULL, false);
								$race['race_candidates']	= (($candidates)? $candidates : array());
								array_push($race_data, $race);
							}
							$race_data = $this->utilities->multiColSort($race_data, 'source_value', SORT_ASC, 'county_fip', SORT_ASC, 'race_id', SORT_ASC);
							$races = $race_data;
							switch(strtolower($form)){
								case 'xml': {
									// Create array of race data
									$xml_data = new SimpleXMLElement("<?xml version='1.0' encoding='ISO-8859-1'?><data></data>");
									$temp_array = array();
									foreach($race_data as $race){
										$thenode = array(
											'race'	=> array (
												'race_info'	=> array(
													'race_id'					=> $race['source_value'],
													'race_name1'				=> htmlspecialchars($race['race_title']),
													'race_name2'				=> htmlspecialchars($race['race_title_short']),
													'race_name3'				=> "",
													'precincts_reporting'		=> number_format($race['race_result_precincts_reporting']),
													'pct_precincts_reporting'	=> number_format($race['race_result_precincts_percent']),
													'precincts_total'			=> number_format($race['race_result_total']),
													'last_updated'				=> str_replace("PM", "p.m.", str_replace("AM", "a.m.", date("M d, Y g:i A", time()))),
													'timestamp'					=> time(),
													'total_vote'				=> "",
													'other1'					=> "",
													'other2'					=> "",
													'other3'					=> $race['county_name'],
													'jurisdiction_id'			=> $race['county_fip'],
												
												),
												'candidate'				=> array()
											)
										);
										$candidates = ((array_key_exists("race_candidates", $race))? $race['race_candidates'] : array());
										foreach($candidates as $candidate){
											$thecandidates = array(
												'candidate_info'	=> array(
													'candidate_id'		=> $candidate['source_value'],
													'first_name'		=> htmlspecialchars($candidate['candidate_first']),
													'initial'			=> htmlspecialchars($candidate['candidate_mi']),
													'last_name'			=> htmlspecialchars($candidate['candidate_last']),
													'incumbent'			=> $candidate['candidate_incumbent'],
													'party_abbr'		=> $candidate['party_code'],
													'cand_vote'			=> number_format($candidate['candidate_result_value']),
													'vote_percent'		=> number_format($candidate['candidate_result_percent']),
													'winner'			=> $candidate['candidate_winner'],
													'projected_winner'	=> "",
													'cother1'			=> "",
													'cother2'			=> "",
													'cother3'			=> ""
												)
											);
											array_push($thenode['race']['candidate'], $thecandidates);
										}
										array_push($temp_array, $thenode);
									}
									// Convert array to XML
									$return = $this->utilities->array_to_xml($temp_array, $xml_data);
									$dom = dom_import_simplexml($return)->ownerDocument;
									$dom->formatOutput = true;
									// Start file export
									$thefile = strtolower(str_replace("-TV", "", $station_display)).'.xml';
									$thelocalfile = (string) FCPATH.'ci/_common/xml/'.$thefile;
									if (!write_file($thelocalfile, $dom->saveXML())) {
										$response[$theorg]['error'] = true;
										array_push($response[$theorg]['message'], "Unable to write the file");
									} else {
										array_push($response[$theorg]['message'], "File ".$thefile." written at ".date("g:i a (T)", time()).".");
										$ftp_address	= $mod_prefs['elect_ftp_export_address'];
										$ftp_user		= $mod_prefs['elect_ftp_export_username'];
										$ftp_pass		= $mod_prefs['elect_ftp_export_password'];
										$ftp_path		= $mod_prefs['elect_ftp_export_path'];
										$ftp_port		= intval($mod_prefs['elect_ftp_export_port']);
										$ftp_conn		= ftp_connect($ftp_address, $ftp_port) or array_push($response[$theorg]['message'], "Could not connect to host.");
										ftp_login($ftp_conn, $ftp_user, $ftp_pass) or array_push($response[$theorg]['message'], "Could not log in.");
										//ftp_chdir($ftp_conn, $ftp_path);
										if(ftp_put($ftp_conn, $ftp_path.$thefile, $thelocalfile, FTP_BINARY)==true) {
											array_push($response[$theorg]['message'], "File ".$thefile." uploaded to ".$this->config->item('ftp_address').$this->config->item('ftp_path').".");
										} else {
											$response[$theorg]['error'] = true;
											array_push($response[$theorg]['message'], "Unable to upload ".$thefile.".");
										}
										ftp_close($ftp_conn);
									}
									break;
								}
								case 'json': {
									$race_info = array();
									$i=0;
									foreach($races as $race){
										$race_info[$i]['race_id'] = $race['source_value'];
										$race_info[$i]['race_name1'] = htmlspecialchars($race['race_title']);
										$race_info[$i]['race_name2'] = htmlspecialchars($race['race_title_short']);
										$race_info[$i]['race_name3'] = "";
										$race_info[$i]['precincts_reporting'] = number_format($race['race_result_precincts_reporting']);
										$race_info[$i]['pct_precincts_reporting'] = number_format($race['race_result_precincts_percent']);
										$race_info[$i]['precincts_total'] = number_format($race['race_result_total']);
										$race_info[$i]['last_updated'] = str_replace("PM", "p.m.", str_replace("AM", "a.m.", date("M d, Y g:i A", $race['race_updated']))); //Nov 03, 2010  11:30 AM
										$race_info[$i]['timestamp'] = $race['race_updated'];
										$race_info[$i]['total_vote'] = "";
										$race_info[$i]['other1'] = "";
										$race_info[$i]['other2'] = "";
										$race_info[$i]['other3'] = "";
										$c=0;
										$candidates = ((array_key_exists("race_candidates", $race))? $race['race_candidates'] : array());
										foreach($candidates as $candidate){
											$race_info[$i]['candidate_info'][$c]['candidate_id'] = $candidate['source_value'];
											$race_info[$i]['candidate_info'][$c]['first_name'] = htmlspecialchars($candidate['candidate_first']);
											$race_info[$i]['candidate_info'][$c]['initial'] = htmlspecialchars($candidate['candidate_mi']);
											$race_info[$i]['candidate_info'][$c]['last_name'] = htmlspecialchars($candidate['candidate_last']);
											$race_info[$i]['candidate_info'][$c]['incumbent'] = $candidate['candidate_incumbent'];
											$race_info[$i]['candidate_info'][$c]['party_abbr'] = $candidate['party_code'];
											$race_info[$i]['candidate_info'][$c]['cand_vote'] = number_format($candidate['candidate_result_value']);
											$race_info[$i]['candidate_info'][$c]['vote_percent'] = "";
											$race_info[$i]['candidate_info'][$c]['winner'] = $candidate['candidate_winner'];
											$race_info[$i]['candidate_info'][$c]['projected_winner'] = "";
											$race_info[$i]['candidate_info'][$c]['cother1'] = "";
											$race_info[$i]['candidate_info'][$c]['cother2'] = "";
											$race_info[$i]['candidate_info'][$c]['cother3'] = "";
											$c++;
										}
										$i++;
									}
									$race_info = json_encode($race_info);
									$thefile = strtolower(str_replace("-TV", "", $station_display)).'.txt';
									$thelocalfile = (string) FCPATH.'ci/_common/xml/'.$thefile;
									if (!write_file($thelocalfile, $race_info)) {
										$response[$theorg]['error'] = true;
										array_push($response[$theorg]['message'], "Unable to write the file.");
									} else {
										array_push($response[$theorg]['message'], "File ".$thefile." written at ".date("g:i a (T)", time()).".");
										$ftp_address	= $mod_prefs['elect_ftp_export_address'];
										$ftp_user		= $mod_prefs['elect_ftp_export_username'];
										$ftp_pass		= $mod_prefs['elect_ftp_export_password'];
										$ftp_path		= $mod_prefs['elect_ftp_export_path'];
										$ftp_port		= $mod_prefs['elect_ftp_export_port'];
										$ftp_conn = ftp_connect($ftp_address, $ftp_port) or array_push($response[$theorg]['message'], "Could not connect to host.");
										ftp_login($ftp_conn, $ftp_user, $ftp_pass) or array_push($response[$theorg]['message'], "Could not log in.");
										//ftp_chdir($ftp_conn, $ftp_path);
										if(ftp_put($ftp_conn, $ftp_path.$thefile, $thelocalfile, FTP_BINARY)==true) {
											array_push($response[$theorg]['message'], "File ".$thefile." uploaded to ".$this->config->item('ftp_address').$this->config->item('ftp_path').".");
										} else {
											$response[$theorg]['error'] = true;
											array_push($response[$theorg]['message'], "Unable to upload ".$thefile.".");
										}
										ftp_close($ftp_conn);
									}
									break;
								}
								default: {
									$error = true;
									array_push($response[$theorg]["message"], "The output format could not be determined");
								}
							}
						} else {
							$error = true;
							array_push($response[$theorg]["message"], "No races could be found for ".$station_display);
						}
						//log_message('error', 'Unable to determine format.');
					} else {
						$error = true;
						array_push($response[$theorg]["message"], $data['station_display']." does not have the election refresh enabled.");
					}
				}
			} else {
				die("Election refresh failed. No organization IDs were supplied.");
			}
		} catch (Exception $e) {
			die($e);
		}
		echo "<pre>";
		print_r($response);
		echo "</pre>";
	}
}
?>