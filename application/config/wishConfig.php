<?php
// Javascript Configs
$config['jquery_current']				= 'https://code.jquery.com/jquery-1.10.2.min.js';
$config['jquery_ui_current']			= 'https://code.jquery.com/ui/1.10.3/jquery-ui.js';
$config['jquery_ui_local']				= 'https://widgets.wishtv.com/ci/_common/js/jqueryui-local/jquery-ui.min.js';
	$config['jquery_ui_1_10_2']			= 'https://code.jquery.com/ui/1.10.2/jquery-ui.js';
	$config['jquery_ui_1_10_1']			= 'https://code.jquery.com/ui/1.10.1/jquery-ui.js';
	$config['jquery_ui_1_9_2']			= 'https://code.jquery.com/ui/1.9.2/jquery-ui.js';
$config['jquery_tools_current']			= 'https://widgets.wishtv.com/ci/_common/js/jquery.tools.min.js';
$config['jquery_datatables_current']	= 'https://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js';
$config['jquery_validation_current']	= 'https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.0/jquery.validate.min.js';
$config['jquery_te_current']			= 'https://widgets.wishtv.com/ci/_common/js/jquery-te/jquery-te-1.4.0.min.js';
$config['jquery_mobile_current']		= 'https://code.jquery.com/mobile/1.3.0/jquery.mobile-1.3.0.min.js';
$config['map_api_key']					= 'AIzaSyCty2AAJHQ4kzHrTNNVQOun0ByFipAfFUM';
$config['jquery_tinymce']				= 'https://widgets.wishtv.com/ci/_common/js/tinymce/jquery.tinymce.min.js';
$config['ckeditor']						= 'https://widgets.wishtv.com/ci/_common/js/ckeditor/ckeditor.js';
$config['jquery_ckeditor_adapter']		= 'https://widgets.wishtv.com/ci/_common/js/ckeditor/adapters/jquery.js';
$config['underscore_current']			= 'https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js';
// CSS configs
$config['jquery_ui_css_current']		= 'https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css';
	$config['jquery_ui_css_1_10_2']		= 'https://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css';
	$config['jquery_ui_css_1_10_1']		= 'https://code.jquery.com/ui/1.10.1/themes/smoothness/jquery-ui.css';
	$config['jquery_ui_css_1_9_2']		= 'https://code.jquery.com/ui/1.9.2/themes/smoothness/jquery-ui.css';
$config['jquery_datatables_css_current'] = '//cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css';
$config['jquery_mobile_css_current']	= 'https://code.jquery.com/mobile/1.3.0/jquery.mobile-1.3.0.min.css';
$config['jquery_te_css_current']		= 'https://widgets.wishtv.com/ci/_common/css/jquery-te-1.4.0.css';
?>